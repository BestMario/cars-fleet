package com.mario.carsfleet.driver.domain;

import com.mario.carsfleet.car.domain.CarFacade;
import com.mario.carsfleet.driver.dto.*;
import com.mario.carsfleet.hepers.DatabaseHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest
public class DriverFacadeTest {

    @Autowired
    private CarFacade carFacade;

    @Autowired
    private DriverFacade driverFacade;

    @Autowired
    private DatabaseHelper databaseHelper;

    @Before
    public void doBeforeEachTests() {
        databaseHelper.deleteAll();
    }

    @Test
    public void given_validDriverData_when_addingNewDriver_then_driverIsAdded() {
        //given
        AddNewDriverCommand newDriver = new AddNewDriverCommand("John", "Kowalski", 32);

        //when
        Integer driverId = driverFacade.add(newDriver);

        //then
        DriverDto driver = driverFacade.findById(driverId);
        Assert.assertNotNull("driver not exist", driver);
    }

    @Test
    public void given_existingDriverData_when_updatingDriver_then_driverIsUpdated() {
        //given
        AddNewDriverCommand newDriver = new AddNewDriverCommand("Mark", "Nowak", 32);
        Integer driverId = driverFacade.add(newDriver);
        UpdateDriverCommand updateDriver = new UpdateDriverCommand("John", "Rony", 32, DriverStatusDto.ACTIVE);
        //when
        driverFacade.update(driverId, updateDriver);

        //then
        DriverDto driver = driverFacade.findById(driverId);
        DriverDto expectedDriver = new DriverDto(driverId, "John", "Rony", 32, DriverStatusDto.ACTIVE);
        Assert.assertThat(driver, is(equalTo(expectedDriver)));
    }

    @Test
    public void given_existingDriverData_when_deletingDriver_then_driverIsDeleted() {
        //given
        AddNewDriverCommand newDriver = new AddNewDriverCommand("Mark", "Nowak", 32);
        Integer driverId = driverFacade.add(newDriver);
        DeleteDriverCommand deleteDriver = new DeleteDriverCommand(driverId);
        //when
        driverFacade.delete(deleteDriver);

        //then
        try {
            driverFacade.findById(driverId);
            Assert.fail("car not exist, exception should be thrown");
        } catch (EmptyResultDataAccessException ex) {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void given_driverExistingData_when_changingDriverStatus_then_statusIsChanged() {
        //given
        AddNewDriverCommand newDriver = new AddNewDriverCommand("Mark", "Nowak", 32);
        Integer driverId = driverFacade.add(newDriver);
        ChangeDriverStatusCommand changeDriverStatus = new ChangeDriverStatusCommand(driverId, DriverStatusDto.INACTIVE);

        //when
        driverFacade.changeDriverStatus(changeDriverStatus);

        //then
        DriverDto driver = driverFacade.findById(driverId);
        Assert.assertThat(driver.getDriverStatus(), is(equalTo(DriverStatusDto.INACTIVE)));
    }

    @Test
    public void given_invalidFirstName_when_addingNewDriver_then_exceptionIsThrown() {
        //given
        AddNewDriverCommand newDriver = new AddNewDriverCommand("", "Kowalski", 23);

        //then
        try {
            driverFacade.add(newDriver);
            //when
            Assert.fail("Driver should not be added because of wrong first name value");
        } catch (NewDriverValidationException ex) {
            Assert.assertTrue("Exception is thrown", true);
        } catch (Exception ex) {
            Assert.fail("Invalid exception");
        }
    }

    @Test
    public void given_invalidDriverLastName_when_updatingDriver_then_exceptionIsThrown() {
        //given
        AddNewDriverCommand newDriver = new AddNewDriverCommand("Jan", "Kowalski", 23);
        Integer driverId = driverFacade.add(newDriver);
        UpdateDriverCommand updateDriver = new UpdateDriverCommand("Jan", "", 23, DriverStatusDto.ACTIVE);

        //when
        try {
            driverFacade.update(driverId, updateDriver);
            //then
            Assert.fail("Driver should not be updated because of wrong last name provided");
        } catch (UpdateDriverValidationException ex) {
            Assert.assertTrue("Exception is thrown", true);
        } catch (Exception ex) {
            Assert.fail("invalid exception");
        }
    }

    @Test
    public void given_invalidDriver_when_deletingDriver_then_exceptionIsThrown() {
        //given
        DeleteDriverCommand deleteDriver = new DeleteDriverCommand(null);

        //when
        try {
            driverFacade.delete(deleteDriver);
            //then
            Assert.fail("Driver should not be deleted because of invalid driver id");
        } catch (DeleteDriverException ex) {
            Assert.assertTrue("Exception thrown", true);
        } catch (Exception ex) {
            Assert.fail("invalid exception");
        }
    }

}
