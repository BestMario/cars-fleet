package com.mario.carsfleet.driver.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mario.carsfleet.driver.dto.AddNewDriverCommand;
import com.mario.carsfleet.driver.dto.DeleteDriverCommand;
import com.mario.carsfleet.driver.dto.DriverStatusDto;
import com.mario.carsfleet.driver.dto.UpdateDriverCommand;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class DriverControllerTest {

    @Autowired
    private DriverFacade driverFacade;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void given_newDriverData_when_creatingNewDriver_then_driverIsCreated() throws Exception {
        //given
        AddNewDriverCommand newDriver = new AddNewDriverCommand("Jan", "Kowalski", 23);

        String newDriverCommandAsString = mapper.writer().writeValueAsString(newDriver);

        //when
        //then
        mockMvc.perform(post("/api/drivers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(newDriverCommandAsString))
                .andExpect(status().is(HttpStatus.CREATED.value()));
    }

    @Test
    public void given_invalidDriverData_when_addingNewDriver_then_driverIsNotAdded() throws Exception {
        //given
        AddNewDriverCommand newDriver = new AddNewDriverCommand("", "K", null);

        String newDriverCommandAsString = mapper.writer().writeValueAsString(newDriver);

        //when
        //then
        mockMvc.perform(post("/api/drivers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(newDriverCommandAsString))
                .andExpect(status().is(HttpStatus.UNPROCESSABLE_ENTITY.value()))
                .andExpect(jsonPath("$.firstNameErrors").value("Name is required"))
                .andExpect(jsonPath("$.lastNameErrors").value("Last name cannot contain less than 2 characters and more than 40"))
                .andExpect(jsonPath("$.ageErrors").value("Age is required"))
                .andDo(print());
    }

    @Test
    public void given_existingDriver_when_updatingDriver_then_driverIsUpdated() throws Exception {
        //given
        AddNewDriverCommand newDriver = new AddNewDriverCommand("Jan", "Kowalski", 23);
        Integer driverId = driverFacade.add(newDriver);
        UpdateDriverCommand updateDriver = new UpdateDriverCommand("Jan", "Kowalski", 23, DriverStatusDto.ACTIVE);

        String updateDriverCommandAsString = mapper.writer().writeValueAsString(updateDriver);

        //when
        //then
        mockMvc.perform(patch("/api/drivers/" + driverId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(updateDriverCommandAsString))
                .andExpect(status().is(HttpStatus.OK.value()));
    }

    @Test
    public void given_invalidDriverData_when_updatingDriver_then_driverIsNotUpdated() throws Exception {
        //given
        AddNewDriverCommand newDriver = new AddNewDriverCommand("Jan", "Kowalski", 23);
        Integer driverId = driverFacade.add(newDriver);
        UpdateDriverCommand updateDriver = new UpdateDriverCommand("", "", null, DriverStatusDto.ACTIVE);

        String updateDriverCommandAsString = mapper.writer().writeValueAsString(updateDriver);

        //when
        //then
        mockMvc.perform(patch("/api/drivers/" + driverId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(updateDriverCommandAsString))
                .andExpect(status().is(HttpStatus.UNPROCESSABLE_ENTITY.value()))
                .andExpect(jsonPath("$.firstNameErrors").value("Name is required"))
                .andExpect(jsonPath("$.lastNameErrors").value("Last name is required"))
                .andExpect(jsonPath("$.ageErrors").value("Age is required"))
                .andDo(print());
    }

    @Test
    public void given_existingCarData_when_deletingCar_then_carIsDeleted() throws Exception {
        //given
        AddNewDriverCommand newDriver = new AddNewDriverCommand("Jan", "Kowalski", 23);
        Integer driverId = driverFacade.add(newDriver);
        DeleteDriverCommand deleteDriverCommand = new DeleteDriverCommand(driverId);

        String deleteDriverCommandAsJson = mapper.writer().writeValueAsString(deleteDriverCommand);

        //when
        //then
        mockMvc.perform(delete("/api/drivers/" + driverId)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(deleteDriverCommandAsJson))
                .andExpect(status().is(HttpStatus.OK.value()));
    }
}
