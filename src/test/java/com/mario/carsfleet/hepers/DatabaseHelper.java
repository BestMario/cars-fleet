package com.mario.carsfleet.hepers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@Service
public class DatabaseHelper {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public DatabaseHelper(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void deleteAll() {
        jdbcTemplate.update("delete from fuel", new MapSqlParameterSource());
        jdbcTemplate.update("delete from car", new MapSqlParameterSource());
        jdbcTemplate.update("delete from car_driver", new MapSqlParameterSource());
        jdbcTemplate.update("delete from account where email_address != 'demo@demo.pl'", new MapSqlParameterSource());
        jdbcTemplate.update("delete from driver", new MapSqlParameterSource());
    }
}
