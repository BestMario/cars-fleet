package com.mario.carsfleet;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CarsFleetApplicationTests {

	@Test
	public void canaryTest() {
		Assert.assertTrue("test configuration failed", true);
	}
}
