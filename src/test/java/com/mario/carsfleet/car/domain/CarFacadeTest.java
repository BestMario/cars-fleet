package com.mario.carsfleet.car.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mario.carsfleet.car.dto.*;
import com.mario.carsfleet.driver.domain.DriverFacade;
import com.mario.carsfleet.driver.dto.AddNewDriverCommand;
import com.mario.carsfleet.hepers.DatabaseHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.hamcrest.Matchers.*;

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest
public class CarFacadeTest {

    @Autowired
    private CarFacade carFacade;

    @Autowired
    private DriverFacade driverFacade;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private DatabaseHelper databaseHelper;

    @Before
    public void doBeforeEachTests() {
        databaseHelper.deleteAll();
    }

    @Test
    public void  given_newCarData_when_addingNewCar_then_carIsAdded() {
        //given
        AddNewCarCommand newCarCommand = new AddNewCarCommand("BMW", "e60", 2010, "POS265441");

        //when
        Integer carId = carFacade.add(newCarCommand);

        //then
        CarDto car = carFacade.findById(carId);
        Assert.assertThat(car.getMake(), is(equalTo("BMW")));
    }

    @Test
    public void given_existingCarData_when_updatingCar_then_CarIsUpdated() {
        //given
        AddNewCarCommand newCarCommand = new AddNewCarCommand("BMW", "e60", 2010, "POS265441");
        Integer carId = carFacade.add(newCarCommand);
        UpdateCarCommand updateCarCommand = new UpdateCarCommand("BMW", "320", 2018, "LK2065487");

        //when
        carFacade.update(carId, updateCarCommand);

        //then
        CarDto car = carFacade.findById(carId);
        Assert.assertThat(car.getManufacturedYear(), is(equalTo(2018)));
    }

    @Test
    public void given_existingCarData_when_deletingCar_then_carIsDeleted() {
        //given
        AddNewCarCommand newCarCommand = new AddNewCarCommand("BMW", "e60", 2010, "POS265441");
        Integer carId = carFacade.add(newCarCommand);
        DeleteCarCommand deleteCarCommand = new DeleteCarCommand(carId);
        //when
        carFacade.delete(deleteCarCommand);

        //then
        try {
            carFacade.findById(carId);
            Assert.fail("car not exist, exception should be thrown");
        } catch (EmptyResultDataAccessException ex) {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void given_carDto_when_changingCarStatus_then_carStatusIsChanged() {
        //given
        AddNewCarCommand newCarCommand = new AddNewCarCommand("BMW", "e60", 2010, "POS265441");
        Integer carId = carFacade.add(newCarCommand);
        ChangeCarStatusCommand changeCarStatus = new ChangeCarStatusCommand(carId, CarStatusDto.DAMAGED);

        //when
        carFacade.changeCarStatus(changeCarStatus);

        //then
        CarDto car = carFacade.findById(carId);
        Assert.assertThat(car.getCarStatus(), is(equalTo(CarStatusDto.DAMAGED)));
    }

    @Test
    public void given_driverDto_when_addingDriverToCar_then_driverIsAddedToCar() {
        //given
        AddNewDriverCommand newDriver = new AddNewDriverCommand("Mark", "Nowak", 32);
        Integer driverId = driverFacade.add(newDriver);
        AddNewCarCommand newCarCommand = new AddNewCarCommand("BMW", "e60", 2010, "POS265441");
        Integer carId = carFacade.add(newCarCommand);

        AddDriverToCarCommand addDriverToCar = new AddDriverToCarCommand(carId, driverId);

        //when
        carFacade.addDriverToCar(addDriverToCar);

        //then
        CarDto car = carFacade.findById(carId);
        Assert.assertThat(car.getDriverId(), is(equalTo(driverId)));
    }

    @Test
    public void given_assignedDriverToCar_when_detachDriver_then_driverIsDetached() {
        //given
        AddNewDriverCommand newDriver = new AddNewDriverCommand("Mark", "Nowak", 32);
        Integer driverId = driverFacade.add(newDriver);
        AddNewCarCommand newCarCommand = new AddNewCarCommand("BMW", "e60", 2010, "POS265441");
        Integer carId = carFacade.add(newCarCommand);
        AddDriverToCarCommand driverToCarCommand = new AddDriverToCarCommand(carId, driverId);
        carFacade.addDriverToCar(driverToCarCommand);

        //when
        carFacade.detachDriverFromCar(new DetachDriverFromCarCommand(carId));
        //then
        CarDto car = carFacade.findById(carId);
        Assert.assertThat(car.getCurrentDriver(), is(notAssignedDriver()));

    }

    private CarDriverDto notAssignedDriver() {
        return new CarDriverDto(null, false, null, null);
    }

    @Test
    public void given_existingCarData_when_addingFuelDetails_then_fuelDetailIsAddedToCar() {
        //given
        AddNewCarCommand newCarCommand = new AddNewCarCommand("BMW", "e60", 2010, "POS265441");
        Integer carId = carFacade.add(newCarCommand);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate refuelingDate = LocalDate.parse("2000-06-20", formatter);
        AddFuelConsumptionCommand fuelConsumptionCommand = new AddFuelConsumptionCommand(refuelingDate, new BigDecimal("52.1"), FuelTypeFormOptions.DIESEL, new BigDecimal("250"), 100000L);

        //when
        Integer fuelId = carFacade.addFuelConsumption(carId, fuelConsumptionCommand);

        //then
        FuelDto fuel = carFacade.findFuelById(fuelId);
        Assert.assertThat(fuel, is(notNullValue()));
    }

    @Test
    public void given_existingFuelConsumptionData_when_updatingFuelConsumption_then_fuelConsumptionIsUpdated() {
        //given
        AddNewCarCommand newCarCommand = new AddNewCarCommand("BMW", "e60", 2010, "POS265441");
        Integer carId = carFacade.add(newCarCommand);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate refuelingDate = LocalDate.parse("2000-06-20", formatter);
        AddFuelConsumptionCommand fuelConsumptionCommand = new AddFuelConsumptionCommand(refuelingDate, new BigDecimal("52.1"), FuelTypeFormOptions.DIESEL, new BigDecimal("250"), 100000L);
        Integer fuelId = carFacade.addFuelConsumption(carId, fuelConsumptionCommand);
        UpdateFuelCommand updateFuelCommand = new UpdateFuelCommand(refuelingDate, new BigDecimal("52.1"), FuelTypeFormOptions.PETROL, new BigDecimal("300"), 100000L);

        //when
        carFacade.updateFuel(carId, fuelId, updateFuelCommand);

        //then
        FuelDto fuel = carFacade.findFuelById(fuelId);
        Assert.assertThat(fuel.getFuelType(), is(equalTo(updateFuelCommand.getFuelType())));
    }

    @Test
    public void given_invalidMake_when_addingNewCar_then_exceptionIsThrown() {
        //given
        AddNewCarCommand newCar = new AddNewCarCommand("d", "e60", 2010, "POS265441");

        //then
        try {
            carFacade.add(newCar);
            //when
            Assert.fail("New car should not be added because of wrong model value provided");
        } catch (NewCarValidationException ex) {
            Assert.assertTrue("Exeption is thrown", true);
        } catch (Exception ex) {
            Assert.fail("invalid exception");
        }
    }

    @Test
    public void given_invalidModel_when_updatingCar_then_exceptionIsThrown() {
        //given
        AddNewCarCommand newCar = new AddNewCarCommand("BMW", "e60", 2012, "POS265441");
        Integer carId = carFacade.add(newCar);
        UpdateCarCommand updateCar = new UpdateCarCommand("BMW", "e", 2012, "POS265441");

        //when
        try {
            carFacade.update(carId, updateCar);
            //then
            Assert.fail("Car should not be updated because of wrong model given");
        } catch (UpdateCarValidationException ex) {
            Assert.assertTrue("Exception is thrown", true);
        } catch (Exception ex) {
            Assert.fail("invalid exception");
        }
    }

    @Test
    public void given_invalidCarId_when_deletingCar_then_exceptionIsThrown() {
        //given
        AddNewCarCommand newCar = new AddNewCarCommand("BMW", "e60", 2012, "POS265441");
        carFacade.add(newCar);
        DeleteCarCommand deleteCar = new DeleteCarCommand(null);

        //when
        try {
            carFacade.delete(deleteCar);
            //then
            Assert.fail("Car should not be deleted because of invalid car Id value");
        } catch (DeleteCarValidationException ex) {
            Assert.assertTrue("Exception is thrown", true);
        } catch (Exception ex) {
            Assert.fail("invalid exception");
        }
    }
}
