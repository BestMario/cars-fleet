package com.mario.carsfleet.car.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mario.carsfleet.car.dto.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CarControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private CarFacade carFacade;

    @Test
    public void given_newCarData_when_addingCar_then_carIsAdded() throws Exception {
        //given
        AddNewCarCommand addNewCar = new AddNewCarCommand("BMW", "e60", 2010, "POS265441");
        String addNewCarCommandAsJson = mapper.writer().writeValueAsString(addNewCar);

        //when
        //then
        mockMvc.perform(post("/api/cars")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(addNewCarCommandAsJson))
                .andExpect(status().is(HttpStatus.CREATED.value()));
    }

    @Test
    public void given_invalidCarData_when_addingCar_then_carIsNotAdded() throws Exception {
        //given
        AddNewCarCommand addNewCar = new AddNewCarCommand("", "e", 201, "POS265441876");
        String addNewCarCommandAsJson = mapper.writer().writeValueAsString(addNewCar);

        //when
        //then
        mockMvc.perform(post("/api/cars")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(addNewCarCommandAsJson))
                .andExpect(status().is(HttpStatus.UNPROCESSABLE_ENTITY.value()))
                .andExpect(jsonPath("$.makeErrors").value("Make is required"))
                .andExpect(jsonPath("$.modelErrors").value("Model cannot contain less than 2 characters and more than 50"))
                .andExpect(jsonPath("$.manufacturedYearErrors").value("Manufactured year must be greater than 1950"))
                .andExpect(jsonPath("$.numberPlateErrors").value("Number plate cannot contain less than 3 and more than 11 characters"))
                .andDo(print());
    }

    @Test
    public void given_existingCarData_when_updatingCar_then_carIsUpdated() throws Exception {
        //given
        AddNewCarCommand addNewCar = new AddNewCarCommand("Audi", "A4", 2012, "POS265441");
        Integer carId = carFacade.add(addNewCar);
        UpdateCarCommand updateCar = new UpdateCarCommand("Audi", "A6", 2012, "POS265441");

        String updateCarCommandAsJson = mapper.writer().writeValueAsString(updateCar);

        //when
        //then
        mockMvc.perform(patch("/api/cars/" + carId.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(updateCarCommandAsJson))
                .andExpect(status().is(HttpStatus.OK.value()));
    }

    @Test
    public void given_invalidCarData_when_updatingCar_then_carIsNotUpdated() throws Exception {
        //given
        AddNewCarCommand addNewCar = new AddNewCarCommand("Opel", "Astra", 2017, "POS265441");
        Integer carId = carFacade.add(addNewCar);
        UpdateCarCommand updateCar = new UpdateCarCommand(" ", " ", 1949, "PO");

        String updateCarCommandAsJson = mapper.writer().writeValueAsString(updateCar);

        //when
        //then
        mockMvc.perform(patch("/api/cars/" + carId.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(updateCarCommandAsJson))
                .andExpect(jsonPath("$.makeErrors").value("Make is required"))
                .andExpect(jsonPath("$.modelErrors").value("Model is required"))
                .andExpect(jsonPath("$.manufacturedYearErrors").value("Manufactured year must be greater than 1950"))
                .andExpect(jsonPath("$.numberPlateErrors").value("Number plate cannot contain less than 3 and more than 11 characters"));
    }

    @Test
    public void given_existingCarData_when_deletingCar_then_carIsDeleted() throws Exception {
        //given
        AddNewCarCommand addNewCar = new AddNewCarCommand("Audi", "A4", 2012, "POS265441");
        Integer carId = carFacade.add(addNewCar);
        DeleteCarCommand deleteCarCommand = new DeleteCarCommand(carId);

        String deleteCarCommandAsJson = mapper.writer().writeValueAsString(deleteCarCommand);

        //when
        //then
        mockMvc.perform(delete("/api/cars/" + carId)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(deleteCarCommandAsJson))
                .andExpect(status().is(HttpStatus.OK.value()));
    }

    @Test
    public void given_newFuelData_when_addingNewFuel_then_fuelConsumptionIsAdded() throws Exception {
        //given
        AddNewCarCommand newCarCommand = new AddNewCarCommand("BMW", "e60", 2010, "POS265441");
        Integer carId = carFacade.add(newCarCommand);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate refuelingDate = LocalDate.parse("2000-06-20", formatter);
        AddFuelConsumptionCommand fuelConsumptionCommand = new AddFuelConsumptionCommand(refuelingDate, new BigDecimal("52.1"), FuelTypeFormOptions.DIESEL, new BigDecimal("250"), 100000L);
        String fuelConsumptionCommandAsString = mapper.writer().writeValueAsString(fuelConsumptionCommand);

        //when
        //then
        mockMvc.perform(post("/api/cars/" + carId + "/new-refueling")
                .contentType(MediaType.APPLICATION_JSON)
                .content(fuelConsumptionCommandAsString))
                .andExpect(status().is(HttpStatus.CREATED.value()));
    }

    @Test
    public void given_invalidFuelConsumptionData_when_addingNewFuelConsumption_then_fuelIsNotAdded() throws Exception {
        //given
        AddNewCarCommand newCarCommand = new AddNewCarCommand("BMW", "e60", 2010, "POS265441");
        Integer carId = carFacade.add(newCarCommand);
        AddFuelConsumptionCommand fuelConsumptionCommand = new AddFuelConsumptionCommand(null, new BigDecimal("1"), FuelTypeFormOptions.DIESEL, new BigDecimal("3000"), 6L);
        String fuelConsumptionCommandAsString = mapper.writer().writeValueAsString(fuelConsumptionCommand);

        //when
        //then
        mockMvc.perform(post("/api/cars/" + carId + "/new-refueling")
                .contentType(MediaType.APPLICATION_JSON)
                .content(fuelConsumptionCommandAsString))
                .andExpect(status().is(HttpStatus.UNPROCESSABLE_ENTITY.value()))
                .andExpect(jsonPath("$.amountInLitersErrors").value("Liters amount should not be less than 10 and more than 500"))
                .andExpect(jsonPath("$.refuelingDateErrors").value("Date is required"))
                .andExpect(jsonPath("$.priceInPlnErrors").value("Price value should not be less than 50 and more than 2500"))
                .andExpect(jsonPath("$.mileageInKmErrors").value("Mileage value should not be less than 1"))
                .andDo(print());
    }

    @Test
    public void given_existingFuelConsumption_when_deletingFuel_then_fuelConsumptionIsDeleted() throws Exception {
        //given
        AddNewCarCommand newCarCommand = new AddNewCarCommand("BMW", "e60", 2010, "POS265441");
        Integer carId = carFacade.add(newCarCommand);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate refuelingDate = LocalDate.parse("2000-06-20", formatter);
        AddFuelConsumptionCommand fuelConsumptionCommand = new AddFuelConsumptionCommand(refuelingDate, new BigDecimal("52.1"), FuelTypeFormOptions.DIESEL, new BigDecimal("250"), 100000L);
        Integer fuelId = carFacade.addFuelConsumption(carId, fuelConsumptionCommand);
        DeleteFuelHistoryCommand deleteFuelHistoryCommand = new DeleteFuelHistoryCommand(carId, fuelId);

        String fuelConsumptionCommandAsString = mapper.writer().writeValueAsString(deleteFuelHistoryCommand);

        //when
        //then
        mockMvc.perform(delete("/api/cars/" + carId + "/fuel-history/" + fuelId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(fuelConsumptionCommandAsString))
                .andExpect(status().is(HttpStatus.OK.value()));
    }
}