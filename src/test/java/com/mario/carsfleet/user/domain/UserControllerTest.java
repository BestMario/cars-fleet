package com.mario.carsfleet.user.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mario.carsfleet.hepers.DatabaseHelper;
import com.mario.carsfleet.user.dto.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    private UserFacade userFacade;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DatabaseHelper databaseHelper;

    @Before
    public void doBeforeEachTest() {
        databaseHelper.deleteAll();
    }

    @Test
    public void given_newUserData_when_addingNewUser_then_userIsAdded() throws Exception {
        //given
        AddNewUserCommand newUserCommand = new AddNewUserCommand("Jan", "Nowak", "jan@gmail.com", "janek");

        String newUserCommandAsString = mapper.writer().writeValueAsString(newUserCommand);

        //when
        //then
        mockMvc.perform(post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(newUserCommandAsString))
                .andExpect(status().is(HttpStatus.CREATED.value()));
    }

    @Test
    public void given_invalidUserData_when_addingNewUser_then_userIsNotAdded() throws Exception {
        //given
        AddNewUserCommand newUserCommand = new AddNewUserCommand("", "", "j", "j");

        String newUserCommandAsString = mapper.writer().writeValueAsString(newUserCommand);

        //when
        //then
        mockMvc.perform(post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(newUserCommandAsString))
                .andExpect(status().is(HttpStatus.UNPROCESSABLE_ENTITY.value()))
                .andExpect(jsonPath("$.firstNameError").value("First name is required"))
                .andExpect(jsonPath("$.lastNameError").value("Last name is required"))
                .andExpect(jsonPath("$.emailAddressError").value("Email address cannot contain less than 3 and more than 30 characters"))
                .andExpect(jsonPath("$.passwordError").value("Password cannot contain less than 3 and more than 30 characters"))
                .andDo(print());
    }

    @Test
    public void given_existingUserData_when_updatingUser_then_userIsUpdated() throws Exception {
        //given
        AddNewUserCommand newUserCommand = new AddNewUserCommand("Jan", "Kowalski", "jan@onet.pl", "janek");
        Integer userId = userFacade.add(newUserCommand);
        UpdateUserCommand updateUserCommand = new UpdateUserCommand("Karol", "Kowalski", "karol#o2.pl", "janek", RoleDto.SIMPLE_USER, UserStatusDto.UNBLOCKED);

        String updateUserCommandAsString = mapper.writer().writeValueAsString(updateUserCommand);

        //when
        //then
        mockMvc.perform(patch("/api/users/" + userId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(updateUserCommandAsString))
                .andExpect(status().is(HttpStatus.OK.value()));
    }

    @Test
    public void given_invalidUserData_when_updatingUser_then_userIsNotUpdated() throws Exception {
        //given
        AddNewUserCommand newUserCommand = new AddNewUserCommand("Jan", "Kowalski", "jan@onet.pl", "janek");
        Integer userId = userFacade.add(newUserCommand);
        UpdateUserCommand updateUserCommand = new UpdateUserCommand("K", "", "", "janek", RoleDto.SIMPLE_USER, UserStatusDto.UNBLOCKED);

        String updateUserCommandAsString = mapper.writer().writeValueAsString(updateUserCommand);

        //when
        //then
        mockMvc.perform(patch("/api/users/" + userId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(updateUserCommandAsString))
                .andExpect(status().is(HttpStatus.UNPROCESSABLE_ENTITY.value()))
                .andExpect(jsonPath("$.firstNameErrors").value("First name cannot contain less than 2 and more than 30 characters"))
                .andExpect(jsonPath("$.lastNameErrors").value("Last name is required"))
                .andExpect(jsonPath("$.emailAddressErrors").value("Email address is required"))
                .andDo(print());

    }

    @Test
    public void given_existingUser_when_deletingUser_then_userIsDeleted() throws Exception {
        //given
        AddNewUserCommand newUserCommand = new AddNewUserCommand("Jan", "Kowalski", "jan@onet.pl", "janek");
        Integer userId = userFacade.add(newUserCommand);
        DeleteUserCommand deleteUserCommand = new DeleteUserCommand(userId);

        String deleteUserCommandAsString = mapper.writer().writeValueAsString(deleteUserCommand);

        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/users/" + userId)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(deleteUserCommandAsString))
                .andExpect(status().is(HttpStatus.OK.value()));
    }

    @Test
    public void given_existingUserStatus_when_changingUserStatus_then_statusIsChanged() throws Exception {
        //given
        AddNewUserCommand newUserCommand = new AddNewUserCommand("Jan", "Kowalski", "jan@onet.pl", "janek");
        Integer userId = userFacade.add(newUserCommand);
        ChangeUserStatusCommand userStatusCommand = new ChangeUserStatusCommand(userId);

        String userStatusCommandAsString = mapper.writer().writeValueAsString(userStatusCommand);
        //when
        //then
        mockMvc.perform(patch("/api/users/" + userId + "/status")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userStatusCommandAsString))
                .andExpect(status().is(HttpStatus.OK.value()));
    }

    @Test
    public void given_demoUserData_when_deletingDemoUser_then_demoUserIsNotDeleted() throws Exception {
        //given
        Integer demoUserId = UserFacade.DEMO_USER_ID;
        DeleteUserCommand deleteUserCommand = new DeleteUserCommand(demoUserId);

        String deleteDemoUserAsString = mapper.writer().writeValueAsString(deleteUserCommand);

        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.delete(("/api/users/" + demoUserId))
                .contentType(MediaType.APPLICATION_JSON)
                .content(deleteDemoUserAsString))
                .andExpect(status().is(HttpStatus.UNPROCESSABLE_ENTITY.value()))
                .andExpect(jsonPath("$.userIdErrors").value("Demo user cannot be deleted"))
                .andDo(print());
    }
}
