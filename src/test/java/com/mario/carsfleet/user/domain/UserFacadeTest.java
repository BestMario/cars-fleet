package com.mario.carsfleet.user.domain;

import com.mario.carsfleet.hepers.DatabaseHelper;
import com.mario.carsfleet.user.dto.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserFacadeTest {

    @Autowired
    private DatabaseHelper databaseHelper;

    @Autowired
    private UserFacade userFacade;

    @Before
    public void doBeforeEachTests() {
        databaseHelper.deleteAll();
    }

    @Test
    public void given_newUserData_when_addingNewUser_then_newUserIsAdded() {
        //given
        AddNewUserCommand newUser = new AddNewUserCommand("Jan", "Nowak", "jan.nowak@gmail.com", "janek");

        //when
        Integer userId = userFacade.add(newUser);

        //then
        UserDto user = userFacade.findById(userId);
        UserDto expectedUser = new UserDto(userId, "Jan", "Nowak", "jan.nowak@gmail.com", RoleDto.SIMPLE_USER, UserStatusDto.UNBLOCKED);
        Assert.assertThat(user, is(equalTo(expectedUser)));
    }

    @Test
    public void given_validUserData_when_updatingUser_then_userIsUpdated() {
        //given
        AddNewUserCommand newUser = new AddNewUserCommand("Jan", "Nowak", "jan.nowak@gmail.com", "janek");
        Integer userId = userFacade.add(newUser);
        UpdateUserCommand updateUser = new UpdateUserCommand("Mark", "Hamilton", "mark.hamilton@gmail.com", "reksio", RoleDto.SIMPLE_USER, UserStatusDto.UNBLOCKED);

        //when
        userFacade.update(userId, updateUser);

        //then
        UserDto user = userFacade.findById(userId);
        UserDto expectedUser = new UserDto(userId, "Mark", "Hamilton", "mark.hamilton@gmail.com", RoleDto.SIMPLE_USER, UserStatusDto.UNBLOCKED);
        Assert.assertThat(user, is(equalTo(expectedUser)));
    }

    @Test
    public void given_validUserData_when_deletingUser_than_userIsDeleted() {
        //given
        AddNewUserCommand newUser = new AddNewUserCommand("Jan", "Nowak", "jan.nowak@gmail.com", "janek");
        Integer userId = userFacade.add(newUser);
        DeleteUserCommand deleteUser = new DeleteUserCommand(userId);

        //when
        userFacade.deleteById(deleteUser);

        //then
        try {
            UserDto user = userFacade.findById(userId);
            Assert.fail("user does not exist");
        } catch (EmptyResultDataAccessException ex) {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void given_invalidName_when_addingNewUser_then_exceptionIsThrown() {
        //given
        AddNewUserCommand newUser = new AddNewUserCommand("l", "Kowalski", "l.kowalski@o2.pl", "kowal");

        //when
        try {
            userFacade.add(newUser);
            //then
            Assert.fail("user should not be save because of wrong name given");
        } catch (NewUserValidationException ex) {
            Assert.assertTrue("validation exception true", true);
        } catch (Exception ex) {
            Assert.fail("invalid exception");
        }
    }

    @Test
    public void given_invalid_lastName_when_updatingNewUser_then_exceptionIsThrown() {
        //given
        AddNewUserCommand newUser = new AddNewUserCommand("Jan", "Kowalski", "j.kowalski@o2.pl", "kowal");
        Integer userId = userFacade.add(newUser);
        UpdateUserCommand updateUser = new UpdateUserCommand("Jan", "", "j.kowalski@o2.pl", "kowal", RoleDto.SIMPLE_USER, UserStatusDto.UNBLOCKED);

        //when
        try {
            userFacade.update(userId, updateUser);
            //then
            Assert.fail("User should not be updated because of wrong last name given");
        } catch (UpdateUserValidationException ex) {
            Assert.assertTrue("validation exception thrown", true);
        } catch (Exception ex) {
            Assert.fail("invalid exception");
        }
    }

    @Test
    public void given_invalidUserId_when_deletingUser_than_exceptionIsThrown() {
        //given
        AddNewUserCommand newUser = new AddNewUserCommand("Jan", "Nowak", "jan.nowak@gmail.com", "janek");
        Integer userId = userFacade.add(newUser);
        DeleteUserCommand deleteUser = new DeleteUserCommand(null);

        //when
        try {
            userFacade.deleteById(deleteUser);
            //then
            Assert.fail("user does not exist");
        } catch (DeleteValidateUserException delEx) {
            Assert.assertTrue("delete validation exception thrown", true);
        } catch (Exception ex) {
            Assert.fail("invalid exception");
        }
    }

    @Test
    public void given_existingUserStatus_when_changingUserStatus_then_statusIsChanged() {
        //given
        AddNewUserCommand newUser = new AddNewUserCommand("Jan", "Nowak", "jan.nowak@gmail.com", "janek");
        Integer userId = userFacade.add(newUser);
        ChangeUserStatusCommand changeUserStatusCommand = new ChangeUserStatusCommand(userId);

        //when
        userFacade.changeUserStatus(changeUserStatusCommand);

        //then
        UserDto user = userFacade.findById(userId);
        Assert.assertThat(user.getUserStatus(), is(equalTo(UserStatusDto.BLOCKED)));
    }

    @Test
    public void given_demoUserData_when_deletingDemoUser_then_exceptionIsThrown() {
        //given
        Integer demoUserId = UserFacade.DEMO_USER_ID;
        DeleteUserCommand deleteUserCommand = new DeleteUserCommand(demoUserId);

        //when
        try {
            userFacade.deleteById(deleteUserCommand);
            //then
            Assert.fail("user does not exist");
        } catch (DeleteValidateUserException delEx) {
            Assert.assertThat("Demo user cannot be deleted", is(equalTo(delEx.getDeleteValidateUser().getUserIdErrors())));
        } catch (Exception ex) {
            Assert.fail("invalid exception");
        }
    }

    @Test
    public void given_demoUserData_when_updatingDemoUser_then_exceptionIsThrown() {
        //given
        Integer demoUserId = UserFacade.DEMO_USER_ID;
        UpdateUserCommand updateUser = new UpdateUserCommand("Jan", "Kowalski", "j.kowalski@o2.pl", "kowal", RoleDto.SIMPLE_USER, UserStatusDto.UNBLOCKED);

        //when
        try {
            userFacade.update(demoUserId, updateUser);
            //then
            Assert.fail("demo user was updated");
        } catch (UpdateUserValidationException ex) {
            Assert.assertThat("demo user cannot be updated", is(equalTo(ex.getUpdateUserValidation().getDemoUpdateErrors())));
        } catch (Exception ex) {
            Assert.fail("invalid exception");
        }
    }

    @Test
    public void given_validUserData_when_updatingUserUsingDemoData_than_exceptionIsThrown() {
        //given
        AddNewUserCommand newUser = new AddNewUserCommand("Jan", "Kowalski", "j.kowalski@o2.pl", "kowal");
        Integer userId = userFacade.add(newUser);
        UpdateUserCommand updateUser = new UpdateUserCommand("a" + UserFacade.DEMO_FIRST_NAME.toUpperCase() + "b", UserFacade.DEMO_LAST_NAME, UserFacade.DEMO_EMAIL_ADDRESS, "demo", RoleDto.SIMPLE_USER, UserStatusDto.UNBLOCKED);

        //when
        try {
            userFacade.update(userId, updateUser);
            //then
            Assert.fail("demo user was updated");
        } catch (UpdateUserValidationException ex) {
            UpdateUserValidationDto expectedValidation = UpdateUserValidationDto.builder()
                    .firstNameErrors("demo cannot be use as first name")
                    .lastNameErrors("demo cannot be use as last name")
                    .emailAddressErrors("email address cannot contain demo").build();
            Assert.assertThat(ex.getUpdateUserValidation(), is(equalTo(expectedValidation)));
        } catch (Exception ex) {
            Assert.fail("invalid exception");
        }
    }

    @Test
    public void given_validDemoUserStatus_when_changingDemoStatus_then_exceptionIsThrown() {
        //given
        Integer demoUserId = UserFacade.DEMO_USER_ID;
        ChangeUserStatusCommand changeUserStatusCommand = new ChangeUserStatusCommand(demoUserId);

        //when
        try {
            userFacade.changeUserStatus(changeUserStatusCommand);
            //then
            Assert.fail("demo status was changed");
        } catch (UserStatusValidationException ex) {
            Assert.assertThat(ex.getDemoStatusValidation().getUserIdErrors(), is(equalTo("Status of demo user cannot be changed")));
        } catch (Exception ex) {
            Assert.fail("validation failed");
        }
    }
}
