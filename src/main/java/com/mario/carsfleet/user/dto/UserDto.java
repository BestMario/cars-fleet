package com.mario.carsfleet.user.dto;

import lombok.Value;

@Value
public class UserDto {
    private Integer id;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private RoleDto role;
    private UserStatusDto userStatus;
}
