package com.mario.carsfleet.user.dto;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

@Value
@Builder
@ToString
public class UpdateUserValidationDto {
    private String firstNameErrors;
    private String lastNameErrors;
    private String emailAddressErrors;
    private String demoUpdateErrors;

    public boolean isValid() {
        return StringUtils.isBlank(firstNameErrors) && StringUtils.isBlank(lastNameErrors) &&
                StringUtils.isBlank(emailAddressErrors) && StringUtils.isBlank(demoUpdateErrors);
    }
}
