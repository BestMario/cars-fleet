package com.mario.carsfleet.user.dto;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

@Value
@Builder
@ToString
public class NewUserValidationDto {
    private String firstNameError;
    private String lastNameError;
    private String emailAddressError;
    private String passwordError;

    public boolean isValid() {
        return StringUtils.isBlank(firstNameError) && StringUtils.isBlank(lastNameError) &&
                StringUtils.isBlank(emailAddressError) && StringUtils.isBlank(passwordError);
    }
}
