package com.mario.carsfleet.user.dto;

public class UpdateUserValidationException extends RuntimeException {
   private final UpdateUserValidationDto updateUserValidation;

    public UpdateUserValidationException(UpdateUserValidationDto updateUserValidation) {
        this.updateUserValidation = updateUserValidation;
    }

    public UpdateUserValidationDto getUpdateUserValidation() {
        return updateUserValidation;
    }
}
