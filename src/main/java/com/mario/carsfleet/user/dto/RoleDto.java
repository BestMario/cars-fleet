package com.mario.carsfleet.user.dto;

public enum RoleDto {
    ADMIN,
    SIMPLE_USER
}
