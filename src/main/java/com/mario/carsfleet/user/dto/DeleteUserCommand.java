package com.mario.carsfleet.user.dto;

import lombok.Value;

@Value
public class DeleteUserCommand {
    private Integer userId;
}
