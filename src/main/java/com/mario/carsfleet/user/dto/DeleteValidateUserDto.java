package com.mario.carsfleet.user.dto;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

@Value
@Builder
@ToString
public class DeleteValidateUserDto {
    private String userIdErrors;

    public boolean isValid() {
        return StringUtils.isBlank(userIdErrors);
    }
}
