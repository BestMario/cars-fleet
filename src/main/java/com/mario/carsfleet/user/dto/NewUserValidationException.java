package com.mario.carsfleet.user.dto;

public class NewUserValidationException extends RuntimeException{
    private final NewUserValidationDto newUserValidation;

    public NewUserValidationException(NewUserValidationDto newUserValidation) {
        this.newUserValidation = newUserValidation;
    }

    public NewUserValidationDto getNewUserValidation() {
        return newUserValidation;
    }
}
