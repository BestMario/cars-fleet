package com.mario.carsfleet.user.dto;

public class DeleteValidateUserException extends RuntimeException {
    private final DeleteValidateUserDto deleteValidateUser;

    public DeleteValidateUserException(DeleteValidateUserDto deleteValidateUser) {
        this.deleteValidateUser = deleteValidateUser;
    }

    public DeleteValidateUserDto getDeleteValidateUser() {
        return deleteValidateUser;
    }
}
