package com.mario.carsfleet.user.dto;

import lombok.Value;

@Value
public class UpdateUserCommand {
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String password;
    private RoleDto role;
    private UserStatusDto userStatus;
}
