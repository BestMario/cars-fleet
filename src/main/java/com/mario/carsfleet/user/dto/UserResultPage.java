package com.mario.carsfleet.user.dto;

import lombok.Value;

import java.util.List;

@Value
public class UserResultPage {
    private List<UserDto> users;
    private Integer pageNumber;
    private Integer numberOfPages;
    private Integer pageSize;
    private String filteringText;
}
