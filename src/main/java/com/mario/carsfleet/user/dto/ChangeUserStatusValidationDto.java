package com.mario.carsfleet.user.dto;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

@Value
@ToString
@Builder
public class ChangeUserStatusValidationDto {

    private String userIdErrors;

    public boolean isValid() {
        return StringUtils.isBlank(userIdErrors);
    }
}
