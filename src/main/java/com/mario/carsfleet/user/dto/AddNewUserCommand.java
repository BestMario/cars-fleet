package com.mario.carsfleet.user.dto;

import lombok.Value;

@Value
public class AddNewUserCommand {
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String password;
}
