package com.mario.carsfleet.user.dto;

import lombok.Value;

@Value
public class ChangeUserStatusCommand {
    private Integer userId;
}
