package com.mario.carsfleet.user.dto;

public class UserStatusValidationException extends RuntimeException {
    private final ChangeUserStatusValidationDto demoStatusValidation;

    public UserStatusValidationException(ChangeUserStatusValidationDto demoStatusValidation) {
        this.demoStatusValidation = demoStatusValidation;
    }

    public ChangeUserStatusValidationDto getDemoStatusValidation() {
        return demoStatusValidation;
    }
}
