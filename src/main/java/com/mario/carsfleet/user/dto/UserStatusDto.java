package com.mario.carsfleet.user.dto;

public enum UserStatusDto {
    BLOCKED,
    UNBLOCKED
}
