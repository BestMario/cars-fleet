package com.mario.carsfleet.user;

import com.mario.carsfleet.user.domain.UserFacade;
import com.mario.carsfleet.user.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

//@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    private final UserFacade userFacade;

    @Autowired
    public UserController(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @GetMapping(value = "/api/users", params = "page-number")
    public UserResultPage findAll(@RequestParam(name = "page-number", defaultValue = "1") Integer pageNumber,
                                  @RequestParam(name = "filter", required = false, defaultValue = "") String filteringText) {
        return userFacade.findAllBy(pageNumber, filteringText);
    }

    @DeleteMapping("/api/users/{id}")
    public void delete(@PathVariable(name = "id") Integer userId) {
        DeleteUserCommand deleteUser = new DeleteUserCommand(userId);
        userFacade.deleteById(deleteUser);
    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping("/api/users")
    public void add(@RequestBody AddNewUserCommand newUser) {
        userFacade.add(newUser);
    }

    @GetMapping("/api/users/{id}")
    public UserDto findById(@PathVariable(name = "id") Integer userId) {
        return userFacade.findById(userId);
    }

    @PatchMapping("/api/users/{id}")
    public void update(@PathVariable(name = "id") Integer userId, @RequestBody UpdateUserCommand updateUser) {
        userFacade.update(userId, updateUser);
    }

    @PatchMapping("/api/users/{id}/status")
    public void changeUserStatus(@PathVariable(name = "id") Integer userId) {
        ChangeUserStatusCommand command = new ChangeUserStatusCommand(userId);
        userFacade.changeUserStatus(command);
    }

    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(value = {NewUserValidationException.class})
    public NewUserValidationDto handlerNewUserValidationException(NewUserValidationException ex) {
        logger.info("validation exception", ex);
        return ex.getNewUserValidation();
    }

    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(value = {UpdateUserValidationException.class})
    public UpdateUserValidationDto handlerUpdateValidationException(UpdateUserValidationException ex) {
        logger.info("validation exception", ex);
        return ex.getUpdateUserValidation();
    }

    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(value = {DeleteValidateUserException.class})
    public DeleteValidateUserDto handlerDeleteUserValidationException(DeleteValidateUserException ex) {
        logger.info("validation exception", ex);
        return ex.getDeleteValidateUser();
    }

    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(value = {UserStatusValidationException.class})
    public ChangeUserStatusValidationDto handleUserStatusValidationException(UserStatusValidationException ex) {
        logger.info("validation exception", ex);
        return ex.getDemoStatusValidation();
    }
}
