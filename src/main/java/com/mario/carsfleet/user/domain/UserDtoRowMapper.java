package com.mario.carsfleet.user.domain;

import com.mario.carsfleet.user.dto.RoleDto;
import com.mario.carsfleet.user.dto.UserDto;
import com.mario.carsfleet.user.dto.UserStatusDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

class UserDtoRowMapper implements RowMapper<UserDto> {
    @Override
    public UserDto mapRow(ResultSet rs, int rowNum) throws SQLException {
        Integer userId = rs.getInt("id");
        String firstName = rs.getString("first_name");
        String lastName = rs.getString("last_name");
        String emailAddress = rs.getString("email_address");
        RoleDto role = readUserRole(rs);
        UserStatusDto userStatus = readUserStatus(rs);
        return new UserDto(userId, firstName, lastName, emailAddress, role, userStatus);
    }

    private UserStatusDto readUserStatus(ResultSet rs) throws SQLException {
        String rawUserStatus = rs.getString("user_status");
        if (StringUtils.isNotBlank(rawUserStatus)) {
            UserStatus userStatus = UserStatus.valueOf(rawUserStatus);
            return UserStatus.convert(userStatus);
        }
        return null;
    }

    private RoleDto readUserRole(ResultSet rs) throws SQLException {
        String rawRole = rs.getString("roll");
        if (StringUtils.isNotBlank(rawRole)) {
            Role role = Role.valueOf(rawRole);
            return Role.convert(role);
        }
        return null;
    }
}
