package com.mario.carsfleet.user.domain;

import org.springframework.data.repository.CrudRepository;

interface UserRepository extends CrudRepository<User, Integer> {
}
