package com.mario.carsfleet.user.domain;

import com.mario.carsfleet.user.dto.UserDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserFinder {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public UserFinder(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public UserDto findById(Integer userId) {
        String query = "select id, first_name, last_name, email_address, roll, user_status from account where id=(:userId)";
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("userId", userId);
        UserDtoRowMapper rowMapper = new UserDtoRowMapper();
        return jdbcTemplate.queryForObject(query, namedParameter, rowMapper);
    }

    public List<UserDto> findAllBy(Integer pageNumber, Integer pageSize, String filteringText) {
        final String query = prepareQueryFindUsersBy(filteringText);
        Integer pageIndex = (pageNumber - 1) * pageSize;
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("pageIndex", pageIndex);
        namedParameter.addValue("pageSize", pageSize);
        namedParameter.addValue("filteringText", "%" + filteringText + "%");
        UserDtoRowMapper rowMapper = new UserDtoRowMapper();
        return jdbcTemplate.query(query, namedParameter, rowMapper);
    }

    private String prepareQueryFindUsersBy(String filteringText) {
        final String query;
        if (StringUtils.isBlank(filteringText)) {
            query = "select id, first_name, last_name, email_address, roll, user_status from account Limit :pageIndex, :pageSize";
        } else {
            query = "select id, first_name, last_name, email_address, roll, user_status from account where (first_name like :filteringText or last_name like :filteringText or email_address like :filteringText) Limit :pageIndex, :pageSize";
        }
        return query;
    }

    public Integer count(String filteringText) {
        String query = prepareQueryCountUsersBy(filteringText);
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("filteringText", "%" + filteringText + "%");
        return jdbcTemplate.queryForObject(query, namedParameter, Integer.class);
    }

    private String prepareQueryCountUsersBy(String filteringText) {
        final String query;
        if (StringUtils.isBlank(filteringText)) {
            query = "select count(1) from account";
        } else {
            query = "select count(1) from account where first_name like :filteringText or last_name like :filteringText or email_address like :filteringText";
        }
        return query;
    }

    boolean isEmailExists(String emailAddress) {
        String query = "select count(1) from account where email_address like :emailAddress";
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("emailAddress", emailAddress);
        Integer count = jdbcTemplate.queryForObject(query, namedParameter, Integer.class);
        return count > 0;
    }

    boolean isEmailExistsExcept(String emailAddress, Integer exceptUserId) {
        String query = "select count(1) from account where email_address like :emailAddress and id !=(:userId)";
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("emailAddress", emailAddress);
        namedParameter.addValue("userId", exceptUserId.toString());
        Integer count = jdbcTemplate.queryForObject(query, namedParameter, Integer.class);
        return count > 0;
    }
}
