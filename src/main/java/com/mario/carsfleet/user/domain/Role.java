package com.mario.carsfleet.user.domain;

import com.mario.carsfleet.user.dto.RoleDto;

enum Role {
    ADMIN,
    SIMPLE_USER;

    static RoleDto convert(Role role) {
        if (Role.ADMIN.equals(role)) {
            return RoleDto.ADMIN;
        } else if (Role.SIMPLE_USER.equals(role)) {
            return RoleDto.SIMPLE_USER;
        }
        return null;
    }

    static Role convert(RoleDto roleDto) {
        if (RoleDto.ADMIN.equals(roleDto)) {
            return Role.ADMIN;
        } else if (RoleDto.SIMPLE_USER.equals(roleDto)) {
            return Role.SIMPLE_USER;
        }
        return null;
    }
}
