//package com.mario.carsfleet.user.domain;
//
//import com.mario.carsfleet.boot.BCryptPasswordEncoder;
//import com.mario.carsfleet.boot.PasswordEncoder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//class UserConfiguration {
//
//    @Bean
//    public PasswordEncoder passwordEncoder() {
//        return new BCryptPasswordEncoder();
//    }
//
//}
