package com.mario.carsfleet.user.domain;

import com.mario.carsfleet.user.dto.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.mario.carsfleet.boot.PageSettings.PAGE_SIZE;

@Service
public class UserFacade {

    public static final Integer DEMO_USER_ID = 12345;
    public static final String DEMO_FIRST_NAME = "demo";
    public static final String DEMO_LAST_NAME = "demo";
    public static final String  DEMO_EMAIL_ADDRESS = "demo@demo.pl";


    private final UserRepository userRepository;
    private final UserFinder userFinder;
    private final PasswordEncoder passwordEncoder;
    private final UserValidator userValidator;

    @Autowired
    public UserFacade(UserRepository userRepository, UserFinder userFinder, PasswordEncoder passwordEncoder, UserValidator userValidator) {
        this.userRepository = userRepository;
        this.userFinder = userFinder;
        this.passwordEncoder = passwordEncoder;
        this.userValidator = userValidator;
    }

    public Integer add(AddNewUserCommand newUser) {

        NewUserValidationDto validation = userValidator.validateNewUser(newUser);
        if (!validation.isValid()) {
            throw new NewUserValidationException(validation);
        }

        User user = new User();
        user.setFirstName(newUser.getFirstName());
        user.setLastName(newUser.getLastName());
        user.setEmailAddress(newUser.getEmailAddress());

        String encodedPassword = passwordEncoder.encode(newUser.getPassword());
        user.setPassword(encodedPassword);
        user.setRole(Role.SIMPLE_USER);
        user.setUserStatus(UserStatus.UNBLOCKED);

        userRepository.save(user);
        return user.getId();
    }

    public UserDto findById(Integer userId) {
        return userFinder.findById(userId);
    }

    public void update(Integer userId, UpdateUserCommand updateUser) {

        UpdateUserValidationDto validation = userValidator.validateUpdateUser(updateUser, userId);
        if (!validation.isValid()) {
            throw new UpdateUserValidationException(validation);
        }
        Optional<User> user = userRepository.findById(userId);
        if (user.isPresent()) {
            User u = user.get();
            u.setFirstName(updateUser.getFirstName());
            u.setLastName(updateUser.getLastName());
            u.setEmailAddress(updateUser.getEmailAddress());

            if (StringUtils.isNotBlank(updateUser.getPassword())) {

                String encodedPassword = passwordEncoder.encode(updateUser.getPassword());
                u.setPassword(encodedPassword);
            }

            u.setRole(Role.convert(updateUser.getRole()));
            u.setUserStatus(UserStatus.convert(updateUser.getUserStatus()));
            userRepository.save(u);
        }
    }

    public void deleteById(DeleteUserCommand deleteUser) {

        DeleteValidateUserDto validate = userValidator.validateDeleteUser(deleteUser);
        if (!validate.isValid()) {
            throw new DeleteValidateUserException(validate);
        }

        userRepository.deleteById(deleteUser.getUserId());
    }

    public UserResultPage findAllBy(Integer pageNumber, String filteringText) {
        List<UserDto> users = userFinder.findAllBy(pageNumber, PAGE_SIZE, filteringText);
        Integer count = userFinder.count(filteringText);
        Integer numberOfPages = (int) Math.ceil((double) count / (double) PAGE_SIZE);
        return new UserResultPage(users, pageNumber, numberOfPages, PAGE_SIZE, filteringText);
    }

    public void changeUserStatus(ChangeUserStatusCommand command) {
        ChangeUserStatusValidationDto validation = userValidator.validateUserStatus(command.getUserId());
        if (!validation.isValid()) {
            throw new UserStatusValidationException(validation);
        }
        Optional<User> uId = userRepository.findById(command.getUserId());
        if (uId.isPresent()) {
            uId.ifPresent(user -> saveUserStatus(user));
        } else {
            throw new IllegalArgumentException("user not found");
        }
//        uId.ifPresentOrElse(user -> saveUserStatus(user), () -> {
//
//        });
    }

    private void saveUserStatus(User user) {

        UserStatus userStatus = user.getUserStatus();
        if (UserStatus.UNBLOCKED.equals(userStatus)) {
            user.setUserStatus(UserStatus.BLOCKED);
            userRepository.save(user);
        } else if (UserStatus.BLOCKED.equals(userStatus)) {
            user.setUserStatus(UserStatus.UNBLOCKED);
            userRepository.save(user);
        }
    }
}
