package com.mario.carsfleet.user.domain;

import com.mario.carsfleet.user.dto.UserStatusDto;

enum UserStatus {
    BLOCKED,
    UNBLOCKED;

    static UserStatusDto convert(UserStatus userStatus) {
     if (UserStatus.UNBLOCKED.equals(userStatus)) {
         return UserStatusDto.UNBLOCKED;
     } else if (UserStatus.BLOCKED.equals(userStatus)) {
         return UserStatusDto.BLOCKED;
     }
     return null;
    }

    static UserStatus convert(UserStatusDto userStatus) {
        if (UserStatusDto.UNBLOCKED.equals(userStatus)) {
            return UserStatus.UNBLOCKED;
        } else if (UserStatusDto.BLOCKED.equals(userStatus)) {
            return UserStatus.BLOCKED;
        }
        return null;
    }
}

