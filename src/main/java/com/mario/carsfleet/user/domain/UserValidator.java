package com.mario.carsfleet.user.domain;

import com.mario.carsfleet.user.dto.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class UserValidator {

    private final UserFinder userFinder;

    @Autowired
    public UserValidator(UserFinder userFinder) {
        this.userFinder = userFinder;
    }

    NewUserValidationDto validateNewUser(AddNewUserCommand newUser) {
        NewUserValidationDto.NewUserValidationDtoBuilder validationBuilder = NewUserValidationDto.builder();

        String firstName = newUser.getFirstName();
        validateFirstName(validationBuilder, firstName);

        String lastName = newUser.getLastName();
        validateLastName(validationBuilder, lastName);

        String emailAddress = newUser.getEmailAddress();
        validateEmailAddress(validationBuilder, emailAddress);

        String password = newUser.getPassword();
        validatePassword(validationBuilder, password);

        return validationBuilder.build();
    }

    private void validatePassword(NewUserValidationDto.NewUserValidationDtoBuilder validationBuilder, String password) {
        if (StringUtils.isBlank(password)) {
            validationBuilder.passwordError("Password is required");
        } else if (password.length() < 3 || password.length() > 30) {
            validationBuilder.passwordError("Password cannot contain less than 3 and more than 30 characters");
        }
    }

    private void validateEmailAddress(NewUserValidationDto.NewUserValidationDtoBuilder validationBuilder, String emailAddress) {
        if (StringUtils.isBlank(emailAddress)) {
            validationBuilder.emailAddressError("Email address is required");
        } else if (emailAddress.length() < 3 || emailAddress.length() > 30) {
            validationBuilder.emailAddressError("Email address cannot contain less than 3 and more than 30 characters");
        } else if (userFinder.isEmailExists(emailAddress)) {
            validationBuilder.emailAddressError("Email address already exist");
        }
    }

    private void validateLastName(NewUserValidationDto.NewUserValidationDtoBuilder validationBuilder, String lastName) {
        if (StringUtils.isBlank(lastName)) {
            validationBuilder.lastNameError("Last name is required");
        } else if (lastName.length() <= 2 || lastName.length() >= 30) {
            validationBuilder.lastNameError("Last name cannot contain less than 2 and more than 30 characters");
        }
    }

    private void validateFirstName(NewUserValidationDto.NewUserValidationDtoBuilder validationBuilder, String firstName) {
        if (StringUtils.isBlank(firstName)) {
            validationBuilder.firstNameError("First name is required");
        } else if (firstName.length() <= 2 || firstName.length() >= 30) {
            validationBuilder.firstNameError("First name cannot contain less than 2 and more than 30 characters");
        }
    }

    UpdateUserValidationDto validateUpdateUser(UpdateUserCommand updateUser, Integer userId) {
        UpdateUserValidationDto.UpdateUserValidationDtoBuilder validationBuilder = UpdateUserValidationDto.builder();

        validateUserId(validationBuilder, userId);

        String firstName = updateUser.getFirstName();
        validateUserFirstName(validationBuilder, firstName);

        String lastName = updateUser.getLastName();
        validateUserLastName(validationBuilder, lastName);

        String emailAddress = updateUser.getEmailAddress();
        validateUserEmailAddress(validationBuilder, emailAddress, userId);

        return validationBuilder.build();
    }

    private void validateUserId(UpdateUserValidationDto.UpdateUserValidationDtoBuilder validationBuilder, Integer userId) {
        if (isUserDemo(userId)) {
            validationBuilder.demoUpdateErrors("demo user cannot be updated");
        }
    }

    private void validateUserEmailAddress(UpdateUserValidationDto.UpdateUserValidationDtoBuilder validationBuilder, String emailAddress, Integer userId) {
        if (StringUtils.isBlank(emailAddress)) {
            validationBuilder.emailAddressErrors("Email address is required");
        } else if (emailAddress.length() < 2 || emailAddress.length() > 30) {
            validationBuilder.emailAddressErrors("Email address cannot contain less than 2 and more than 30 characters");
        } else if (StringUtils.containsIgnoreCase(emailAddress, UserFacade.DEMO_EMAIL_ADDRESS)) {
            validationBuilder.emailAddressErrors("email address cannot contain demo");
        } else if (userFinder.isEmailExistsExcept(emailAddress, userId)) {
            validationBuilder.emailAddressErrors("Email address already exist");
        }
    }

    private void validateUserLastName(UpdateUserValidationDto.UpdateUserValidationDtoBuilder validationBuilder, String lastName) {
        if (StringUtils.isBlank(lastName)) {
            validationBuilder.lastNameErrors("Last name is required");
        } else if (lastName.length() < 2 || lastName.length() > 30) {
            validationBuilder.lastNameErrors("Last name cannot contain less than 2 and more than 30 characters");
        } else if (StringUtils.containsIgnoreCase(lastName, UserFacade.DEMO_LAST_NAME)) {
            validationBuilder.lastNameErrors("demo cannot be use as last name");
        }
    }

    private void validateUserFirstName(UpdateUserValidationDto.UpdateUserValidationDtoBuilder
                                               validationBuilder, String firstName) {
        if (StringUtils.isBlank(firstName)) {
            validationBuilder.firstNameErrors("First name is required");
        } else if (firstName.length() < 2 || firstName.length() > 30) {
            validationBuilder.firstNameErrors("First name cannot contain less than 2 and more than 30 characters");
        } else if (StringUtils.containsIgnoreCase(firstName, UserFacade.DEMO_FIRST_NAME)) {
            validationBuilder.firstNameErrors("demo cannot be use as first name");
        }
    }

    DeleteValidateUserDto validateDeleteUser(DeleteUserCommand deleteUser) {
        DeleteValidateUserDto.DeleteValidateUserDtoBuilder validationBuilder = DeleteValidateUserDto.builder();

        Integer userId = deleteUser.getUserId();
        validateUserDelete(validationBuilder, userId);

        return validationBuilder.build();
    }

    private void validateUserDelete(DeleteValidateUserDto.DeleteValidateUserDtoBuilder validationBuilder, Integer userId) {
        if (Objects.isNull(userId)) {
            validationBuilder.userIdErrors("User id does not exist");
        } else if (isUserDemo(userId)) {
            validationBuilder.userIdErrors("Demo user cannot be deleted");
        }
    }

    public ChangeUserStatusValidationDto validateUserStatus(Integer userId) {
        ChangeUserStatusValidationDto.ChangeUserStatusValidationDtoBuilder validationDtoBuilder = ChangeUserStatusValidationDto.builder();

        validateIfNotUserDemo(validationDtoBuilder, userId);

        return validationDtoBuilder.build();
    }

    private void validateIfNotUserDemo(ChangeUserStatusValidationDto.ChangeUserStatusValidationDtoBuilder validationDtoBuilder, Integer userId) {

        if (isUserDemo(userId)) {
            validationDtoBuilder.userIdErrors("Status of demo user cannot be changed");
        }
    }

    private boolean isUserDemo(Integer userId) {
        return userId.equals(UserFacade.DEMO_USER_ID);
    }
}
