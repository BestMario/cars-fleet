package com.mario.carsfleet.boot.jwt;

import com.mario.carsfleet.login.domain.LoginDto;
import com.mario.carsfleet.login.domain.LoginFinder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class JwtUserDetailsService implements UserDetailsService {

  private final LoginFinder loginFinder;


  public JwtUserDetailsService(LoginFinder loginFinder) {
    this.loginFinder = loginFinder;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    LoginDto login = loginFinder.findByEmail(username);
    if (Objects.isNull(login)) {
      throw new UsernameNotFoundException("User not found: " + username);
    }
    return new JwtUserDetails((long) login.getId(), login.getEmail(), login.getPassword(), login.getRole());
  }

}