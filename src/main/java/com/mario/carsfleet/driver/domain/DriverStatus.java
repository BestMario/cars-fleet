package com.mario.carsfleet.driver.domain;

import com.mario.carsfleet.driver.dto.DriverStatusDto;

enum DriverStatus {
    ACTIVE,
    INACTIVE;

    static DriverStatus convert(DriverStatusDto driverStatusDto) {
        if (DriverStatusDto.ACTIVE.equals(driverStatusDto)) {
            return DriverStatus.ACTIVE;
        } else if (DriverStatusDto.INACTIVE.equals(driverStatusDto)) {
            return DriverStatus.INACTIVE;
        }
        return null;
    }

    static DriverStatusDto convert(DriverStatus driverStatus) {
        if (DriverStatus.ACTIVE.equals(driverStatus)) {
            return DriverStatusDto.ACTIVE;
        } else if (DriverStatus.INACTIVE.equals(driverStatus)) {
            return DriverStatusDto.INACTIVE;
        }
        return null;
    }
}
