package com.mario.carsfleet.driver.domain;

import com.mario.carsfleet.driver.dto.DriverDto;
import com.mario.carsfleet.driver.dto.DriverStatusDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

class DriverDtoRowMapper implements RowMapper<DriverDto> {
    @Override
    public DriverDto mapRow(ResultSet rs, int rowNum) throws SQLException {
        Integer driverId = rs.getInt("id");
        String firstName = rs.getString("first_name");
        String lastName = rs.getString("last_name");
        int age = rs.getInt("age");
        DriverStatusDto driverStatus = readDriverStatusOptions(rs);
        return new DriverDto(driverId, firstName, lastName, age, driverStatus);
    }

    private DriverStatusDto readDriverStatusOptions(ResultSet rs) throws SQLException {
        String rawDriverStatus = rs.getString("driver_status");
        if (StringUtils.isNotBlank(rawDriverStatus)) {
            DriverStatus driverStatus = DriverStatus.valueOf(rawDriverStatus);
            return DriverStatus.convert(driverStatus);
        }
        return null;
    }
}
