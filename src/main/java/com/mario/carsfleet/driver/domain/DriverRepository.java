package com.mario.carsfleet.driver.domain;

import org.springframework.data.repository.CrudRepository;

interface DriverRepository extends CrudRepository<Driver, Integer> {
}
