package com.mario.carsfleet.driver.domain;

import com.mario.carsfleet.driver.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static com.mario.carsfleet.boot.PageSettings.PAGE_SIZE;

@Service
public class DriverFacade {

    private final DriverRepository driverRepository;
    private final DriverFinder driverFinder;
    private final DriverEventPublisher driverEventPublisher;
    private final DriverValidator driverValidator;

    @Autowired
    public DriverFacade(DriverRepository driverRepository, DriverFinder driverFinder, DriverEventPublisher driverEventPublisher, DriverValidator driverValidator) {
        this.driverRepository = driverRepository;
        this.driverFinder = driverFinder;
        this.driverEventPublisher = driverEventPublisher;
        this.driverValidator = driverValidator;
    }

    @Transactional
    public Integer add(AddNewDriverCommand newDriver) {

        NewDriverValidationDto validation = driverValidator.validateNewDriver(newDriver);
        if (!validation.isValid()) {
            throw new NewDriverValidationException(validation);
        }

        Driver driver = new Driver();
        driver.setFirstName(newDriver.getFirstName());
        driver.setLastName(newDriver.getLastName());
        driver.setAge(newDriver.getAge());
        driver.setDriverStatus(DriverStatus.ACTIVE);
        driverRepository.save(driver);
        driverEventPublisher.publishDriver(driver.getId(), driver.getFirstName(), driver.getLastName(), driver.getAge());
        return driver.getId();
    }

    public DriverDto findById(Integer driverId) {
        return driverFinder.findById(driverId);
    }

    @Transactional
    public void update(Integer driverId, UpdateDriverCommand updateDriver) {

        UpdateDriverValidationDto validation = driverValidator.validationUpdateDriver(updateDriver);
        if (!validation.isValid()) {
            throw new UpdateDriverValidationException(validation);
        }

        Optional<Driver> driver = driverRepository.findById(driverId);
        if (driver.isPresent()) {
            Driver d = driver.get();
            d.setFirstName(updateDriver.getFirstName());
            d.setLastName(updateDriver.getLastName());
            d.setAge(updateDriver.getAge());
            d.setDriverStatus(DriverStatus.convert(updateDriver.getDriverStatus()));
            driverRepository.save(d);
            driverEventPublisher.publishDriver(d.getId(), d.getFirstName(), d.getLastName(), d.getAge());

        }
    }

    @Transactional
    public void delete(DeleteDriverCommand deleteDriver) {

        DeleteDriverValidationDto validation = driverValidator.validateDeleteDriver(deleteDriver);
        if (!validation.isValid()) {
            throw new DeleteDriverException(validation);
        }

        driverRepository.deleteById(deleteDriver.getDriverId());
        driverEventPublisher.publishDriverDeleted(deleteDriver.getDriverId());
    }

    public DriverResultPage findBy(Integer pageNumber, String filteringText) {
        List<DriverDto> drivers = driverFinder.findBy(pageNumber, PAGE_SIZE, filteringText);
        Integer count = driverFinder.count(filteringText);
        Integer numberOfPages = (int) Math.ceil((double) count / (double) PAGE_SIZE);
        return new DriverResultPage(drivers, pageNumber, numberOfPages, PAGE_SIZE, count, filteringText);
    }

    public void changeDriverStatus(ChangeDriverStatusCommand changeDriverStatus) {
        Optional<Driver> d = driverRepository.findById(changeDriverStatus.getDriverId());
        if (d.isPresent()) {
            Driver driver = d.get();
            driver.setDriverStatus(DriverStatus.convert(changeDriverStatus.getDriverStatusDto()));
            driverRepository.save(driver);
        }
    }
}
