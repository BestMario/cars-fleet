package com.mario.carsfleet.driver.domain;

import com.mario.carsfleet.driver.dto.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class DriverValidator {

    NewDriverValidationDto validateNewDriver(AddNewDriverCommand newDriver) {
        NewDriverValidationDto.NewDriverValidationDtoBuilder validationBuilder = NewDriverValidationDto.builder();

        String firstName = newDriver.getFirstName();
        validateFirstName(validationBuilder, firstName);

        String lastName = newDriver.getLastName();
        validateLastName(validationBuilder, lastName);

        Integer age = newDriver.getAge();
        validateAge(validationBuilder, age);

        return validationBuilder.build();
    }

    private void validateAge(NewDriverValidationDto.NewDriverValidationDtoBuilder validationBuilder, Integer age) {
        if (Objects.isNull(age)) {
            validationBuilder.ageErrors("Age is required");
        } else if (age < 18 || age > 100) {
            validationBuilder.ageErrors("Age needs to be between 18 and 100");
        }
    }

    private void validateLastName(NewDriverValidationDto.NewDriverValidationDtoBuilder validationBuilder, String lastName) {
        if (StringUtils.isBlank(lastName)) {
            validationBuilder.lastNameErrors("Last name is required");
        } else if (lastName.length() < 2 || lastName.length() > 40) {
            validationBuilder.lastNameErrors("Last name cannot contain less than 2 characters and more than 40");
        }
    }

    private void validateFirstName(NewDriverValidationDto.NewDriverValidationDtoBuilder validationBuilder, String firstName) {
        if (StringUtils.isBlank(firstName)) {
            validationBuilder.firstNameErrors("Name is required");
        } else if (firstName.length() < 2 || firstName.length() > 40) {
            validationBuilder.firstNameErrors("Name cannot contain less than 2 characters and more than 40");
        }
    }

    UpdateDriverValidationDto validationUpdateDriver(UpdateDriverCommand updateDriver) {

        UpdateDriverValidationDto.UpdateDriverValidationDtoBuilder validationBuilder = UpdateDriverValidationDto.builder();

        String firstName = updateDriver.getFirstName();
        validateFirstName(validationBuilder, firstName);

        String lastName = updateDriver.getLastName();
        validateLastName(validationBuilder, lastName);

        Integer age = updateDriver.getAge();
        validateAge(validationBuilder, age);

        return validationBuilder.build();
    }

    private void validateAge(UpdateDriverValidationDto.UpdateDriverValidationDtoBuilder validationBuilder, Integer age) {
        if (Objects.isNull(age)) {
            validationBuilder.ageErrors("Age is required");
        } else if (age < 18 || age > 100) {
            validationBuilder.ageErrors("Age needs to be between 18 and 100");
        }
    }

    private void validateLastName(UpdateDriverValidationDto.UpdateDriverValidationDtoBuilder validationBuilder, String lastName) {
        if (StringUtils.isBlank(lastName)) {
            validationBuilder.lastNameErrors("Last name is required");
        } else if (lastName.length() < 2 || lastName.length() > 40) {
            validationBuilder.lastNameErrors("Last name cannot contain less than 2 characters and more than 40");
        }
    }

    private void validateFirstName(UpdateDriverValidationDto.UpdateDriverValidationDtoBuilder validationBuilder, String firstName) {
        if (StringUtils.isBlank(firstName)) {
            validationBuilder.firstNameErrors("Name is required");
        } else if (firstName.length() < 2 || firstName.length() > 40) {
            validationBuilder.firstNameErrors("Name can not contain less than 2 characters and more than 40");
        }
    }

    DeleteDriverValidationDto validateDeleteDriver(DeleteDriverCommand deleteDriver) {
        DeleteDriverValidationDto.DeleteDriverValidationDtoBuilder validateBuilder = DeleteDriverValidationDto.builder();

        Integer driverId = deleteDriver.getDriverId();
        validateDriverId(validateBuilder, driverId);

        return validateBuilder.build();
    }

    private void validateDriverId(DeleteDriverValidationDto.DeleteDriverValidationDtoBuilder validateBuilder, Integer driverId) {
        if (Objects.isNull(driverId)) {
            validateBuilder.driverIdErrors("Driver id required");
        }
    }
}
