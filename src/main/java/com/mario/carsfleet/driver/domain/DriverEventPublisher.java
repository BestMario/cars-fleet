package com.mario.carsfleet.driver.domain;

import com.mario.carsfleet.driver.dto.DriverDeletedEvent;
import com.mario.carsfleet.driver.dto.DriverEventDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class DriverEventPublisher {

    private static final Logger logger = LoggerFactory.getLogger(DriverEventPublisher.class);

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    public void publishDriver(Integer id, String firstName, String lastName, Integer age) {
        DriverEventDto driverEvent = new DriverEventDto(this, id, firstName, lastName, age);
        logger.info("publishing event" + driverEvent);
        applicationEventPublisher.publishEvent(driverEvent);
    }

    public void publishDriverDeleted(Integer id) {
        DriverDeletedEvent driverDeletedEvent = new DriverDeletedEvent(this, id);
        logger.info("publishing event" + driverDeletedEvent);
        applicationEventPublisher.publishEvent(driverDeletedEvent);
    }


}
