package com.mario.carsfleet.driver.domain;

import com.mario.carsfleet.driver.dto.DriverDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
class DriverFinder {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public DriverFinder(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public DriverDto findById(Integer driverId) {
        String query = "select id, first_name, last_name, age, driver_status from driver where id=(:driverId)";
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("driverId", driverId);
        DriverDtoRowMapper rowMapper = new DriverDtoRowMapper();
        return jdbcTemplate.queryForObject(query, namedParameter, rowMapper);
    }

    public List<DriverDto> findBy(Integer pageNumber, Integer pageSize, String filteringText) {
        final String filteringQuery = prepareFindDriversByQuery(filteringText);

        Integer pageIndex = (pageNumber - 1) * pageSize;
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("pageIndex", pageIndex);
        namedParameter.addValue("pageSize", pageSize);
        namedParameter.addValue("filteringText", "%" + filteringText + "%");
        DriverDtoRowMapper rowMapper = new DriverDtoRowMapper();
        return jdbcTemplate.query(filteringQuery, namedParameter, rowMapper);
    }

    private String prepareFindDriversByQuery(String filteringText) {
        final String query;
        if (StringUtils.isBlank(filteringText)) {
            query = "select id, first_name, last_name, age, driver_status from driver LIMIT :pageIndex, :pageSize";
        } else {
            query = "select id, first_name, last_name, age, driver_status from driver where (first_name like :filteringText or last_name like :filteringText) LIMIT :pageIndex, :pageSize";
        }
        return query;
    }

    public Integer count(String filteringText) {
        final String query = prepareCountDriversByQuery(filteringText);
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("filteringText", "%" + filteringText + "%");
        return jdbcTemplate.queryForObject(query, namedParameter, Integer.class);
    }

    private String prepareCountDriversByQuery(String filteringText) {
        final String query;
        if (StringUtils.isBlank(filteringText)) {
            query = "select count(1) from driver";
        } else {
            query = "select count(1) from driver where first_name like :filteringText or last_name like :filteringText";
        }
        return query;
    }
}
