package com.mario.carsfleet.driver;

import com.mario.carsfleet.driver.domain.DriverFacade;
import com.mario.carsfleet.driver.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
public class DriverController {

    private static final Logger logger = LoggerFactory.getLogger(DriverController.class);

    private final DriverFacade driverFacade;

    @Autowired
    public DriverController(DriverFacade driverFacade) {
        this.driverFacade = driverFacade;
    }

    @PostMapping("/api/drivers")
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@RequestBody AddNewDriverCommand newDriver) {
        driverFacade.add(newDriver);
    }

    @GetMapping("/api/drivers/{id}")
    public DriverDto findById(@PathVariable("id") Integer driverId) {
        return driverFacade.findById(driverId);
    }

    @PatchMapping("/api/drivers/{id}")
    public void update(@PathVariable("id") Integer driverId, @RequestBody UpdateDriverCommand updateDriver) {
        driverFacade.update(driverId, updateDriver);
    }

    @DeleteMapping("/api/drivers/{id}")
    public void delete(@PathVariable("id") Integer driverId) {
        DeleteDriverCommand deleteDriver = new DeleteDriverCommand(driverId);
        driverFacade.delete(deleteDriver);
    }

    @GetMapping(value = "/api/drivers", params = "page-number")
    public DriverResultPage findBy(@RequestParam(name = "page-number", defaultValue = "1") Integer pageNumber,
                                   @RequestParam(name = "filter", required = false, defaultValue = "") String filteringText) {
        return driverFacade.findBy(pageNumber, filteringText);
    }

    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(value = {NewDriverValidationException.class})
    public NewDriverValidationDto handleNewDriverValidationException(NewDriverValidationException ex) {
        logger.info("validation exception", ex);
        return ex.getNewDriverValidationDto();
    }

    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(value = {UpdateDriverValidationException.class})
    public UpdateDriverValidationDto handleUpdateDriverValidationException(UpdateDriverValidationException ex) {
        logger.info("update validation exception", ex);
        return ex.getUpdateDriverValidation();
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = DeleteDriverException.class)
    public DeleteDriverValidationDto handleDeleteDriverValidationException(DeleteDriverException delEx) {
        logger.info("delete validation exception", delEx);
        return delEx.getDeleteDriverValidation();
    }
}
