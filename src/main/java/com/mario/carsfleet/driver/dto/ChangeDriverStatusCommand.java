package com.mario.carsfleet.driver.dto;

import lombok.Value;

@Value
public class ChangeDriverStatusCommand {
    private Integer driverId;
    private DriverStatusDto driverStatusDto;
}
