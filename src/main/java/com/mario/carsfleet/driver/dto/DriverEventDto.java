package com.mario.carsfleet.driver.dto;

import org.springframework.context.ApplicationEvent;


public class DriverEventDto extends ApplicationEvent {

    private final Integer id;
    private final String firstName;
    private final String lastName;
    private final Integer age;

    public DriverEventDto(Object source, Integer id, String firstName, String lastName, Integer age) {
        super(source);
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Integer getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "DriverEventDto{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }
}
