package com.mario.carsfleet.driver.dto;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

@Value
@Builder
@ToString
public class UpdateDriverValidationDto {
    private String firstNameErrors;
    private String lastNameErrors;
    private String ageErrors;

    public boolean isValid() {
        return StringUtils.isBlank(firstNameErrors) && StringUtils.isBlank(lastNameErrors)
                && StringUtils.isBlank(ageErrors);
    }
}
