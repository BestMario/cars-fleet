package com.mario.carsfleet.driver.dto;

import org.springframework.context.ApplicationEvent;

public class DriverDeletedEvent extends ApplicationEvent {

    private final Integer id;


    public DriverDeletedEvent(Object source, Integer id) {
        super(source);
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "DriverDeletedEvent{" +
                "id=" + id +
                '}';
    }
}
