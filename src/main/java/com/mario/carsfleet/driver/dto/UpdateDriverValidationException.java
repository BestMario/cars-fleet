package com.mario.carsfleet.driver.dto;

public class UpdateDriverValidationException extends RuntimeException {
    private final UpdateDriverValidationDto updateDriverValidation;

    public UpdateDriverValidationException(UpdateDriverValidationDto updateDriverValidationDto) {
        this.updateDriverValidation = updateDriverValidationDto;
    }

    public UpdateDriverValidationDto getUpdateDriverValidation() {
        return updateDriverValidation;
    }
}
