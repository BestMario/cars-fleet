package com.mario.carsfleet.driver.dto;

import lombok.Value;

import java.util.List;

@Value
public class DriverResultPage {
   private List<DriverDto> drivers;
    private Integer pageNumber;
    private Integer numberOfPages;
    private Integer pageSize;
    private Integer count;
    private String filteringString;

}
