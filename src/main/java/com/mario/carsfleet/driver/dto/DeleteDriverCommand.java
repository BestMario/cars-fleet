package com.mario.carsfleet.driver.dto;

import lombok.Value;

@Value
public class DeleteDriverCommand {
    private Integer driverId;
}
