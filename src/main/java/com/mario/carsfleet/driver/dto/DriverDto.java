package com.mario.carsfleet.driver.dto;

import lombok.Value;

@Value
public class DriverDto {
    private Integer id;
    private String firstName;
    private String lastName;
    private Integer age;
    private DriverStatusDto driverStatus;
}
