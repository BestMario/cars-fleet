package com.mario.carsfleet.driver.dto;

public class DeleteDriverException extends RuntimeException {
    private final DeleteDriverValidationDto deleteDriverValidation;

    public DeleteDriverException(DeleteDriverValidationDto deleteDriverValidation) {
        this.deleteDriverValidation = deleteDriverValidation;
    }

    public DeleteDriverValidationDto getDeleteDriverValidation() {
        return deleteDriverValidation;
    }
}
