package com.mario.carsfleet.driver.dto;

import lombok.Value;

@Value
public class UpdateDriverCommand {
    private String firstName;
    private String lastName;
    private Integer age;
    private DriverStatusDto driverStatus;
}
