package com.mario.carsfleet.driver.dto;

import lombok.Value;

@Value
public class AddNewDriverCommand {
    private String firstName;
    private String lastName;
    private Integer age;
}
