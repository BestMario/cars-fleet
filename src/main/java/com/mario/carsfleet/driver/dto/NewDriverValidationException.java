package com.mario.carsfleet.driver.dto;

public class NewDriverValidationException extends RuntimeException {
    private final NewDriverValidationDto newDriverValidationDto;

    public NewDriverValidationException(NewDriverValidationDto newDriverValidationDto) {
        super();
        this.newDriverValidationDto = newDriverValidationDto;
    }

    public NewDriverValidationDto getNewDriverValidationDto() {
        return newDriverValidationDto;
    }

    @Override
    public String toString() {
        return "NewDriverValidationException{" +
                "newDriverValidationDto=" + newDriverValidationDto +
                '}';
    }
}
