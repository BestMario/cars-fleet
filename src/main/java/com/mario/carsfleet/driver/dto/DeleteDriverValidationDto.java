package com.mario.carsfleet.driver.dto;

import lombok.Builder;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

@Value
@Builder
public class DeleteDriverValidationDto {
    private String driverIdErrors;

    public boolean isValid() {
        return StringUtils.isBlank(driverIdErrors);
    }
}
