package com.mario.carsfleet.driver.dto;

public enum DriverStatusDto {
    ACTIVE,
    INACTIVE
}
