package com.mario.carsfleet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarsFleetApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarsFleetApplication.class, args);

	}
}
