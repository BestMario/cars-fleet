package com.mario.carsfleet.car;

import com.mario.carsfleet.car.domain.CsvReportsFacade;
import com.mario.carsfleet.car.domain.ExcelReportsFacade;
import com.mario.carsfleet.car.domain.PdfReportsFacade;
import com.mario.carsfleet.car.dto.ReportTypeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

@RestController
public class ReportsController {

    private final CsvReportsFacade csvReportsFacade;
    private final ExcelReportsFacade excelReportsFacade;
    private final PdfReportsFacade pdfReportsFacade;

    @Autowired
    public ReportsController(CsvReportsFacade csvReportsFacade, ExcelReportsFacade excelReportsFacade, PdfReportsFacade pdfReportsFacade) {
        this.csvReportsFacade = csvReportsFacade;
        this.excelReportsFacade = excelReportsFacade;
        this.pdfReportsFacade = pdfReportsFacade;
    }

    @GetMapping("/api/cars/{carId}/fuel/report")
    public void carFuelConsumptionRaportHandler(@PathVariable(name = "carId") Integer carId,
                                                @RequestParam(value = "refueling-date-from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, pattern = "yyyy-MM-dd") LocalDate refuelingDateFrom,
                                                @RequestParam(value = "refueling-date-to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, pattern = "yyyy-MM-dd") LocalDate refuelingDateTo,
                                                @RequestParam(value = "report-format", defaultValue = "CSV") ReportTypeFormat reportFormat,
                                                HttpServletResponse response) throws IOException {

        if (ReportTypeFormat.CSV.equals(reportFormat)) {
            csvReportsFacade.prepareCsvReportForSingleCar(carId, refuelingDateFrom, refuelingDateTo, response);
        } else if (ReportTypeFormat.EXCEL.equals(reportFormat)){
            excelReportsFacade.prepareExcelReportForSingleCar(carId, refuelingDateFrom, refuelingDateTo, response);
        } else {
            pdfReportsFacade.preparePdfReportForSingleCar(carId, refuelingDateFrom, refuelingDateTo, response);
        }
    }

    @GetMapping("/api/cars/fuels/report")
    public void allCarsFuelConsumptionsReportsHandler(@RequestParam(name = "refueling-date-from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, pattern = "yyyy-MM-dd") LocalDate refuelingDateFrom,
                                                      @RequestParam(name = "refueling-date-to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, pattern = "yyyy-MM-dd") LocalDate refuelingDateTo,
                                                      @RequestParam(value = "report-format", defaultValue = "") ReportTypeFormat reportFormat,
                                                      HttpServletResponse response) throws IOException {

        if (ReportTypeFormat.CSV.equals(reportFormat)) {
            csvReportsFacade.prepareReportCsvForAllCars(refuelingDateFrom, refuelingDateTo, response);
        } else if (ReportTypeFormat.EXCEL.equals(reportFormat)){
            excelReportsFacade.prepareExcelReportForAllCars(refuelingDateFrom, refuelingDateTo, response);
        } else {
            pdfReportsFacade.preparePdfReportForAllCars(refuelingDateFrom, refuelingDateTo, response);
        }
    }
}

