package com.mario.carsfleet.car.domain;

import org.springframework.data.repository.CrudRepository;

interface CarDriverRepository extends CrudRepository<Driver, Integer> {
}
