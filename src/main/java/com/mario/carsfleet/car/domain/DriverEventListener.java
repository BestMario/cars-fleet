package com.mario.carsfleet.car.domain;

import com.mario.carsfleet.driver.dto.DriverEventDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class DriverEventListener implements ApplicationListener<DriverEventDto> {

    private static final Logger logger = LoggerFactory.getLogger(DriverEventListener.class);

    private final SaveOrUpdateDriverService saveOrUpdateDriverService;

    @Autowired
    public DriverEventListener(SaveOrUpdateDriverService saveOrUpdateDriverService) {
        this.saveOrUpdateDriverService = saveOrUpdateDriverService;
    }

    @Override
    public void onApplicationEvent(DriverEventDto driverEvent) {
        logger.info("event received - " + driverEvent);
        saveOrUpdateDriverService.saveOrUpdateDriver(driverEvent);
    }
}
