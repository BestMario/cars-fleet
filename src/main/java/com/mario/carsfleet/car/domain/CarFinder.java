package com.mario.carsfleet.car.domain;

import com.mario.carsfleet.car.dto.CarDriverDto;
import com.mario.carsfleet.car.dto.CarDto;
import com.mario.carsfleet.car.dto.FuelDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

@Component
class CarFinder {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public CarFinder(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public CarDto findById(Integer carId) {
        String query = "select car.id, make, car_model, manufactured_year, number_plate, car_status, longitude, latitude, current_driver_id, car_driver.first_name, car_driver.last_name from car left join car_driver on car.current_driver_id = car_driver.id where car.id=(:carId)";
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("carId", carId);
        CarDtoRowMapper rowMapper = new CarDtoRowMapper();
        return jdbcTemplate.queryForObject(query, namedParameter, rowMapper);
    }

    public List<CarDto> findBy(String filteringText, Integer pageNumber, Integer pageSize) {
        final String filteringQuery = prepareFindByFilteringQuery(filteringText);

        Integer pageIndex = (pageNumber - 1) * pageSize;
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("pageIndex", pageIndex);
        namedParameter.addValue("pageSize", pageSize);
        namedParameter.addValue("filteringText", "%" + filteringText + "%");
        CarDtoRowMapper rowMapper = new CarDtoRowMapper();

        return jdbcTemplate.query(filteringQuery, namedParameter, rowMapper);
    }

    public Integer countBy(String filteringText) {
        final String query = prepareCountByQuery(filteringText);
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("filteringText", "%" + filteringText + "%");
        return jdbcTemplate.queryForObject(query, namedParameter, Integer.class);

    }

    private String prepareCountByQuery(String filteringText) {
        final String query;
        if (StringUtils.isBlank(filteringText)) {
            query = "select count(1) from car";
        } else {
            query = "select count(1) from car where make like :filteringText or car_model like :filteringText";
        }
        return query;
    }

    private String prepareFindByFilteringQuery(String filteringText) {
        final String query;
        if (StringUtils.isBlank(filteringText)) {
            query = "select (car.id) as 'id', make, car_model, manufactured_year, number_plate, car_status, longitude, latitude, current_driver_id, car_driver.first_name, car_driver.last_name from car left join car_driver on car.current_driver_id = car_driver.id LIMIT :pageIndex, :pageSize";
        } else {
            query = "select (car.id) as 'id', make, car_model, manufactured_year, number_plate, car_status, longitude, latitude, current_driver_id, car_driver.first_name, car_driver.last_name from car left join car_driver on car.current_driver_id = car_driver.id where (make like :filteringText or car_model like :filteringText) LIMIT :pageIndex, :pageSize";
        }
        return query;
    }

    public List<CarDriverDto> findDriversBy(Integer pageNumber, String filteringText, Integer pageSize) {
        final String query = prepareFindDriversBy(filteringText);
        Integer pageIndex = (pageNumber - 1) * pageSize;
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("pageIndex", pageIndex);
        namedParameter.addValue("pageSize", pageSize);
        namedParameter.addValue("filteringText", "%" + filteringText + "%");
        CarDriverDtoRowMapper rowMapper = new CarDriverDtoRowMapper();

        return jdbcTemplate.query(query, namedParameter, rowMapper);
    }

    private String prepareFindDriversBy(String filteringText) {
        final String query;
        if (StringUtils.isBlank(filteringText)) {
            query = "select (car_driver.id) as 'id', first_name, last_name, current_driver_id from car_driver left join car on car_driver.id = car.current_driver_id LIMIT :pageIndex, :pageSize";
        } else {
            query = "select (car_driver.id) as 'id', first_name, last_name, current_driver_id from car_driver left join car on car_driver.id = car.current_driver_id where (first_name like :filteringText or last_name like :filteringText) LIMIT :pageIndex, :pageSize";
        }
        return query;
    }

    public Integer countDriversBy(String filteringText) {
        final String query = prepareCountDriversBy(filteringText);
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("filteringText", "%" + filteringText + "%");
        return jdbcTemplate.queryForObject(query, namedParameters, Integer.class);

    }

    private String prepareCountDriversBy(String filteringText) {
        final String query;
        if (StringUtils.isBlank(filteringText)) {
            query = "select count(1) from car_driver";
        } else {
            query = "select count(1) from car_driver where first_name like :filteringText or last_name like :filteringText";
        }
        return query;
    }

    public List<FuelDto> findFuelBy(Integer carId, Integer pageNumber, Integer pageSize) {
        String query = "select (fuel.id) as 'id', refueling_date, amount_in_liters, fuel_type, price_in_pln, mileage_in_km, car_id from fuel where car_id=(:carId) LIMIT :pageIndex, :pageSize";
        Integer pageIndex = (pageNumber - 1) * pageSize;
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("carId", carId);
        namedParameter.addValue("pageIndex", pageIndex);
        namedParameter.addValue("pageSize", pageSize);
        FuelDtoRowMapper rowMapper = new FuelDtoRowMapper();
        return jdbcTemplate.query(query, namedParameter, rowMapper);
    }

    public FuelDto findFuelById(Integer fuelId) {
        String query = "select (fuel.id) as 'id', refueling_date, amount_in_liters, fuel_type, price_in_pln, mileage_in_km, (car_id) as 'car_id' from fuel where fuel.id=(:fuelId)";
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("fuelId", fuelId);
        FuelDtoRowMapper rowMapper = new FuelDtoRowMapper();
        return jdbcTemplate.queryForObject(query, namedParameter, rowMapper);
    }

    public List<FuelDto> findAllFuelBy(Integer carId, LocalDate refuelingDateFrom, LocalDate refuelingDateTo) {
        final String query = prepareFindAllFuelBy(refuelingDateFrom, refuelingDateTo);
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("carId", carId);
        namedParameter.addValue("refuelingDateFrom", refuelingDateFrom);
        namedParameter.addValue("refuelingDateTo", refuelingDateTo);
        FuelDtoRowMapper rowMapper = new FuelDtoRowMapper();
        return jdbcTemplate.query(query, namedParameter, rowMapper);
    }

    private String prepareFindAllFuelBy(LocalDate refuelingDateFrom, LocalDate refuelingDateTo) {
        final String query;
        if (Objects.isNull(refuelingDateFrom) && (Objects.isNull(refuelingDateTo))) {
            query = "select (fuel.id) as 'id', refueling_date, amount_in_liters, fuel_type, price_in_pln, mileage_in_km, (car_id) as 'car_id' from fuel where car_id=(:carId) order by refueling_date";
        } else {
            query = "select (fuel.id) as 'id', refueling_date, amount_in_liters, fuel_type, price_in_pln, mileage_in_km, (car_id) as 'car_id' from fuel where car_id=(:carId) and refueling_date >= :refuelingDateFrom and refueling_date <= :refuelingDateTo order by refueling_date";
        }
        return query;
    }

    public Integer countFuels(Integer carId) {
        final String query = "select count(1) from fuel where car_id=(:carId)";
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("carId", carId);
        return jdbcTemplate.queryForObject(query, namedParameter, Integer.class);
    }

    public List<FuelDto> findAllFuel(LocalDate refuelingDateFrom, LocalDate refuelingDateTo) {
        final String query = prepareFindAllFuel(refuelingDateFrom, refuelingDateTo);
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();

        if (Objects.nonNull(refuelingDateFrom) && Objects.nonNull(refuelingDateTo)) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String refuelingDataFromAsString = refuelingDateFrom.format(formatter);
            String refuelingDataToAsString = refuelingDateTo.format(formatter);

            namedParameters.addValue("refuelingDateFrom", refuelingDataFromAsString);
            namedParameters.addValue("refuelingDateTo", refuelingDataToAsString);
        }

        FuelDtoRowMapper rowMapper = new FuelDtoRowMapper();
        return jdbcTemplate.query(query, namedParameters, rowMapper);
    }

    private String prepareFindAllFuel(LocalDate refuelingDateFrom, LocalDate refuelingDateTo) {
        final String query;
        if (Objects.isNull(refuelingDateFrom) || (Objects.isNull(refuelingDateTo))) {
            query = "select (fuel.id) as 'id', refueling_date, amount_in_liters, fuel_type, price_in_pln, mileage_in_km, car_id from fuel order by car_id, refueling_date";
        } else {
            query = "select (fuel.id) as 'id', refueling_date, amount_in_liters, fuel_type, price_in_pln, mileage_in_km, car_id from fuel where refueling_date >= :refuelingDateFrom and refueling_date <= :refuelingDateTo order by car_id, refueling_date";
        }
        return query;
    }

    public List<CarDto> findCars() {
        String query = "select (car.id) as 'id', make, car_model, manufactured_year, number_plate, car_status, longitude, latitude, current_driver_id, car_driver.first_name, car_driver.last_name from car left join car_driver on car.current_driver_id = car_driver.id";
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        CarDtoRowMapper rowMapper = new CarDtoRowMapper();
        return jdbcTemplate.query(query, namedParameter, rowMapper);
    }
}
