package com.mario.carsfleet.car.domain;

import com.mario.carsfleet.car.dto.CarDriverDto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

class CarDriverDtoRowMapper implements RowMapper<CarDriverDto> {
    @Override
    public CarDriverDto mapRow(ResultSet rs, int rowNum) throws SQLException {
        Integer driverId = rs.getInt("id");
        Integer currentDriverId = readCurrentDriverId(rs);
        boolean isAssigned = Objects.nonNull(currentDriverId);
        String firstName = rs.getString("first_name");
        String lastName = rs.getString("last_name");
        return new CarDriverDto(driverId, isAssigned, firstName, lastName);
    }


    private Integer readCurrentDriverId(ResultSet rs) throws SQLException {
        Integer rawCurrentDriverId = rs.getInt("current_driver_id");
        if (Objects.nonNull(rawCurrentDriverId)) {
            return rawCurrentDriverId;
        }
        return null;
    }
}