package com.mario.carsfleet.car.domain;

import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletResponse;

class ReportUtils {

    private ReportUtils() {}

    static void setContentDisposition(HttpServletResponse response, String s, String s2) {
        String fileName = s;
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                s2 + fileName + "\"");
    }

    static void setContentType(HttpServletResponse response, String s) {
        response.setContentType(s);
    }
}
