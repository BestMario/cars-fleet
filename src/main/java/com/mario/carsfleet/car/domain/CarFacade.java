package com.mario.carsfleet.car.domain;

import com.mario.carsfleet.car.dto.*;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.*;

import static com.mario.carsfleet.boot.PageSettings.PAGE_SIZE;

@Service
public class CarFacade {

    private final CarRepository carRepository;
    private final CarFinder carFinder;
    private final CarDriverRepository driverRepository;
    private final CarValidator carValidator;

    @Autowired
    public CarFacade(CarRepository carRepository, CarFinder carFinder, CarDriverRepository driverRepository, CarValidator carValidator) {
        this.carRepository = carRepository;
        this.carFinder = carFinder;
        this.driverRepository = driverRepository;
        this.carValidator = carValidator;
    }

    public Integer add(AddNewCarCommand newCar) {

        NewCarValidationDto validation = carValidator.validateAddNewCar(newCar);
        if (!validation.isValid()) {
            throw new NewCarValidationException(validation);
        }

        Integer carId = RandomUtils.nextInt(1, Integer.MAX_VALUE);


        Car car = new Car();
        car.setId(carId);
        car.setMake(newCar.getMake());
        car.setModel(newCar.getModel());
        car.setManufacturedYear(newCar.getManufacturedYear());
        car.setNumberPlate(newCar.getNumberPlate());
        car.setCarStatus(CarStatus.NEW);

        Position position = positionGenerate();
        car.setPosition(position);

        carRepository.save(car);
        return car.getId();
    }

    private Position positionGenerate() {
        Position position = new Position();
        long longitudeLeftLimit = 1600000L;
        long longitudeRightLimit = 1780000L;
        long latitudeLeftLimit = 5000000L;
        long latitudeRightLimit = 5200000L;
        RandomDataGenerator randomDataGenerator = new RandomDataGenerator();
        position.setLongitude(randomDataGenerator.nextLong(longitudeLeftLimit, longitudeRightLimit));
        position.setLatitude(randomDataGenerator.nextLong(latitudeLeftLimit, latitudeRightLimit));
        return position;
    }

    public CarDto findById(Integer carId) {
        return carFinder.findById(carId);
    }

    public void update(Integer carId, UpdateCarCommand updateCar) {

        UpdateCarValidationDto validation = carValidator.validateUpdateCar(updateCar);
        if (!validation.isValid()) {
            throw new UpdateCarValidationException(validation);
        }

        Optional<Car> car = carRepository.findById(carId);
        if (car.isPresent()) {
            Car c = car.get();
            c.setMake(updateCar.getMake());
            c.setModel(updateCar.getModel());
            c.setManufacturedYear(updateCar.getManufacturedYear());
            c.setNumberPlate(updateCar.getNumberPlate());
            carRepository.save(c);
        }
    }

    public void delete(DeleteCarCommand deleteCar) {

        DeleteCarValidationDto validation = carValidator.validateDeleteCar(deleteCar);
        if (!validation.isValid()) {
            throw new DeleteCarValidationException(validation);
        }
        carRepository.deleteById(deleteCar.getId());
    }

    public CarResultPage findBy(Integer pageNumber, String filteringText) {
        List<CarDto> cars = carFinder.findBy(filteringText, pageNumber, PAGE_SIZE);
        Integer count = carFinder.countBy(filteringText);
        Integer numberOfPages = (int) Math.ceil((double) count / (double) PAGE_SIZE);

        return new CarResultPage(cars, pageNumber, numberOfPages, PAGE_SIZE, count, filteringText);
    }

    public void changeCarStatus(ChangeCarStatusCommand changeCarStatus) {
        Optional<Car> c = carRepository.findById(changeCarStatus.getId());
        if (c.isPresent()) {
            Car car = c.get();
            car.setCarStatus(CarStatus.convert(changeCarStatus.getCarStatus()));
            carRepository.save(car);
        }
    }

    @Transactional
    public void addDriverToCar(AddDriverToCarCommand command) {
        Optional<Car> car = carRepository.findById(command.getCarId());

        throwExceptionIfCarNotFound(command, car);

        if (!car.isPresent()) {
            throw new CarNotFoundException(command.getCarId());
        }
        Car c = car.get();

        throwExceptionIfCarIsInUse(c);

        Optional<Driver> driver = driverRepository.findById(command.getDriverId());
        if (driver.isPresent()) {
            Driver d = driver.get();
            c.setCurrentDriver(d);
            carRepository.save(c);
        }
    }

    private void throwExceptionIfCarIsInUse(Car c) {
        Driver currentDriver = c.getCurrentDriver();
        if (Objects.nonNull(currentDriver)) {
            AssignDriverValidationDto validation = AssignDriverValidationDto.builder().assignedErrors("Car is already in use ").build();
            throw new AssignDriverToCarValidationException(validation);
        }
    }

    private void throwExceptionIfCarNotFound(AddDriverToCarCommand addDriverToCar, Optional<Car> car) {
        if (!car.isPresent()) {
            throw new CarNotFoundException(addDriverToCar.getCarId());
        }
    }

    public void detachDriverFromCar(DetachDriverFromCarCommand detachDriverFromCar) {
        Optional<Car> car = carRepository.findById(detachDriverFromCar.getCarId());
        if (car.isPresent()) {
            Car c = car.get();
            c.setCurrentDriver(null);
            carRepository.save(c);
        }
    }

    public DriversResultPage findDriversBy(Integer pageNumber, String filteringText) {
        List<CarDriverDto> drivers = carFinder.findDriversBy(pageNumber, filteringText, PAGE_SIZE);
        Integer count = carFinder.countDriversBy(filteringText);
        Integer numberOfPages = (int) Math.ceil((double) count / (double) PAGE_SIZE);
        return new DriversResultPage(drivers, pageNumber, numberOfPages, PAGE_SIZE, count, filteringText);
    }

    @Transactional
    public Integer addFuelConsumption(Integer carId, AddFuelConsumptionCommand fuelConsumptionCommand) {

        AddFuelConsumptionValidationDto validation = carValidator.validateAddFuelConsumption(carId, fuelConsumptionCommand);
        if (!validation.isValid()) {
            throw new AddFuelConsumptionException(validation);
        }

        Optional<Car> car = carRepository.findById(carId);
        if (car.isPresent()) {
            Car c = car.get();
            if (Objects.isNull(c.getFuelConsumption())) {
                c.setFuelConsumption(new ArrayList<>());
            }
            Fuel fuel = new Fuel();

            Integer fuelId = RandomUtils.nextInt(1, Integer.MAX_VALUE);

            fuel.setId(fuelId);
            fuel.setRefuelingDate(fuelConsumptionCommand.getRefuelingDate());
            fuel.setAmountInLiters(fuelConsumptionCommand.getAmountInLiters());
            fuel.setFuelType(FuelType.convert(fuelConsumptionCommand.getFuelType()));
            fuel.setPriceInPln(fuelConsumptionCommand.getPriceInPln());
            fuel.setMileageInKm(fuelConsumptionCommand.getMileageInKm());
            c.getFuelConsumption().add(fuel);
            carRepository.save(c);
            return fuel.getId();
        }
        return null;
    }

    public FuelResultPage findFuelBy(Integer carId, Integer pageNumber) {
        List<FuelDto> fuels = carFinder.findFuelBy(carId, pageNumber, PAGE_SIZE);
        Integer count = carFinder.countFuels(carId);
        Integer numberOfPages = (int) Math.ceil((double) count / (double) PAGE_SIZE);
        return new FuelResultPage(fuels, pageNumber, numberOfPages, PAGE_SIZE, count);
    }

    public void deleteFuel(DeleteFuelHistoryCommand fuelHistoryCommand) {

        DeleteFuelValidationDto validation = carValidator.validateDeleteFuel(fuelHistoryCommand);
        if (!validation.isValid()) {
            throw new DeleFuelValidationException(validation);
        }

        Integer carId = fuelHistoryCommand.getCarId();
        Optional<Car> car = carRepository.findById(carId);
        if (car.isPresent()) {
            Car c = car.get();
            c.deleteFuelById(fuelHistoryCommand.getFuelId());
            carRepository.save(c);
        }
    }

    public FuelDto findFuelById(Integer fuelId) {
        return carFinder.findFuelById(fuelId);
    }

    public List<FuelDto> findAllFuelBy(Integer carId, LocalDate refuelingDateFrom, LocalDate refuelingDateTo) {
        return carFinder.findAllFuelBy(carId, refuelingDateFrom, refuelingDateTo);
    }

    @Transactional
    public void updateFuel(Integer carId, Integer fuelId, UpdateFuelCommand updateFuelCommand) {

        UpdateFuelValidationDto validation = carValidator.validateUpdateFuelConsumption(carId, fuelId, updateFuelCommand);
        if (!validation.isValid()) {
            throw new UpdateFuelValidationException(validation);
        }

        Optional<Car> car = carRepository.findById(carId);
        if (car.isPresent()) {
            Car c = car.get();

            List<Fuel> fuelConsumption = c.getFuelConsumption();
            Optional<Fuel> fuelOptional = fuelConsumption.stream().filter(fuel -> fuel.getId().equals(fuelId)).findFirst();

            if (fuelOptional.isPresent()) {
                Fuel fuel = fuelOptional.get();
                fuel.setRefuelingDate(updateFuelCommand.getRefuelingDate());
                fuel.setAmountInLiters(updateFuelCommand.getAmountInLiters());
                fuel.setFuelType(FuelType.convert(updateFuelCommand.getFuelType()));
                fuel.setPriceInPln(updateFuelCommand.getPriceInPln());
                fuel.setMileageInKm(updateFuelCommand.getMileageInKm());
                carRepository.save(c);
            }
        }
    }

    public List<FuelDto> findAllFuel(LocalDate refuelingDateFrom, LocalDate refuelingDateTo) {
        return carFinder.findAllFuel(refuelingDateFrom, refuelingDateTo);
    }

    public List<CarDto> findCars() {
        return carFinder.findCars();
    }
}


