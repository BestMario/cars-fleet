package com.mario.carsfleet.car.domain;

import com.mario.carsfleet.driver.dto.DriverDeletedEvent;
import com.mario.carsfleet.driver.dto.DriverEventDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

@Service
class SaveOrUpdateDriverService {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    SaveOrUpdateDriverService(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    void saveOrUpdateDriver(DriverEventDto event) {
        boolean driverExists = isDriverExists(event);
        if (driverExists) {
            updateDriver(event);
        } else {
            insertDriver(event);
        }
    }

    private void insertDriver(DriverEventDto event) {
        String query = "insert into car_driver (id, first_name, last_name, age) values ((:id), :first_name, :last_name, :age);";
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("id", event.getId());
        namedParameter.addValue("first_name", event.getFirstName());
        namedParameter.addValue("last_name", event.getLastName());
        namedParameter.addValue("age", event.getAge());
        jdbcTemplate.update(query, namedParameter);
    }

    private void updateDriver(DriverEventDto event) {
        String query = "update car_driver set first_name=:first_name, last_name=:last_name, age=:age where id =(:id);";
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("id", event.getId());
        namedParameter.addValue("first_name", event.getFirstName());
        namedParameter.addValue("last_name", event.getLastName());
        namedParameter.addValue("age", event.getAge());
        jdbcTemplate.update(query, namedParameter);
    }

    private boolean isDriverExists(DriverEventDto event) {
        String query = "select exists(select 1 from car_driver where id=(:id)) as 'row_exists'";
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("id", event.getId());
        return jdbcTemplate.queryForObject(query, namedParameter, Boolean.class);
    }

    public void deleteDriver(DriverDeletedEvent event) {
        usingDriver(event);

        delete(event);
    }

    private void delete(DriverDeletedEvent event) {
        String query = "delete from car_driver where id =(:driverId)";
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("driverId", event.getId());
        jdbcTemplate.update(query, namedParameter);
    }

    private void usingDriver(DriverDeletedEvent event) {
        String query = "update car set current_driver_id = null where current_driver_id =(:driverId)";
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("driverId", event.getId());
        jdbcTemplate.update(query, namedParameter);
    }
}


