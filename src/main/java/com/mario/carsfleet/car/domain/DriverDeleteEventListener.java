package com.mario.carsfleet.car.domain;

import com.mario.carsfleet.driver.dto.DriverDeletedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class DriverDeleteEventListener implements ApplicationListener<DriverDeletedEvent> {

    private static final Logger logger = LoggerFactory.getLogger(DriverEventListener.class);
    private final SaveOrUpdateDriverService saveOrUpdateDriverService;

    @Autowired
    public DriverDeleteEventListener(SaveOrUpdateDriverService saveOrUpdateDriverService) {
        this.saveOrUpdateDriverService = saveOrUpdateDriverService;
    }

    @Override
    public void onApplicationEvent(DriverDeletedEvent event) {
        logger.info("event received - " + event);
        saveOrUpdateDriverService.deleteDriver(event);
    }
}
