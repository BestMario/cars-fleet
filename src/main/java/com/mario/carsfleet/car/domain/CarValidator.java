package com.mario.carsfleet.car.domain;

import com.mario.carsfleet.car.dto.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Objects;

@Service
class CarValidator {

    NewCarValidationDto validateAddNewCar(AddNewCarCommand newCar) {
        NewCarValidationDto.NewCarValidationDtoBuilder validationBuilder = NewCarValidationDto.builder();

        String make = newCar.getMake();
        validateMake(validationBuilder, make);

        String model = newCar.getModel();
        validateModel(validationBuilder, model);

        Integer manufacturedYear = newCar.getManufacturedYear();
        validateManufacturedYear(validationBuilder, manufacturedYear);

        String numberPlate = newCar.getNumberPlate();
        validateNumberPlate(validationBuilder, numberPlate);

        return validationBuilder.build();
    }

    private void validateNumberPlate(NewCarValidationDto.NewCarValidationDtoBuilder validationBuilder, String numberPlate) {
        if (Objects.isNull(numberPlate)) {
            validationBuilder.numberPlateErrors("Number plate is required ");
        } else if (numberPlate.length() < 3 || numberPlate.length() > 11) {
            validationBuilder.numberPlateErrors("Number plate cannot contain less than 3 and more than 11 characters");
        }
    }

    private void validateManufacturedYear(NewCarValidationDto.NewCarValidationDtoBuilder validationBuilder, Integer manufacturedYear) {
        if (Objects.isNull(manufacturedYear)) {
            validationBuilder.manufacturedYearErrors("Manufactured year is required");
        } else if (manufacturedYear <= 1950) {
            validationBuilder.manufacturedYearErrors("Manufactured year must be greater than 1950");
        }
    }

    private void validateModel(NewCarValidationDto.NewCarValidationDtoBuilder validationBuilder, String model) {
        if (StringUtils.isBlank(model)) {
            validationBuilder.modelErrors("Model is required");
        } else if (model.length() < 2 || model.length() > 50) {
            validationBuilder.modelErrors("Model cannot contain less than 2 characters and more than 50");
        }
    }

    private void validateMake(NewCarValidationDto.NewCarValidationDtoBuilder validationBuilder, String make) {
        if (StringUtils.isBlank(make)) {
            validationBuilder.makeErrors("Make is required");
        } else if (make.length() < 2 || make.length() > 40) {
            validationBuilder.makeErrors("Make cannot contain less than 2 characters and more than 40");
        }
    }

    UpdateCarValidationDto validateUpdateCar(UpdateCarCommand updateCar) {
        UpdateCarValidationDto.UpdateCarValidationDtoBuilder validationBuilder = UpdateCarValidationDto.builder();

        String make = updateCar.getMake();
        validateMake(validationBuilder, make);

        String model = updateCar.getModel();
        validateModel(validationBuilder, model);

        Integer manufacturedYear = updateCar.getManufacturedYear();
        validateManufacturedYear(validationBuilder, manufacturedYear);

        String numberPlate = updateCar.getNumberPlate();
        validateUpdateNumberPlate(validationBuilder, numberPlate);

        return validationBuilder.build();
    }

    private void validateUpdateNumberPlate(UpdateCarValidationDto.UpdateCarValidationDtoBuilder validationBuilder, String numberPlate) {
        if (StringUtils.isBlank(numberPlate)) {
            validationBuilder.numberPlateErrors("Number plate is required");
        } else if (numberPlate.length() < 3 || numberPlate.length() > 11) {
            validationBuilder.numberPlateErrors("Number plate cannot contain less than 3 and more than 11 characters");
        }
    }

    private void validateManufacturedYear(UpdateCarValidationDto.UpdateCarValidationDtoBuilder validationDtoBuilder, Integer manufacturedYear) {
        if (Objects.isNull(manufacturedYear)) {
            validationDtoBuilder.manufacturedYearErrors("Manufactured year is required");
        } else if (manufacturedYear <= 1950) {
            validationDtoBuilder.manufacturedYearErrors("Manufactured year must be greater than 1950");
        }
    }

    private void validateModel(UpdateCarValidationDto.UpdateCarValidationDtoBuilder validationBuilder, String model) {
        if (StringUtils.isBlank(model)) {
            validationBuilder.modelErrors("Model is required");
        } else if (model.length() < 2 || model.length() > 40) {
            validationBuilder.modelErrors("Model cannot contain less than 2 characters and more than 50");
        }
    }

    private void validateMake(UpdateCarValidationDto.UpdateCarValidationDtoBuilder validationBuilder, String make) {
        if (StringUtils.isBlank(make)) {
            validationBuilder.makeErrors("Make is required");
        } else if (make.length() < 2 || make.length() > 40) {
            validationBuilder.makeErrors("Make cannot contain less than 2 characters and more than 40");
        }
    }

    DeleteCarValidationDto validateDeleteCar(DeleteCarCommand deleteCar) {
        DeleteCarValidationDto.DeleteCarValidationDtoBuilder validationBuilder = DeleteCarValidationDto.builder();

        Integer carId = deleteCar.getId();
        validateCarId(validationBuilder, carId);

        return validationBuilder.build();
    }

    private void validateCarId(DeleteCarValidationDto.DeleteCarValidationDtoBuilder validationBuilder, Integer carId) {
        if (Objects.isNull(carId)) {
            validationBuilder.carIdErrors("Car id required");
        }
    }

    public AddFuelConsumptionValidationDto validateAddFuelConsumption(Integer carId, AddFuelConsumptionCommand fuelConsumption) {
        AddFuelConsumptionValidationDto.AddFuelConsumptionValidationDtoBuilder validationDtoBuilder = AddFuelConsumptionValidationDto.builder();
        if (Objects.isNull(carId)) {
            validationDtoBuilder.carIdErrors("Car is required");
        }

        if (Objects.isNull(fuelConsumption.getRefuelingDate())) {
            validationDtoBuilder.refuelingDateErrors("Date is required");
        }

        if (Objects.isNull(fuelConsumption.getAmountInLiters()) ||
                fuelConsumption.getAmountInLiters().compareTo(new BigDecimal("10")) < 0 ||
                fuelConsumption.getAmountInLiters().compareTo(new BigDecimal("500")) > 0) {
            validationDtoBuilder.amountInLitersErrors("Liters amount should not be less than 10 and more than 500");
        }

        if (Objects.isNull(fuelConsumption.getPriceInPln()) ||
                fuelConsumption.getPriceInPln().compareTo(new BigDecimal("50")) < 0 ||
                fuelConsumption.getPriceInPln().compareTo(new BigDecimal("2500")) > 0) {
            validationDtoBuilder.priceInPlnErrors("Price value should not be less than 50 and more than 2500");
        }

        if (Objects.isNull(fuelConsumption.getMileageInKm()) || fuelConsumption.getMileageInKm() < 10) {
            validationDtoBuilder.mileageInKmErrors("Mileage value should not be less than 1");
        }
        return validationDtoBuilder.build();
    }

    public UpdateFuelValidationDto validateUpdateFuelConsumption(Integer carId, Integer fuelId, UpdateFuelCommand updateFuelCommand) {
        UpdateFuelValidationDto.UpdateFuelValidationDtoBuilder validationDtoBuilder = UpdateFuelValidationDto.builder();
        if (Objects.isNull(carId)) {
            validationDtoBuilder.carIdErrors("Car is required");
        }

        if (Objects.isNull(fuelId)) {
            validationDtoBuilder.fuelIdErrors("Fuel consumption is required");
        }

        if (updateFuelCommand.getAmountInLiters().compareTo(new BigDecimal("10")) < 0 ||
                updateFuelCommand.getAmountInLiters().compareTo(new BigDecimal("500")) > 0) {
            validationDtoBuilder.amountInLitersErrors("Liters amount should not be less than 10 and more than 500");
        }

        if (updateFuelCommand.getPriceInPln().compareTo(new BigDecimal("50")) < 0 ||
                updateFuelCommand.getPriceInPln().compareTo(new BigDecimal("2500")) > 0) {
            validationDtoBuilder.priceInPlnErrors("Price value should not be less than 50 and more than 2500");
        }

        if (updateFuelCommand.getMileageInKm() < 1) {
            validationDtoBuilder.mileageInKmErrors("Mileage value should not be less than 1");
        }
        return validationDtoBuilder.build();
    }

    public DeleteFuelValidationDto validateDeleteFuel(DeleteFuelHistoryCommand deleteFuelHistory) {
        DeleteFuelValidationDto.DeleteFuelValidationDtoBuilder validationDtoBuilder = DeleteFuelValidationDto.builder();
        if (Objects.isNull(deleteFuelHistory.getCarId())) {
            validationDtoBuilder.carIdErrors("Car is required");
        } else if (Objects.isNull(deleteFuelHistory.getFuelId())) {
            validationDtoBuilder.fuelIdErrors("Fuel consumption is required");
        }
        return validationDtoBuilder.build();
    }
}
