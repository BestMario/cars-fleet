package com.mario.carsfleet.car.domain;

import com.mario.carsfleet.car.ReportsController;
import com.mario.carsfleet.car.dto.FuelDto;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

@Component
public class PdfReportsFacade {
    
    private final CarFacade carFacade;
    private static final Logger logger = LoggerFactory.getLogger(ReportsController.class);

    @Autowired
    public PdfReportsFacade(CarFacade carFacade) {
        this.carFacade = carFacade;
    }

    public void preparePdfReportForSingleCar(Integer carId, LocalDate refuelingDateFrom, LocalDate refuelingDateTo, HttpServletResponse response) {
        List<FuelDto> allRefuelings = carFacade.findAllFuelBy(carId, refuelingDateFrom, refuelingDateTo);
        makePdfReport(response, allRefuelings);
    }

    private String formatFuel(FuelDto fuel) {
        return fuel.getCarId().toString() + ", " +
                fuel.getAmountInLiters().toString() + ", " +
                fuel.getRefuelingDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ", " +
                fuel.getPriceInPln().toString() + ", " +
                fuel.getFuelType().toString() + ", " +
                fuel.getMileageInKm().toString();
    }

    public void preparePdfReportForAllCars(LocalDate refuelingDateFrom, LocalDate refuelingDateTo, HttpServletResponse response) {
        List<FuelDto> allRefuelings = carFacade.findAllFuel(refuelingDateFrom, refuelingDateTo);

        makePdfReport(response, allRefuelings);
    }

    private void makePdfReport(HttpServletResponse response, List<FuelDto> allRefuelings) {
        response.addHeader("Content-Type", "application/force-download");
        response.addHeader("Content-Disposition", "attachment; filename=\"report.pdf\"");

        PDDocument document = new PDDocument();
        PDPage page = new PDPage();
        page.setMediaBox(PDRectangle.A4);
        document.addPage(page);

        PDFont font = PDType1Font.HELVETICA_BOLD_OBLIQUE;
        PDPageContentStream contentStream = null;

        try {

            contentStream = new PDPageContentStream(document, page);
            final float fontSize = 14;
            final float margin = 50;
            float totalHeight = 50;
            configureNewContentStream(font, contentStream, fontSize);

            for (FuelDto fuel : allRefuelings) {

                String fuelText = formatFuel(fuel);

                contentStream.showText(fuelText);
                contentStream.newLine();
                float lineHeight = calculateLineHeight(font, fontSize);
                totalHeight += lineHeight;

                float a4Height= PDRectangle.A4.getHeight();
                if(totalHeight+margin>=a4Height){
                    page = new PDPage();
                    page.setMediaBox(PDRectangle.A4);
                    document.addPage(page);

                    contentStream.endText();
                    contentStream.close();
                    contentStream = new PDPageContentStream(document, page);
                    configureNewContentStream(font, contentStream, fontSize);
                }

            }
        } catch (IOException ie) {
            logger.error("exception while saving", ie);
        } finally {
            try {
                if (Objects.nonNull(contentStream)) {
                    contentStream.endText();

                    contentStream.close();
                    document.save(response.getOutputStream());
                }
            } catch (IOException e) {
                logger.error("exception while closing", e);
            }

            try {
                document.close();

            } catch (IOException e) {
                logger.error("exception while closing", e);
            }
        }
    }

    private void configureNewContentStream(PDFont font, PDPageContentStream contentStream, float fontSize) throws IOException {
        contentStream.beginText();
        contentStream.setFont(font, fontSize);
        contentStream.setLeading(14.5f);
        contentStream.newLineAtOffset(25, 700);
    }

    private float calculateLineHeight(PDFont font, float fontSize) {
        return font.getFontDescriptor().getFontBoundingBox()
                .getHeight()
                / 1000 * fontSize;

    }
}
