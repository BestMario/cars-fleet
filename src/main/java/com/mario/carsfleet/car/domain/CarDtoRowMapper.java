package com.mario.carsfleet.car.domain;


import com.mario.carsfleet.car.dto.CarDriverDto;
import com.mario.carsfleet.car.dto.CarDto;
import com.mario.carsfleet.car.dto.CarStatusDto;
import com.mario.carsfleet.car.dto.PositionDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

class CarDtoRowMapper implements RowMapper<CarDto> {
    @Override
    public CarDto mapRow(ResultSet rs, int rowNum) throws SQLException {
        Integer carId = rs.getInt("id");
        Integer currentDriverId = readCurrentDriverId(rs);
        boolean isAssigned = Objects.nonNull(currentDriverId);
        String make = rs.getString("make");
        String carModel = rs.getString("car_model");
        int manufacturedYear = rs.getInt("manufactured_year");
        CarStatusDto carStatus = readCarStatusOptions(rs);
        String firstName = rs.getString("first_name");
        String lastName = rs.getString("last_name");
        PositionDto position = readPosition(rs);
        String numberPlate = rs.getString("number_plate");

        CarDriverDto carDriver = new CarDriverDto(currentDriverId, isAssigned, firstName, lastName);
        return new CarDto(carId, currentDriverId, make, carModel, manufacturedYear, carStatus, carDriver, position, numberPlate);
    }

    private PositionDto readPosition(ResultSet rs) throws SQLException {
        Long longitude = rs.getLong("longitude");
        Long latitude = rs.getLong("latitude");
        return new PositionDto(longitude, latitude);
    }

    private Integer readCurrentDriverId(ResultSet rs) throws SQLException {
        Integer rawCurrentDriverId = rs.getInt("current_driver_id");
        if (rawCurrentDriverId == 0L) {
            return null;
        }
        return rawCurrentDriverId;
    }

    private CarStatusDto readCarStatusOptions(ResultSet rs) throws SQLException {
        String rawCarStatus = rs.getString("car_status");
        if (StringUtils.isNotBlank(rawCarStatus)) {
            CarStatus carStatus = CarStatus.valueOf(rawCarStatus);
            return CarStatus.convert(carStatus);
        }
        return null;

    }
}
