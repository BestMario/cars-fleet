package com.mario.carsfleet.car.domain;

import com.mario.carsfleet.car.dto.FuelDto;
import com.mario.carsfleet.car.dto.FuelTypeFormOptions;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

class FuelDtoRowMapper implements RowMapper<FuelDto> {
    @Override
    public FuelDto mapRow(ResultSet rs, int rowNum) throws SQLException {
        Long fuelId = rs.getLong("id");
        LocalDate refuelingDate = rs.getObject("refueling_date", LocalDate.class);
        BigDecimal amountInLiters = rs.getBigDecimal("amount_in_liters");
        FuelTypeFormOptions fuelType = readFuelTypeFormOptions(rs);
        BigDecimal priceInPln = rs.getBigDecimal("price_in_pln");
        long mileageInKm = rs.getLong("mileage_in_km");
        Long carId = rs.getLong("car_id");
        return new FuelDto(fuelId, carId, refuelingDate, amountInLiters, fuelType, priceInPln, mileageInKm);
    }

    private FuelTypeFormOptions readFuelTypeFormOptions(ResultSet rs) throws SQLException {
        String rawFuelType = rs.getString("fuel_type");
        if (StringUtils.isNotBlank(rawFuelType)) {
            FuelType fuelType = FuelType.valueOf(rawFuelType);
            return FuelType.convert(fuelType);
        }
        return null;
    }
}
