package com.mario.carsfleet.car.domain;

import org.springframework.data.repository.CrudRepository;

interface CarRepository extends CrudRepository<Car, Integer> {
}
