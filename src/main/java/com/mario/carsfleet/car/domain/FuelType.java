package com.mario.carsfleet.car.domain;

import com.mario.carsfleet.car.dto.FuelTypeFormOptions;

enum FuelType {
    PETROL,
    DIESEL,
    LPG;

    static FuelType convert(FuelTypeFormOptions formOptions) {
        if (FuelTypeFormOptions.PETROL.equals(formOptions)) {
            return FuelType.PETROL;
        } else if (FuelTypeFormOptions.DIESEL.equals(formOptions)) {
            return FuelType.DIESEL;
        } else if (FuelTypeFormOptions.LPG.equals(formOptions)) {
            return FuelType.LPG;
        }
        return null;
    }

    static FuelTypeFormOptions convert(FuelType clientStatus) {
        if (FuelType.PETROL.equals(clientStatus)) {
            return FuelTypeFormOptions.PETROL;
        } else if (FuelType.DIESEL.equals(clientStatus)) {
            return FuelTypeFormOptions.DIESEL;
        } else if (FuelType.LPG.equals(clientStatus)) {
            return FuelTypeFormOptions.LPG;
        }
        return null;
    }
}
