package com.mario.carsfleet.car.domain;

import com.mario.carsfleet.car.dto.CarStatusDto;

enum CarStatus {
    NEW,
    IN_REPAIR,
    DAMAGED,
    EFFICIENT;

    static CarStatus convert(CarStatusDto carStatusDto) {
        if (CarStatusDto.NEW.equals(carStatusDto)) {
            return CarStatus.NEW;
        } else if (CarStatusDto.IN_REPAIR.equals(carStatusDto)) {
            return CarStatus.IN_REPAIR;
        } else if (CarStatusDto.DAMAGED.equals(carStatusDto)) {
            return CarStatus.DAMAGED;
        } else if (CarStatusDto.EFFICIENT.equals(carStatusDto)) {
            return CarStatus.EFFICIENT;
        }
        return null;
    }

    static CarStatusDto convert(CarStatus carStatus) {
        if (CarStatus.NEW.equals(carStatus)) {
            return CarStatusDto.NEW;
        } else if (CarStatus.IN_REPAIR.equals(carStatus)) {
            return CarStatusDto.IN_REPAIR;
        } else if (CarStatus.DAMAGED.equals(carStatus)) {
            return CarStatusDto.DAMAGED;
        } else if (CarStatus.EFFICIENT.equals(carStatus)) {
            return CarStatusDto.EFFICIENT;
        }
        return null;
    }
}

