package com.mario.carsfleet.car.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
class Fuel {

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private LocalDate refuelingDate;
    private BigDecimal amountInLiters;

    @Column(name = "fuel_type")
    @Enumerated(EnumType.STRING)
    private FuelType fuelType;

    private BigDecimal priceInPln;
    private Long mileageInKm;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getRefuelingDate() {
        return refuelingDate;
    }

    public void setRefuelingDate(LocalDate refuelingDate) {
        this.refuelingDate = refuelingDate;
    }

    public BigDecimal getAmountInLiters() {
        return amountInLiters;
    }

    public void setAmountInLiters(BigDecimal amountInLiters) {
        this.amountInLiters = amountInLiters;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }

    public BigDecimal getPriceInPln() {
        return priceInPln;
    }

    public void setPriceInPln(BigDecimal priceInPln) {
        this.priceInPln = priceInPln;
    }

    public Long getMileageInKm() {
        return mileageInKm;
    }

    public void setMileageInKm(Long mileageInKm) {
        this.mileageInKm = mileageInKm;
    }
}
