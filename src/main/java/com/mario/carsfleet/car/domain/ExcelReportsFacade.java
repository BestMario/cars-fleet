package com.mario.carsfleet.car.domain;

import com.mario.carsfleet.car.ReportsController;
import com.mario.carsfleet.car.dto.FuelDto;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Component
public class ExcelReportsFacade {

    private final CarFacade carFacade;
    private static final Logger logger = LoggerFactory.getLogger(ReportsController.class);

    @Autowired
    public ExcelReportsFacade(CarFacade carFacade) {
        this.carFacade = carFacade;
    }

    public void prepareExcelReportForSingleCar(Integer carId, LocalDate refuelingDateFrom, LocalDate refuelingDateTo, HttpServletResponse response) {
        List<FuelDto> allRefuelings = carFacade.findAllFuelBy(carId, refuelingDateFrom, refuelingDateTo);
        makeExcelReport(response, allRefuelings);
    }

    private void setExcelContentType(HttpServletResponse response) {
        ReportUtils.setContentType(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    }

    private void setContentDispositionForAllCarsExcel(HttpServletResponse response) {
        ReportUtils.setContentDisposition(response, "fuel-consumption.xlsx", "attachment; filename=\"");
    }

    private void makeExcelReport(HttpServletResponse response, List<FuelDto> allRefuelings) {

        setExcelContentType(response);
        setContentDispositionForAllCarsExcel(response);
        int rownum = 0;
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Car report");

        for (FuelDto fuel : allRefuelings) {
            Row row = sheet.createRow(rownum++);

            Cell carIdCell = row.createCell(0);
            carIdCell.setCellValue(fuel.getCarId().toString());

            Cell amountInLitersCell = row.createCell(1);
            amountInLitersCell.setCellValue(fuel.getAmountInLiters().doubleValue());

            Cell refuelingDateCell = row.createCell(2);
            Date refuelingDate = Date.from(fuel.getRefuelingDate().atStartOfDay()
                    .atZone(ZoneId.systemDefault())
                    .toInstant());
            refuelingDateCell.setCellValue(refuelingDate);

            Cell priceInPlnCell = row.createCell(3);
            priceInPlnCell.setCellValue(fuel.getPriceInPln().doubleValue());

            Cell fuelTypeCell = row.createCell(4);
            fuelTypeCell.setCellValue(fuel.getFuelType().toString());

            Cell mileageInKmCell = row.createCell(5);
            mileageInKmCell.setCellValue(fuel.getMileageInKm().toString());
        }

        try {
            workbook.write(response.getOutputStream());
        } catch (Exception e) {
            logger.error("exception while writing to workbook", e);
        } finally {
            try {
                workbook.close();
            } catch (IOException e) {
                logger.error("cannot close workbook", e);
            }
        }
    }

    public void prepareExcelReportForAllCars(LocalDate refuelingDateFrom, LocalDate refuelingDateTo, HttpServletResponse response) {
        List<FuelDto> allRefuelings = carFacade.findAllFuel(refuelingDateFrom, refuelingDateTo);
       makeExcelReport(response, allRefuelings);
    }
}
