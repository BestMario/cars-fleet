package com.mario.carsfleet.car.domain;

import com.mario.carsfleet.car.dto.FuelDto;
import com.mario.carsfleet.car.dto.FuelTypeFormOptions;
import com.opencsv.CSVWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

@Component
public class CsvReportsFacade {

    private final CarFacade carFacade;

    @Autowired
    public CsvReportsFacade(CarFacade carFacade) {
        this.carFacade = carFacade;
    }

    public void prepareCsvReportForSingleCar(@PathVariable(name = "carId") Integer carId, @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, pattern = "yyyy-MM-dd") @RequestParam("refueling-date-from") LocalDate refuelingDateFrom, @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, pattern = "yyyy-MM-dd") @RequestParam("refueling-date-to") LocalDate refuelingDateTo, HttpServletResponse response) throws IOException {
        prepareSingleCarReport(carId, refuelingDateFrom, refuelingDateTo, response);
    }

    private void setCsvTxtContentType(HttpServletResponse response) {
        ReportUtils.setContentType(response, "text/csv");
    }

    public void setContentDispositionForCsv(HttpServletResponse response) {
        ReportUtils.setContentDisposition(response, "fuel-consumption.csv", "attachment; filename=\"");
    }

    public void prepareSingleCarReport(Integer carId, LocalDate refuelingDateFrom, LocalDate refuelingDateTo, HttpServletResponse response) throws IOException {
        List<FuelDto> allRefuelings = carFacade.findAllFuelBy(carId, refuelingDateFrom, refuelingDateTo);
        makeCsvReport(response, allRefuelings);
    }

    private void makeCsvReport(HttpServletResponse response, List<FuelDto> allRefuelings) throws IOException {
        setCsvTxtContentType(response);
        setContentDispositionForCsv(response);
        CSVWriter writer = null;
        try {
            PrintWriter printWriter = response.getWriter();
            writer = new CSVWriter(printWriter);

            for (FuelDto fuel : allRefuelings) {
                String rawCarId = fuel.getCarId().toString();

                BigDecimal amountInLiters = fuel.getAmountInLiters();
                String amountInLitersAsString = amountInLiters.toString();

                LocalDate refuelingDate = fuel.getRefuelingDate();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                String formattedString = refuelingDate.format(formatter);

                BigDecimal priceInPln = fuel.getPriceInPln();
                String priceInPlnAsString = priceInPln.toString();

                FuelTypeFormOptions fuelType = fuel.getFuelType();
                String fuelTypeAsString = fuelType.toString();

                Long mileageInKm = fuel.getMileageInKm();
                String mileageInKmAsString = mileageInKm.toString();

                String[] fuelConsumption = new String[]{rawCarId, amountInLitersAsString, formattedString,
                        priceInPlnAsString, fuelTypeAsString, mileageInKmAsString};
                writer.writeNext(fuelConsumption);
            }
        } finally {
            if (Objects.nonNull(writer)) {
                writer.close();
            }
        }
    }

    public void prepareReportCsvForAllCars(LocalDate refuelingDateFrom, @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, pattern = "yyyy-MM-dd") @RequestParam(name = "refueling-date-to") LocalDate refuelingDateTo, HttpServletResponse response) throws IOException {
        prepareAllCarsReport(refuelingDateFrom, refuelingDateTo, response);
    }

    private void prepareAllCarsReport(LocalDate refuelingDateFrom, @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, pattern = "yyyy-MM-dd") @RequestParam(name = "refueling-date-to") LocalDate refuelingDateTo, HttpServletResponse response) throws IOException {
        List<FuelDto> allRefuelings = carFacade.findAllFuel(refuelingDateFrom, refuelingDateTo);
        makeCsvReport(response, allRefuelings);
    }
}
