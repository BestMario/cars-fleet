package com.mario.carsfleet.car.domain;

import javax.persistence.*;
import java.util.List;
import java.util.Optional;

@Entity
public class Car {

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String make;
    private Integer manufacturedYear;
    private String numberPlate;

    @Embedded
    private Position position;

    @Column(name = "car_model")
    private String model;

    @Column(name = "car_status")
    @Enumerated(EnumType.STRING)
    private CarStatus carStatus;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "current_driver_id", referencedColumnName = "id")
    private Driver currentDriver;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "car_id", referencedColumnName = "id")
    private List<Fuel> fuelConsumption;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getNumberPlate() {
        return numberPlate;
    }

    public void setNumberPlate(String numberPlate) {
        this.numberPlate = numberPlate;
    }

    public CarStatus getCarStatus() {
        return carStatus;
    }

    public void setCarStatus(CarStatus carStatus) {
        this.carStatus = carStatus;
    }

    public Integer getManufacturedYear() {
        return manufacturedYear;
    }

    public void setManufacturedYear(Integer manufacturedYear) {
        this.manufacturedYear = manufacturedYear;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Driver getCurrentDriver() {
        return currentDriver;
    }

    public void setCurrentDriver(Driver currentDriver) {
        this.currentDriver = currentDriver;
    }

    public List<Fuel> getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(List<Fuel> fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public void deleteFuelById(Integer fuelId) {
        Optional<Fuel> f = this.fuelConsumption.stream().filter(fuel -> fuel.getId().equals(fuelId)).findFirst();
        if (f.isPresent()) {
            Fuel fuelForDelete = f.get();
            this.fuelConsumption.remove(fuelForDelete);
        }
    }

}
