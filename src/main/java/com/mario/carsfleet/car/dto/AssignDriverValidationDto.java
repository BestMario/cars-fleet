package com.mario.carsfleet.car.dto;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

@Value
@Builder
@ToString
public class AssignDriverValidationDto {
    private String assignedErrors;

    public boolean isValid() {
        return StringUtils.isBlank(assignedErrors);
    }
}

