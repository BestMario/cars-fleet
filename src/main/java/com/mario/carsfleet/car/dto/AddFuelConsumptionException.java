package com.mario.carsfleet.car.dto;

public class AddFuelConsumptionException extends RuntimeException {
    private final AddFuelConsumptionValidationDto addFuelConsumptionValidation;

    public AddFuelConsumptionException(AddFuelConsumptionValidationDto addFuelConsumptionValidation) {
        this.addFuelConsumptionValidation = addFuelConsumptionValidation;
    }

    public AddFuelConsumptionValidationDto getAddFuelConsumptionValidation() {
        return addFuelConsumptionValidation;
    }
}
