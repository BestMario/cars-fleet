package com.mario.carsfleet.car.dto;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

@Value
@Builder
@ToString
public class UpdateCarValidationDto {
    private String makeErrors;
    private String modelErrors;
    private String manufacturedYearErrors;
    private String numberPlateErrors;

    public boolean isValid() {
        return StringUtils.isBlank(makeErrors) && StringUtils.isBlank(modelErrors)
                && StringUtils.isBlank(manufacturedYearErrors) && StringUtils.isBlank(numberPlateErrors);
    }
}
