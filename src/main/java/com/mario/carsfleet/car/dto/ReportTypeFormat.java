package com.mario.carsfleet.car.dto;

public enum ReportTypeFormat {
    CSV,
    EXCEL,
    PDF
}
