package com.mario.carsfleet.car.dto;

import lombok.Value;

import java.math.BigDecimal;
import java.time.LocalDate;

@Value
public class AddFuelConsumptionCommand {
    private LocalDate refuelingDate;
    private BigDecimal amountInLiters;
    private FuelTypeFormOptions fuelType;
    private BigDecimal priceInPln;
    private Long mileageInKm;

}
