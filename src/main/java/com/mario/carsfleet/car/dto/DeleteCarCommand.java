package com.mario.carsfleet.car.dto;

import lombok.Value;

@Value
public class DeleteCarCommand {
    private Integer id;
}
