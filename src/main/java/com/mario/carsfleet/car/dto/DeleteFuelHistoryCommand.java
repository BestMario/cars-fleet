package com.mario.carsfleet.car.dto;

import lombok.Value;

@Value
public class DeleteFuelHistoryCommand {
    private Integer carId;
    private Integer fuelId;
}
