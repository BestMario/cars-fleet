package com.mario.carsfleet.car.dto;

import lombok.Value;

import java.util.List;

@Value
public class FuelResultPage {
    private List<FuelDto> fuelConsumptions;
    private Integer pageNumber;
    private Integer numberOfPages;
    private Integer pageSize;
    private Integer count;
}
