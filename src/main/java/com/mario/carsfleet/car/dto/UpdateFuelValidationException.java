package com.mario.carsfleet.car.dto;

public class UpdateFuelValidationException extends RuntimeException {
    private final UpdateFuelValidationDto updateFuelValidation;

    public UpdateFuelValidationException(UpdateFuelValidationDto updateFuelValidation) {
        this.updateFuelValidation = updateFuelValidation;
    }

    public UpdateFuelValidationDto getUpdateFuelValidation() {
        return updateFuelValidation;
    }
}
