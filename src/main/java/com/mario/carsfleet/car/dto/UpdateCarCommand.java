package com.mario.carsfleet.car.dto;

import lombok.Value;

@Value
public class UpdateCarCommand {
    private String make;
    private String model;
    private Integer manufacturedYear;
    private String numberPlate;
}
