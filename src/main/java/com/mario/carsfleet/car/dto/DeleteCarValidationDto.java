package com.mario.carsfleet.car.dto;

import lombok.Builder;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

@Value
@Builder
public class DeleteCarValidationDto {
    private String carIdErrors;

    public boolean isValid() {
        return StringUtils.isBlank(carIdErrors);
    }
}
