package com.mario.carsfleet.car.dto;

public enum CarStatusDto {
    NEW,
    IN_REPAIR,
    DAMAGED,
    EFFICIENT
}
