package com.mario.carsfleet.car.dto;

public enum FuelTypeFormOptions {
    PETROL,
    DIESEL,
    LPG
}
