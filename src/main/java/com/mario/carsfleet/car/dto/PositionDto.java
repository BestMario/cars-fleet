package com.mario.carsfleet.car.dto;

import lombok.Value;

@Value
public class PositionDto {
    private Long latitude;
    private Long longitude;
}
