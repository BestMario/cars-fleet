package com.mario.carsfleet.car.dto;

import lombok.Value;

import java.util.List;

@Value
public class DriversResultPage {
    private List<CarDriverDto> drivers;
    private Integer pageNumber;
    private Integer numberOfPages;
    private Integer pageSize;
    private Integer count;
    private String filteringString;
}
