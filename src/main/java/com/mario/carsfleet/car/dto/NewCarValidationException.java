package com.mario.carsfleet.car.dto;

public class NewCarValidationException extends RuntimeException {
    private final NewCarValidationDto newCarValidation;


    public NewCarValidationException(NewCarValidationDto newCarValidation) {
        this.newCarValidation = newCarValidation;
    }

    public NewCarValidationDto getNewCarValidation() {
        return newCarValidation;
    }
}
