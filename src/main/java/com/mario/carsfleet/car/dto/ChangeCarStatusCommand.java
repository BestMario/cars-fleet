package com.mario.carsfleet.car.dto;

import lombok.Value;

@Value
public class   ChangeCarStatusCommand {
    private Integer id;
    private CarStatusDto carStatus;
}
