package com.mario.carsfleet.car.dto;

import lombok.Value;

@Value
public class CarDto {
    private Integer carId;
    private Integer driverId;
    private String make;
    private String model;
    private Integer manufacturedYear;
    private CarStatusDto carStatus;
    private CarDriverDto currentDriver;
    private PositionDto position;
    private String numberPlate;
}
