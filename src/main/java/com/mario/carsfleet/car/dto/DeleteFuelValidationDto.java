package com.mario.carsfleet.car.dto;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

@Value
@ToString
@Builder
public class DeleteFuelValidationDto {
    private String carIdErrors;
    private String fuelIdErrors;

    public boolean isValid() {
        return StringUtils.isBlank(carIdErrors) && StringUtils.isBlank(fuelIdErrors);
    }
}
