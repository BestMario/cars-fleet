package com.mario.carsfleet.car.dto;

public class DeleteCarValidationException extends RuntimeException {
    private final DeleteCarValidationDto deleteCarValidation;

    public DeleteCarValidationException(DeleteCarValidationDto deleteCarValidation) {
        this.deleteCarValidation = deleteCarValidation;
    }

    public DeleteCarValidationDto getDeleteCarValidation() {
        return deleteCarValidation;
    }
}
