package com.mario.carsfleet.car.dto;

import lombok.Value;

@Value
public class AddNewCarCommand {
    private String make;
    private String model;
    private Integer manufacturedYear;
    private String numberPlate;
}
