package com.mario.carsfleet.car.dto;

public class AssignDriverToCarValidationException extends RuntimeException {
    private final AssignDriverValidationDto validationDto;

    public AssignDriverToCarValidationException(AssignDriverValidationDto validationDto) {
        super(validationDto.toString());
        this.validationDto = validationDto;
    }

    public AssignDriverValidationDto getValidationDto() {
        return validationDto;
    }
}
