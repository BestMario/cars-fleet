package com.mario.carsfleet.car.dto;

import lombok.Value;

import java.util.List;

@Value
public class CarResultPage {
   private List<CarDto> cars;
   private Integer pageNumber;
   private Integer numberOfPages;
   private Integer pageSize;
   private Integer count;
   private String filteringText;

}
