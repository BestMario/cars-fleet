package com.mario.carsfleet.car.dto;

import lombok.Value;

@Value
public class CarDriverDto {
    private Integer driverId;
    private boolean assigned;
    private String firstName;
    private String lastName;
}
