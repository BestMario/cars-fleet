package com.mario.carsfleet.car.dto;

public class DeleFuelValidationException extends RuntimeException {
    private final DeleteFuelValidationDto deleteFuelValidation;

    public DeleFuelValidationException(DeleteFuelValidationDto deleteFuelValidation) {
        this.deleteFuelValidation = deleteFuelValidation;
    }

    public DeleteFuelValidationDto getDeleteFuelValidation() {
        return deleteFuelValidation;
    }
}
