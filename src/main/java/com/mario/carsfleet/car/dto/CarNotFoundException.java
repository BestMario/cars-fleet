package com.mario.carsfleet.car.dto;

public class CarNotFoundException extends RuntimeException {

    private final Integer carId;

    public CarNotFoundException(Integer carId) {
        super("Car not found: " + carId);
        this.carId = carId;
    }

    public Integer getCarId() {
        return carId;
    }
}
