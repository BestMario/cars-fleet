package com.mario.carsfleet.car.dto;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

@Value
@ToString
@Builder
public class AddFuelConsumptionValidationDto {
    private String carIdErrors;
    private String  refuelingDateErrors;
    private String amountInLitersErrors;
    private String priceInPlnErrors;
    private String mileageInKmErrors;

    public boolean isValid() {
        return StringUtils.isBlank(carIdErrors) && StringUtils.isBlank(refuelingDateErrors) &&
                StringUtils.isBlank(amountInLitersErrors) && StringUtils.isBlank(priceInPlnErrors) &&
                StringUtils.isBlank(mileageInKmErrors);
    }
}
