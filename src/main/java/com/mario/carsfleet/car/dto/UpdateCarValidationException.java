package com.mario.carsfleet.car.dto;

public class UpdateCarValidationException extends RuntimeException {
    private final UpdateCarValidationDto updateCarValidation;


    public UpdateCarValidationException(UpdateCarValidationDto updateCarValidation) {
        this.updateCarValidation = updateCarValidation;
    }

    public UpdateCarValidationDto getUpdateCarValidation() {
        return updateCarValidation;
    }
}
