package com.mario.carsfleet.car.dto;

import lombok.Value;

@Value
public class
AddDriverToCarCommand {
    private Integer carId;
    private Integer driverId;

}
