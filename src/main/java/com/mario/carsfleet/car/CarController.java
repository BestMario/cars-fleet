package com.mario.carsfleet.car;

import com.mario.carsfleet.car.domain.CarFacade;
import com.mario.carsfleet.car.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
public class CarController {

    private static final Logger logger = LoggerFactory.getLogger(CarController.class);

    private final CarFacade carFacade;

    @Autowired
    public CarController(CarFacade carFacade) {
        this.carFacade = carFacade;
    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping("/api/cars")
    public void add(@RequestBody AddNewCarCommand newCar) {
        carFacade.add(newCar);
    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping("/api/cars/{carId}/new-refueling")
    public void addFuelConsumption(@PathVariable(name = "carId") Integer carId,
                                   @RequestBody AddFuelConsumptionCommand fuelConsumptionCommand) {
        carFacade.addFuelConsumption(carId, fuelConsumptionCommand);
    }

    @GetMapping("/api/cars/{id}")
    public CarDto findById(@PathVariable("id") Integer carId) {
        return carFacade.findById(carId);
    }


    @PatchMapping("/api/cars/{id}")
    public void update(@PathVariable("id") Integer carId, @RequestBody UpdateCarCommand updateCar) {
        carFacade.update(carId, updateCar);
    }

    @PatchMapping("/api/cars/{carId}/fuel/{fuelId}")
    public void updateFuel(@PathVariable(name = "carId") Integer carId, @PathVariable(name = "fuelId") Integer fuelId,
                           @RequestBody UpdateFuelCommand updateFuelCommand) {
        carFacade.updateFuel(carId, fuelId, updateFuelCommand);
    }

    @DeleteMapping("/api/cars/{id}")
    public void delete(@PathVariable("id") Integer id) {
        DeleteCarCommand carCommand = new DeleteCarCommand(id);
        carFacade.delete(carCommand);
    }

    @GetMapping(value = "/api/cars")
    public List<CarDto> findCars() {
        return carFacade.findCars();
    }


    @GetMapping(value = "/api/cars", params = "page-number")
    public CarResultPage findBy(@RequestParam(name = "page-number", defaultValue = "1") Integer pageNumber,
                                @RequestParam(name = "filter", required = false, defaultValue = "") String filteringText) {
        return carFacade.findBy(pageNumber, filteringText);
    }

    @GetMapping("/api/cars/drivers")
    public DriversResultPage findDriversBy(@RequestParam(name = "page-number", defaultValue = "1") Integer pageNumber,
                                           @RequestParam(name = "filter", required = false, defaultValue = "") String filteringText) {
        return carFacade.findDriversBy(pageNumber, filteringText);
    }

    @GetMapping("/api/cars/{carId}/fuel-history")
    public FuelResultPage findFuelBy(@PathVariable(name = "carId") Integer carId,
                                     @RequestParam(name = "page-number", defaultValue = "1") Integer pageNumber) {
        return carFacade.findFuelBy(carId, pageNumber);
    }

    @GetMapping("/api/cars/fuel/{fuelId}")
    public FuelDto findFuelById(@PathVariable(name = "fuelId") Integer fuelId) {
        return carFacade.findFuelById(fuelId);
    }

    @PatchMapping("/api/cars/{carId}/drivers/{driverId}")
    public void assignDriverToCar(@PathVariable(name = "carId") Integer carId,
                                  @PathVariable(name = "driverId") Integer driverId) {
        AddDriverToCarCommand driverToCarCommand = new AddDriverToCarCommand(carId, driverId);
        carFacade.addDriverToCar(driverToCarCommand);
    }

    @DeleteMapping("/api/cars/{carId}/current-driver")
    public void detachDriverFromCar(@PathVariable(name = "carId") Integer carId) {
        DetachDriverFromCarCommand driverFromCarCommand = new DetachDriverFromCarCommand(carId);
        carFacade.detachDriverFromCar(driverFromCarCommand);

    }

    @ResponseStatus(value = HttpStatus.OK)
    @DeleteMapping("/api/cars/{carId}/fuel-history/{fuelId}")
    public void deleteFuel(@PathVariable(name = "carId") Integer carId,
                           @PathVariable(name = "fuelId") Integer fuelId) {
        DeleteFuelHistoryCommand fuelHistoryCommand = new DeleteFuelHistoryCommand(carId, fuelId);
        carFacade.deleteFuel(fuelHistoryCommand);
    }

    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(value = {NewCarValidationException.class})
    public NewCarValidationDto handlerNewCarValidationException(NewCarValidationException ex) {
        logger.info("validation exception", ex);
        return ex.getNewCarValidation();
    }

    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(value = {UpdateCarValidationException.class})
    public UpdateCarValidationDto handlerUpdateCarValidationException(UpdateCarValidationException editEx) {
        logger.info("validation exception", editEx);
        return editEx.getUpdateCarValidation();
    }

    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(value = {DeleteCarValidationException.class})
    public DeleteCarValidationDto handlerDeleteValidationException(DeleteCarValidationException delEx) {
        logger.info("validation exception", delEx);
        return delEx.getDeleteCarValidation();
    }

    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(value = {AssignDriverToCarValidationException.class})
    public AssignDriverValidationDto handleAsigneDriverToCarValidationException(AssignDriverToCarValidationException assignEx) {
        logger.info("validation exception", assignEx);
        return assignEx.getValidationDto();
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = {CarNotFoundException.class})
    public String handleCarNotFoundException(CarNotFoundException ex) {
        logger.info("Car not found", ex);
        return "Car not found";
    }



    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(value = {AddFuelConsumptionException.class})
    public AddFuelConsumptionValidationDto handleAddNewFuelConsumptionexception(AddFuelConsumptionException addFuelEx) {
        logger.info("Fuel consumption not found");
        return addFuelEx.getAddFuelConsumptionValidation();
    }

    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(value = UpdateFuelValidationException.class)
    public UpdateFuelValidationDto handleUpdateFuelValidationException(UpdateFuelValidationException updateFuelEx) {
        logger.info("validation exception", updateFuelEx);
        return updateFuelEx.getUpdateFuelValidation();
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = DeleFuelValidationException.class)
    public DeleteFuelValidationDto handleDeleteFuelValidationException(DeleFuelValidationException delFuelEx) {
        logger.info("validation exception");
        return delFuelEx.getDeleteFuelValidation();
    }
}
