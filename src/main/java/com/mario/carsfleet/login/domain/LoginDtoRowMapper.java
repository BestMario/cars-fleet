package com.mario.carsfleet.login.domain;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

class LoginDtoRowMapper implements RowMapper<LoginDto> {

    @Override
    public LoginDto mapRow(ResultSet rs, int rowNum) throws SQLException {
        Integer id = rs.getInt("id");
        String emailAddress = rs.getString("email_address");
        String password = rs.getString("password");
        String role = rs.getString("roll");

        return new LoginDto(id, emailAddress, password, role);
    }
}
