package com.mario.carsfleet.login.domain;

import lombok.Value;

@Value
public class LoginDto {
    private Integer id;
    private String email;
    private String password;
    private String role;
}
