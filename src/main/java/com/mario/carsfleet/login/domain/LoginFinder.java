package com.mario.carsfleet.login.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class LoginFinder {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public LoginFinder(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public LoginDto findByEmail(String email) {
        final String query = "select id, email_address, password, roll from account where email_address=:email_address";
        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("email_address", email);
        LoginDtoRowMapper rowMapper = new LoginDtoRowMapper();
        return namedParameterJdbcTemplate.queryForObject(query, namedParameter, rowMapper);
    }
}
