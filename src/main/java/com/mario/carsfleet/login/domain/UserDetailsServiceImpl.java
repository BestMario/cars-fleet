//package com.mario.carsfleet.login.domain;
//
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.Objects;
//
//@Service
//public class UserDetailsServiceImpl implements UserDetailsService {
//
//    private final LoginFinder loginFinder;
//
//    public UserDetailsServiceImpl(LoginFinder loginFinder) {
//        this.loginFinder = loginFinder;
//    }
//
//    @Override
//    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
//
//        LoginDto login = loginFinder.findByEmail(email);
//        if (Objects.isNull(login)) {
//            throw new UsernameNotFoundException("User not found " + email);
//        }
//        return new User(login.getEmail(), login.getPassword(), new ArrayList<>());
//    }
//}
