package com.mario.carsfleet.login;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

//@CrossOrigin(origins={ "http://localhost:3000"})
@CrossOrigin(origins={
        "http://localhost:3000",
        "http://localhost:8080",
        "http://car-fleet-management.appspot.com",
        "http://car-fleet-management.appspot.com:8080"
})
@RestController
public class BasicAuthenticationController {

    @GetMapping(path = "/basicauth")
    public String authenticate() {
        //throw new RuntimeException("Some Error has Happened! Contact Support at ***-***");
        return "You are authenticated";
    }

}
