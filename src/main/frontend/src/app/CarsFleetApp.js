import React from 'react'
import MenuComponent from "../menu/MenuComponent";
import AuthenticatedRoute from "../login/AuthenticatedRoute";
import '../map/MapPage'
import MapPage from "../map/MapPage"
import Cars from '../cars/Cars'
import NewCar from '../cars/NewCar'
import Edit from '../cars/Edit'
import {BrowserRouter as Router, Route, Switch} from "react-router-dom"
import Drivers from '../drivers/Drivers'
import AddDriverToCar from '../cars/AddDriverToCar'
import NewDriver from '../drivers/NewDriver'
import EditDrivers from '../drivers/EditDrivers'
import Fuel from '../cars/Fuel'
import NewFuel from '../cars/NewFuel'
import EditFuel from '../cars/EditFuel'
import Reports from '../raports/Reports'
import Users from '../users/Users'
import NewUser from '../users/NewUser'
import EditUser from '../users/EditUser'
import LoginPage from "../login/LoginPage"

class CarsFleetApp extends React.Component {

    render() {

        return (
            <div className="App">
                <Router>
                     <MenuComponent/>
                    <main className="p-2">
                        <Switch>
                            <Route path="/" exact component={LoginPage}/>
                            <AuthenticatedRoute path="/map" exact component={MapPage}/>
                            <AuthenticatedRoute path="/cars" exact component={Cars}/>
                            <AuthenticatedRoute path="/cars/new" exact component={NewCar}/>
                            <AuthenticatedRoute path="/cars/:id" exact component={Edit}/>
                            <AuthenticatedRoute path="/drivers" exact component={Drivers}/>
                            <AuthenticatedRoute path="/cars/:carId/driver" exact component={AddDriverToCar}/>
                            <AuthenticatedRoute path="/drivers/new" exact component={NewDriver}/>
                            <AuthenticatedRoute path="/drivers/:id" exact component={EditDrivers}/>
                            <AuthenticatedRoute path="/cars/:carId/fuel" exact component={Fuel}/>
                            <AuthenticatedRoute path="/cars/:carId/new-refueling" exact component={NewFuel}/>
                            <AuthenticatedRoute path="/cars/:carId/fuel/:fuelId" exact component={EditFuel}/>
                            <AuthenticatedRoute path="/raports" exact component={Reports}/>
                            <AuthenticatedRoute path="/users" exact component={Users}/>
                            <AuthenticatedRoute path="/users/new" exact component={NewUser}/>
                            <AuthenticatedRoute path="/users/:id" exact component={EditUser}/>
                            <AuthenticatedRoute path="/logout" exact component={LoginPage}/>
                            <Route path="/login" exact component={LoginPage}/>

                        </Switch>
                    </main>
                </Router>
            </div>
        );
    }

}

export default CarsFleetApp;