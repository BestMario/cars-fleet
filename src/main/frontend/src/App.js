import React from 'react'
import './App.css'
import CarsFleetApp from "./app/CarsFleetApp"

function App() {

    return (
        <CarsFleetApp/>
    )
}

export default App;
