import React from 'react'
import {withRouter, Link} from "react-router-dom";
import AuthenticationService from "../login/AuthenticationService";

import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';

class MenuComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        };

        this.handleToggle = this.handleToggle.bind(this)
    }

    handleToggle() {
        this.setState(prevState => ({
            isOpen: !prevState.isOpen
        }))
    }

    render() {

        const isUserLoggedIn = AuthenticationService.isUserLoggedIn();

        const isOpen = this.state.isOpen

        return (
            <Navbar dark className="bg-dark" expand="sm">
                {isUserLoggedIn ?  <NavbarBrand href="/map" className="navbar-brand">Cars Fleet</NavbarBrand> :
                    <NavbarBrand href="/" className="navbar-brand">Cars Fleet</NavbarBrand>}
                {isUserLoggedIn && <NavbarToggler onClick={this.handleToggle}/>}
                <Collapse isOpen={isOpen} navbar>
                    <Nav className="navbar-nav" navbar>
                        <NavItem>
                            {isUserLoggedIn && <Link to="/map" className="nav-item nav-link">Map</Link>}
                        </NavItem>
                        <NavItem>
                            {isUserLoggedIn && <Link to="/cars?page=1" className="nav-item nav-link">Cars</Link>}
                        </NavItem>
                        <NavItem>
                            {isUserLoggedIn && <Link to="/drivers?page=1" className="nav-item nav-link">Drivers</Link>}
                        </NavItem>
                        <NavItem>
                            {isUserLoggedIn && <Link to="/raports" className="nav-item nav-link">Reports</Link>}
                        </NavItem>
                        <NavItem>
                            {isUserLoggedIn && <Link to="/users" className="nav-item nav-link">Users</Link>}
                        </NavItem>
                    </Nav>
                    <Nav className="ml-auto">
                        <NavItem className="navbar-nav">
                            {isUserLoggedIn && <Link className="nav-item nav-link" to="/logout" onClick={AuthenticationService.logout}>Logout</Link>}
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        )

    }

}

export default withRouter(MenuComponent);