import React from 'react';
import {Link} from "react-router-dom";
import qs from 'query-string';
import axios from 'axios'
import config from '../config'

class Drivers extends React.Component {

    constructor(props) {
        super(props)
        let filteringText = qs.parse(this.props.location.search, {ignoreQueryPrefix: true}).filter
        filteringText = filteringText === undefined ? "" : filteringText
        let pageNumber = qs.parse(this.props.location.search, {ignoreQueryPrefix: true}).page
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        this.state = {
            resultPage: {drivers: []},
            filteringText: filteringText,
            pageNumber: pageNumber
        }
        this.reloadPage = this.reloadPage.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this)
        this.onInputSearchEnter = this.onInputSearchEnter.bind(this)
        this.onFilteringChange = this.onFilteringChange.bind(this)
        this.filterDrivers = this.filterDrivers.bind(this)
    }

    componentDidMount() {
        const pageNumber = this.state.pageNumber
        const filteringText = this.state.filteringText
        this.reloadPage(pageNumber, filteringText)
    }

    reloadPage(pageNumber, filteringText) {
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        axios.get(`${config.apiGateway.URL}/api/drivers?page-number=` + pageNumber + "&filter=" + filteringText)
            .then(response => {
                const resultPage = response.data
                this.setState({resultPage: resultPage})})
    }

    handlePageChange(pageNumber) {
        this.reloadPage(pageNumber, this.state.filteringText)
    }

    handleDelete(driverId) {
        fetch(`${config.apiGateway.URL}/api/drivers/` + driverId, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
               this.reloadPage(this.state.pageNumber, this.state.filteringText)
            });
    }

    onInputSearchEnter(event) {
        if (event.keyCode === 13) {
            this.setState({pageNumber: 1})
            this.filterDrivers()
        }
    }

    filterDrivers() {
        const pageNumber = 1;
        this.setState({pageNumber: pageNumber})
        const filteringText = this.state.filteringText
        this.reloadPage(pageNumber, filteringText)
    }

    onFilteringChange(event) {
        this.setState({filteringText: event.target.value})
    }


    render() {
        const drivers = this.state.resultPage.drivers
        let pages = []
        for (let i = 1; i <= this.state.resultPage.numberOfPages; i++) {
            pages.push(i)
        }
        return (
            <div>
                <div className="container search-margin">
                    <div className="row">
                        <div className="col-4">
                            <Link to="drivers/new" className="btn btn-link">Add New Driver</Link>
                        </div>
                        <div className="col-8">
                            <div className="input-group">
                                <input type="text" onKeyDown={this.onInputSearchEnter} value={this.state.filteringText}
                                       className="form-control" placeholder="Search"
                                       onChange={this.onFilteringChange}/>
                                <div className="input-group-append">
                                    <button className="btn btn-secondary" type="button" onClick={this.filterDrivers}>
                                        <i className="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row border table-header-bg">
                        <div className="col-4 col-sm-2">First Name</div>
                        <div className="col-3 col-sm-2">Last Name</div>
                        <div className="col-2 col-sm-2">Age</div>
                        <div className="col-3 col-sm-2 status-margin-bolt">Status</div>
                        <div className="col-sm-4 d-none d-sm-block">Options</div>
                    </div>
                    {drivers.map(driver =>
                        <div key={driver.id} className="row border-left border-right border-bottom">
                            <div className="col-4 col-sm-2">{driver.firstName}</div>
                            <div className="col-3 col-sm-2">{driver.lastName}</div>
                            <div className="col-2 col-sm-2">{driver.age}</div>
                            <div className="col-3 col-sm-2 status-margin">{driver.driverStatus}</div>
                            <div className="col-12 col-sm-4">
                                <Link to={`drivers/${driver.id}`} className="btn btn-link">Edit</Link>
                                <input className="btn btn-link" type="button" value="Delete"
                                       onClick={() => this.handleDelete(driver.id)}/>
                            </div>
                        </div>
                    )}
                    <div className="row mt-md-1">
                        <div className="col-12 d-flex justify-content-center">
                            <ul className="pagination pagination-sm">
                                {pages.map(page => {
                                    return <li key={page} className="page-item"><Link
                                        onClick={() => this.handlePageChange(page)}
                                        to={`drivers?page=${page}&filter=${this.state.filteringText}`}
                                        className="page-link" href="#">{page}</Link></li>
                                })}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Drivers;