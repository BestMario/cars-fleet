import React from 'react'
import config from '../config'

class NewDriver extends React.Component {

    constructor() {
        super()
        this.state = {firstName: '', lastName: '', age: '', validational: {}};
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
        this.handleAge = this.handleAge.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        const firstName = this.state.firstName;
        const lastName = this.state.lastName;
        const age = this.state.age;
        const addNewDriverCommand = {firstName: firstName, lastName: lastName, age: age}

        fetch(`${config.apiGateway.URL}/api/drivers`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(addNewDriverCommand)
        })
            .then(this.parseFetchResponse)
            .then(response => {
                const json = response.json
                const meta = response.meta

                if (meta.ok) {
                    this.props.history.push('/drivers')
                } else {
                    this.setState({validational: json})
                }
            })
    }

    parseFetchResponse(response) {
        if (response.ok) {
            return {json: {}, meta: response}
        }
        return response.json().then(function (json) {
            return {json: json, meta: response}
        })
    }

    handleFirstNameChange(event) {
        this.setState({firstName: event.target.value})
    }

    handleLastNameChange(event) {
        this.setState({lastName: event.target.value})
    }

    handleAge(event) {
        this.setState({age: event.target.value})
    }


    render() {
        const validation = this.state.validational

        return (
            <div>
                <form onSubmit={this.handleSubmit} className="container">
                    <div className="row">
                        <div className="form-group col-12 col-md-5 text-left">
                            <label htmlFor="first-name">First Name</label>
                            <input type="text" className="form-control col-12" id="first-name"
                                   placeholder="Enter First Name" value={this.state.firstName}
                                   onChange={this.handleFirstNameChange}/>
                            <span className="text-danger">
                             {validation.firstNameErrors}
                         </span>
                        </div>
                        <div className="form-group col-12 col-md-4 text-left">
                            <label htmlFor="last-name">Last Name</label>
                            <input type="text" className="form-control col-12" id="last-name"
                                   placeholder="Enter Last Name" value={this.state.lastName}
                                   onChange={this.handleLastNameChange}/>
                            <span className="text-danger">
                                   {validation.lastNameErrors}
                               </span>
                        </div>
                        <div className="form-group col-12 col-md-3 text-left">
                            <label htmlFor="age">Age</label>
                            <input type="text" className="form-control col-12" id="age"
                                   placeholder="Enter Age" value={this.state.age}
                                   onChange={this.handleAge}/>
                            <span className="text-danger">
                                   {validation.ageErrors}
                               </span>
                        </div>
                    </div>
                    <button type="submit" value="Save" className="btn btn-primary">Save</button>
                </form>
            </div>
        )
    }

}

export default NewDriver;