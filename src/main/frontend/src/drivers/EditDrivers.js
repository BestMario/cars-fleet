import React from 'react'
import axios from 'axios'
import config from '../config'

class EditDrivers extends React.Component {

    constructor(props) {
        super(props)

        this.state = {firstName: '', lastName: '', age: '', driverStatus: 'ACTIVE', validational: {}};
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
        this.handleAge = this.handleAge.bind(this);
        this.handleStatusChange = this.handleStatusChange.bind(this);
    }

    componentDidMount() {
        const driverId = this.props.match.params.id
        axios.get(`${config.apiGateway.URL}/api/drivers/` + driverId)
            .then(response => {
                const driver = response.data
                this.setState({
                    firstName: driver.firstName, lastName: driver.lastName,
                    age: driver.age, driverStatus: driver.driverStatus
                })
            })
    }

    handleSubmit(event) {
        event.preventDefault();
        const firstName = this.state.firstName;
        const lastName = this.state.lastName;
        const age = this.state.age;
        const driverStatus = this.state.driverStatus

        const addNewDriverCommand = {firstName: firstName, lastName: lastName, age: age, driverStatus: driverStatus}

        const driverId = this.props.match.params.id

        axios.patch(`${config.apiGateway.URL}/api/drivers/` + driverId,
            addNewDriverCommand
        )
        .then(response => this.props.history.push('/drivers'))
        .catch(err => {
            if (err.response && err.response.data) {
                const validation = err.response.data
                this.setState({validational: validation})
            }
            // else {
            //     this.setState({generalErrorMessage: "Something went wrong while executing request"})
            // }
        })


        // fetch("http://localhost:8080/api/drivers/" + driverId, {
        //     method: 'PATCH',
        //     headers: {
        //         'Accept': 'application/json',
        //         'Content-Type': 'application/json'
        //     },
        //     body: JSON.stringify(addNewDriverCommand)
        // })
        //     .then(this.parseFetchResponse)
        //     .then(response => {
        //         const json = response.json
        //         const validational = response.validational
        //
        //         if (validational.ok) {
        //             this.props.history.push('/drivers')
        //         } else {
        //             this.setState({validational: json})
        //         }
        //     })
    }

    parseFetchResponse(response) {
        if (response.ok) {
            return {json: {}, validational: response}
        }
        return response.json().then(function (json) {
            return {json: json, validational: response}
        })
    }

    handleFirstNameChange(event) {
        this.setState({firstName: event.target.value})
    }

    handleLastNameChange(event) {
        this.setState({lastName: event.target.value})
    }

    handleAge(event) {
        this.setState({age: event.target.value})
    }

    handleStatusChange(event) {
        this.setState({driverStatus: event.target.value})
    }

    render() {
        const validational = this.state.validational

        return (
            <div className="container">
                <form onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="form-group col-12 col-md-6">
                            <label htmlFor="first-name">First Name</label>
                            <input type="text" className="form-control" id="first-name"
                                   placeholder="Enter First Name" value={this.state.firstName}
                                   onChange={this.handleFirstNameChange}/>
                            <span className="text-danger">
                                   {validational.firstNameErrors}
                               </span>
                        </div>
                        <div className="form-group col-12 col-md-6">
                            <label htmlFor="last-name">Last Name</label>
                            <input type="text" className="form-control" id="last-name"
                                   placeholder="Enter Last Name" value={this.state.lastName}
                                   onChange={this.handleLastNameChange}/>
                            <span className="text-danger">
                                   {validational.lastNameErrors}
                               </span>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-12 col-md-6">
                            <label htmlFor="age">Age</label>
                            <input type="text" className="form-control" id="age"
                                   placeholder="Enter Age" value={this.state.age}
                                   onChange={this.handleAge}/>
                            <span className="text-danger">
                                   {validational.ageErrors}
                               </span>
                        </div>
                        <div className="col-12 col-md-6">
                            <label htmlFor="driver-status">Select Driver Status</label>
                            <select className="form-control" value={this.state.driverStatus}
                                    onChange={this.handleStatusChange}>
                                <option value="ACTIVE">Active</option>
                                <option value="INACTIVE">Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 car-margin">
                            <button type="submit" value="Save" className="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }

}

export default EditDrivers;