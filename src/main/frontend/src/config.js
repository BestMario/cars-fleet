const dev = {
    apiGateway: {
        URL: "http://localhost:8080"
    }
};

const gcp = {
    apiGateway: {
        URL: "https://car-fleet-management.appspot.com"
    }
};

const config = process.env.REACT_APP_STAGE === 'gcp' ? gcp : dev;

export default {
    ...config
};