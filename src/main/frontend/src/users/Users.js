import React from 'react'
import {Link} from "react-router-dom";
import qs from 'query-string'
import axios from 'axios'
import config from '../config'

class Users extends React.Component {

    constructor(props) {
        super(props)
        let pageNumber = qs.parse(this.props.location.search, {ignoreQueryPrefix: true}).page
        let filteringText = qs.parse(this.props.location.search, {ignoreQueryPrefix: true}).filter
        pageNumber = pageNumber === undefined ? 1 : pageNumber
        filteringText = filteringText === undefined ? "" : filteringText
        this.state = {
            resultPage: {users: []},
            pageNumber: pageNumber,
            filteringText: filteringText,
            generalErrorMessage: '',
            validation: {}
        }
        this.reloadPage = this.reloadPage.bind(this)
        this.handlePageChange = this.handlePageChange.bind(this)
        this.onInputSearchEnter = this.onInputSearchEnter.bind(this)
        this.filterUsers = this.filterUsers.bind(this)
        this.onFilteringChange = this.onFilteringChange.bind(this)
        this.handleUserStatus = this.handleUserStatus.bind(this)
        this.handleDelete = this.handleDelete.bind(this)
    }

    componentDidMount() {
        const pageNumber = this.state.pageNumber
        const filteringText = this.state.filteringText
        this.reloadPage(pageNumber, filteringText)
    }

    reloadPage(pageNumber, filteringText) {
        pageNumber = pageNumber === undefined ? 1 : pageNumber
        axios.get(`${config.apiGateway.URL}/api/users?page-number=` + pageNumber + "&filter=" + filteringText)
            .then(resultPage => {
                this.setState({resultPage: resultPage.data})
            })
    }

    handlePageChange(pageNumber) {
        this.setState({pageNumber: pageNumber})
        this.reloadPage(pageNumber, this.state.filteringText)
    }

    handleDelete(userId) {
        axios.delete(`${config.apiGateway.URL}/api/users/` + userId)
            .then(response => {
                this.handlePageChange(this.state.pageNumber)
                //this.forceUpdate()
                //this.props.history.push('/users')
            })
            .catch(err => {
                if (err.response && err.response.data) {
                    const validation = err.response.data
                    this.setState({generalErrorMessage: validation.userIdErrors})
                } else {
                    this.setState({generalErrorMessage: "Something went wrong while deleting user"})
                }
            });
    }

    onInputSearchEnter(event) {
        if (event.keyCode === 13) {
            this.setState({pageNumber: 1})
            this.filterUsers()
        }
    }

    filterUsers() {
        const pageNumber = 1;
        this.setState({pageNumber: pageNumber})
        const filteringText = this.state.filteringText
        this.reloadPage(pageNumber, filteringText)
    }

    onFilteringChange(event) {
        this.setState({filteringText: event.target.value})
    }

    handleUserStatus(userId) {
        axios.patch(`${config.apiGateway.URL}/api/users/` + userId + "/status")
            .then(response => {
                this.handlePageChange(this.state.pageNumber)
                //this.forceUpdate()
                // this.props.history.push('/users')
            })
            .catch(err => {
                if (err.response && err.response.data) {
                    const validation = err.response.data
                    this.setState({validation: validation})
                } else {
                    this.setState({generalErrorMessage: "Something went wrong while executing request"})
                }
            })
    }

    render() {
        const users = this.state.resultPage.users
        let pages = []
        for (let i = 1; i <= this.state.resultPage.numberOfPages; i++) {
            pages.push(i)
        }
        return (

            <div>
                <div className="container search-margin">
                    <div className="row">
                        <div className="col-4">
                            <Link to="users/new" className="btn btn-link">Add New User</Link>
                        </div>
                        <div className="col-8">
                            <div className="input-group">
                                <input type="text" onKeyDown={this.onInputSearchEnter} value={this.state.filteringText}
                                       className="form-control" placeholder="Search"
                                       onChange={this.onFilteringChange}/>
                                <div className="input-group-append">
                                    <button className="btn btn-secondary" type="button" onClick={this.filterUsers}>
                                        <i className="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row border table-header-bg">
                        <div className="col-3 col-md-2">First Name</div>
                        <div className="col-3 col-md-2">Last Name</div>
                        <div className="col-6 col-md-3">Email Address</div>
                        <div className="col-md-2 col-lg-1 d-none d-md-block">Role</div>
                        <div className="col-12 col-md-3 col-lg-4 d-none d-md-block">Options</div>
                    </div>
                    {users.map(user =>
                        <div key={user.id}
                             className={user.userStatus !== "BLOCKED" ? ("row border-left border-right border-bottom") : ("row border-left border-right border-bottom text-danger")}>
                            <div className="col-3 col-md-2">{user.firstName}</div>
                            <div className="col-3 col-md-2">{user.lastName}</div>
                            <div className="col-6 col-md-3 text-break">{user.emailAddress}</div>
                            <div className="col-md-2 col-lg-1 d-none d-md-block">{user.role}</div>
                            <div className="col-12 col-md-3 col-lg-4">
                                <Link to={`users/${user.id}`}
                                      className={user.firstName === "demo" ? ("btn btn-link-disabled") : ("btn btn-link")}>Edit</Link>
                                <input
                                    className={user.firstName === "demo" ? ("btn btn-link-disabled") : ("btn btn-link")}
                                    type="button" value="Delete"
                                    onClick={() => this.handleDelete(user.id)}/>
                                {user.userStatus !== "BLOCKED" ? (
                                    <input
                                        className={user.firstName === "demo" ? ("btn btn-link-disabled") : ("btn btn-link")}
                                        type="button" value="BLOCK"
                                        onClick={() => this.handleUserStatus(user.id)}/>
                                ) : (
                                    <input className="btn btn-link" type="button" value="UNBLOCK"
                                           onClick={() => this.handleUserStatus(user.id)}/>
                                )}
                            </div>
                        </div>
                    )}
                    <div>
                        <div className="global-error-margin">
                    <span className="text-danger">
                                   {this.state.generalErrorMessage}{this.state.validation.userIdErrors}
                               </span>
                        </div>
                    </div>
                    <div className="row mt-md-1">
                        <div className="col-12 d-flex justify-content-center">
                            <ul className="pagination pagination-sm">
                                {pages.map(page => {
                                    return <li key={page} className="page-item"><Link
                                        onClick={() => this.handlePageChange(page)}
                                        to={`users?page=${page}&filter=${this.state.filteringText}`}
                                        className="page-link" href="#">{page}</Link></li>
                                })}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }


}

export default Users;