import React from 'react'
import axios from 'axios'
import config from '../config'

class EditUser extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            firstName: '',
            lastName: '',
            emailAddress: '',
            password: '',
            role: 'SIMPLE_USER',
            userStatus: 'UNBLOCKED',
            validation: {},
            generalErrorMessage: ''
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleFirstNameChange = this.handleFirstNameChange.bind(this)
        this.handleLastNameChange = this.handleLastNameChange.bind(this)
        this.handleEmailAddressChange = this.handleEmailAddressChange.bind(this)
        this.handlePasswordChange = this.handlePasswordChange.bind(this)
        this.handleUserRoleChange = this.handleUserRoleChange.bind(this)
        this.handleUserStatusChange = this.handleUserStatusChange.bind(this)
    }

    componentDidMount() {
        const userId = this.props.match.params.id
        axios.get(`${config.apiGateway.URL}/api/users/` + userId)
            .then(response => {
                const user = response.data
                this.setState({
                    firstName: user.firstName, lastName: user.lastName,
                    emailAddress: user.emailAddress, role: user.role, userStatus: user.userStatus
                })
            })
    }

    handleSubmit(event) {
        event.preventDefault();
        const firstName = this.state.firstName;
        const lastName = this.state.lastName;
        const emailAddress = this.state.emailAddress;
        const password = this.state.password;
        const role = this.state.role;
        const userStatus = this.state.userStatus;
        const updateUserCommand = {
            firstName: firstName,
            lastName: lastName,
            emailAddress: emailAddress,
            password: password,
            role: role,
            userStatus: userStatus
        }

        const userId = this.props.match.params.id

        axios.patch(`${config.apiGateway.URL}/api/users/` + userId,
            updateUserCommand
        )
            .then(response => {
                this.props.history.push('/users')
            }).catch(err => {
            if (err.response && err.response.data) {
                const validation = err.response.data
                this.setState({validation: validation})
            } else {
                this.setState({generalErrorMessage: "Something went wrong while executing request"})
            }
        })
    }


    handleFirstNameChange(event) {
        this.setState({firstName: event.target.value})
    }

    handleLastNameChange(event) {
        this.setState({lastName: event.target.value})
    }

    handleEmailAddressChange(event) {
        this.setState({emailAddress: event.target.value})
    }

    handlePasswordChange(event) {
        this.setState({password: event.target.value})
    }

    handleUserRoleChange(event) {
        this.setState({role: event.target.value})
    }

    handleUserStatusChange(event) {
        this.setState({userStatus: event.target.value})
    }

    render() {

        const validation = this.state.validation

        return (
            <div className="container">
                <form onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="form-group col-12 col-md-6">
                            <label htmlFor="first-name">First Name</label>
                            <input type="text" className="form-control" id="first-name"
                                   placeholder="Enter First Name" value={this.state.firstName}
                                   onChange={this.handleFirstNameChange}/>
                            <span className="text-danger">{validation.firstNameErrors}</span>
                        </div>
                        <div className="form-group col-12 col-md-6">
                            <label htmlFor="last-name">Last Name</label>
                            <input type="text" className="form-control" id="last-name"
                                   placeholder="Enter Last Name" value={this.state.lastName}
                                   onChange={this.handleLastNameChange}/>
                            <span className="text-danger">{validation.lastNameErrors}</span>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-12 col-md-6">
                            <label htmlFor="email-address">Email Address</label>
                            <input type="text" className="form-control" id="email-address"
                                   placeholder="Enter Email Address" value={this.state.emailAddress}
                                   onChange={this.handleEmailAddressChange}/>
                            <span className="text-danger">{validation.emailAddressErrors}</span>
                        </div>
                        <div className="form-group col-12 col-md-6">
                            <label htmlFor="password">Password</label>
                            <input type="password" className="form-control" id="password"
                                   placeholder="Enter New Password" value={this.state.password}
                                   onChange={this.handlePasswordChange}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 col-md-6">
                            <label htmlFor="user-role">Select User Role</label>
                            <select className="form-control" value={this.state.role}
                                    onChange={this.handleUserRoleChange}>
                                <option value="SIMPLE_USER">Simple User</option>
                                <option value="ADMIN">Admin</option>
                            </select>
                        </div>
                        <div className="col-12 col-md-6">
                            <label htmlFor="user-status">Select User Status</label>
                            <select className="form-control" value={this.state.userStatus}
                                    onChange={this.handleUserStatusChange}>
                                <option value="UNBLOCKED">UNBLOCKED</option>
                                <option value="BLOCKED">BLOCKED</option>
                            </select>
                        </div>
                        <div>
                            <div className="global-error-margin">
                    <span className="text-danger">
                                   {this.state.generalErrorMessage}{validation.demoUpdateErrors}
                               </span>
                            </div>
                        </div>
                    </div>
                    <button type="submit" value="Save" className="btn btn-primary">Save</button>
                </form>
            </div>
        );
    }

}

export default EditUser;