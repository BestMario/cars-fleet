import React from 'react'
import axios from 'axios'
import config from '../config'

class NewUser extends React.Component {

    constructor() {
        super()
        this.state = {firstName: '', lastName: '', emailAddress: '', password: '', validation: {}, generalErrorMessage: ''}

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleFirstNameChange = this.handleFirstNameChange.bind(this)
        this.handleLastNameChange = this.handleLastNameChange.bind(this)
        this.handleEmailAddressChange = this.handleEmailAddressChange.bind(this)
        this.handlePasswordChange = this.handlePasswordChange.bind(this)
    }

    handleSubmit(event) {
        event.preventDefault();
        const firstName = this.state.firstName;
        const lastName = this.state.lastName;
        const emailAddress = this.state.emailAddress;
        const password = this.state.password;
        const addNewUserCommand = {
            firstName: firstName,
            lastName: lastName,
            emailAddress: emailAddress,
            password: password,
            generalErrorMessage: ''
        }

        axios.post(`${config.apiGateway.URL}/api/users`,
            addNewUserCommand
        )
            .then(response => {
                this.props.history.push('/users')
            })
            .catch(err => {
                if (err.response && err.response.data) {
                    const validation = err.response.data
                    this.setState({validation: validation})
                } else {
                    this.setState({generalErrorMessage: "Something went wrong while executing request"})
                }
            })
    }

    handleFirstNameChange(event) {
        this.setState({firstName: event.target.value})
    }

    handleLastNameChange(event) {
        this.setState({lastName: event.target.value})
    }

    handleEmailAddressChange(event) {
        this.setState({emailAddress: event.target.value})
    }

    handlePasswordChange(event) {
        this.setState({password: event.target.value})
    }

    render() {

        const validation = this.state.validation

        return (
            <div>
                <form onSubmit={this.handleSubmit} className="container">
                    <div className="row">
                    <div className="form-group col-12 col-md-6 text-left">
                        <label htmlFor="first-name">First Name</label>
                        <input type="text" className="form-control col-12" id="firstName"
                               placeholder="Enter First Name" value={this.state.firstName} onChange={this.handleFirstNameChange}/>
                               <span className="text-danger">{validation.firstNameError}</span>
                    </div>
                    <div className="form-group col-12 col-md-6 text-left">
                        <label htmlFor="last-name">Last Name</label>
                        <input type="text" className="form-control col-12" id="lastName"
                               placeholder="Enter Last Name" value={this.state.lastName} onChange={this.handleLastNameChange}/>
                               <span className="text-danger">{validation.lastNameError}</span>
                    </div>
                    </div>
                    <div className="row">
                    <div className="form-group col-12 col-md-6 text-left">
                        <label htmlFor="email-address">Email Address</label>
                        <input type="text" className="form-control col-12" id="emailAddress"
                               placeholder="Enter Email Address" value={this.state.emailAddress} onChange={this.handleEmailAddressChange}/>
                               <span className="text-danger">{validation.emailAddressError}</span>
                    </div>
                    <div className="form-group col-12 col-md-6 text-left">
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control col-12" id="password"
                               placeholder="Enter password" value={this.state.password} onChange={this.handlePasswordChange}/>
                               <span className="text-danger">{validation.passwordError}</span>
                    </div>
                    </div>
                    <div>
                        <div className="global-error-margin">
                    <span className="text-danger">
                                   {this.state.generalErrorMessage}
                               </span>
                        </div>
                    </div>
                    <button type="submit" value="Save" className="btn btn-primary">Save</button>
                </form>
            </div>
        )
    }
}

export default NewUser;