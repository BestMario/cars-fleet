import React from 'react'
import DatePicker from "react-datepicker";
import axios from 'axios'
import config from '../config'

class EditFuel extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            refuelingDate: new Date(),
            amountInLiters: '',
            fuelType: '',
            priceInPln: '',
            mileageInKm: '',
            generalErrorMassage: ''
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleLitersChange = this.handleLitersChange.bind(this);
        this.handleFuelTypeChange = this.handleFuelTypeChange.bind(this);
        this.handlePriceChange = this.handlePriceChange.bind(this);
        this.handleMileageChange = this.handleMileageChange.bind(this);
    }

    componentDidMount() {
        const fuelId = this.props.match.params.fuelId;
        axios.get(`${config.apiGateway.URL}/api/cars/fuel/` + fuelId)
            .then(response => {
                const fuel = response.data
                this.setState({
                    refuelingDate: new Date(), amountInLiters: fuel.amountInLiters,
                    fuelType: fuel.fuelType, priceInPln: fuel.priceInPln, mileageInKm: fuel.mileageInKm
                })
            })
    }

    handleSubmit(event) {
        event.preventDefault();

        const refuelingDate = this.state.refuelingDate;
        const amountInLiters = this.state.amountInLiters;
        const fuelType = this.state.fuelType;
        const priceInPln = this.state.priceInPln;
        const mileageInKm = this.state.mileageInKm;
        const updateFuelCommand = {
            refuelingDate: refuelingDate,
            amountInLiters: amountInLiters,
            fuelType: fuelType,
            priceInPln: priceInPln,
            mileageInKm: mileageInKm
        }

        const carId = this.props.match.params.carId;
        const fuelId = this.props.match.params.fuelId;
        axios.patch(`${config.apiGateway.URL}/api/cars/` + carId + "/fuel/" + fuelId,
            updateFuelCommand
        )
            .then(response => this.props.history.push('/cars/' + carId + "/fuel"))
            .catch(err => this.setState({generalErrorMassage: "Something went wrong while executing your request"}))
    }

    handleDateChange = date => {
        this.setState({refuelingDate: date});
    };

    handleLitersChange(event) {
        this.setState({amountInLiters: event.target.value})
    }

    handleFuelTypeChange(event) {
        this.setState({fuelType: event.target.value})
    }

    handlePriceChange(event) {
        this.setState({priceInPln: event.target.value})
    }

    handleMileageChange(event) {
        this.setState({mileageInKm: event.target.value})
    }


    render() {
        return (
            <div className="container">
                <form onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="form-group col-12 col-md-4">
                            <label>Select Date</label>
                            <div className="customDatePickerWidth">
                                <DatePicker className="form-control" dateFormat="yyyy-MM-dd" selected={this.state.refuelingDate}
                                            onChange={this.handleDateChange}/>
                            </div>
                        </div>
                        <div className="form-group col-12 col-md-4">
                            <label htmlFor="amount-in-liters">Amount in liters</label>
                            <input type="text" className="form-control" id="amount-in-liters"
                                   placeholder="Enter amount" value={this.state.amountInLiters}
                                   onChange={this.handleLitersChange}/>
                        </div>
                        <div className="col-12 col-md-4">
                            <div className="form-group">
                                <label>Fuel Type</label>
                                <select className="form-control" value={this.state.fuelType}
                                        onChange={this.handleFuelTypeChange}>
                                    <option value="DIESEL">DIESEL</option>
                                    <option value="PETROL">PETROL</option>
                                    <option value="LPG">LPG</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-12 col-md-6">
                            <label htmlFor="price-in-pln">Price in PLN</label>
                            <input type="text" className="form-control" id="price-in-pln"
                                   placeholder="Enter price" value={this.state.priceInPln}
                                   onChange={this.handlePriceChange}/>
                        </div>
                        <div className="form-group col-12 col-md-6">
                            <label htmlFor="mileage-in-km">Mileage in km</label>
                            <input type="text" className="form-control" id="mileage-in-km"
                                   placeholder="Enter mileage" value={this.state.mileageInKm}
                                   onChange={this.handleMileageChange}/>
                        </div>
                    </div>
                    <div>
                        <div className="global-error-margin">
                    <span className="text-danger">
                                   {this.state.generalErrorMessage}
                               </span>
                        </div>
                    </div>
                    <button type="submit" value="Save" className="btn btn-primary">Save</button>
                </form>
            </div>
        )
    }
}


export default EditFuel;