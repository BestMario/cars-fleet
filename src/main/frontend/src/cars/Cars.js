import React from 'react'
import {
    Link
} from "react-router-dom";
import qs from 'query-string';
import axios from "axios";
import config from '../config'

class Cars extends React.Component {

    constructor(props) {
        super(props)
        let filteringText = qs.parse(this.props.location.search, {ignoreQueryPrefix: true}).filter
        let pageNumber = qs.parse(this.props.location.search, {ignoreQueryPrefix: true}).page
        pageNumber = pageNumber === undefined ? 1 : pageNumber
        filteringText = filteringText === undefined ? "" : filteringText
        this.state = {
            resultPage: {cars: []},
            filteringText: filteringText,
            pageNumber: pageNumber,
            generalErrorMessage: ''
        }
        this.handlePageChange = this.handlePageChange.bind(this);
        this.reloadPage = this.reloadPage.bind(this);
        this.filterCars = this.filterCars.bind(this);
        this.onFilteringChangeChange = this.onFilteringChangeChange.bind(this);
        this.onInputSearchEnter = this.onInputSearchEnter.bind(this);
        this.handleDetachDriver = this.handleDetachDriver.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    componentDidMount() {
        const pageNumber = this.state.pageNumber
        const filteringText = this.state.filteringText
        this.reloadPage(pageNumber, filteringText)
    }

    reloadPage(pageNumber, filteringText) {
        pageNumber = pageNumber === undefined ? 1 : pageNumber
        axios.get(`${config.apiGateway.URL}/api/cars?page-number=` + pageNumber + "&filter=" + filteringText
        //     , {
        //     headers: {
        //         'Access-Control-Allow-Origin': '*',
        //     }
        // }
        )
            .then(response => {
                const resultPage = response.data
                this.setState({resultPage: resultPage})
            })
    }

    handlePageChange(pageNumber) {
        this.setState({pageNumber: pageNumber})
        this.reloadPage(pageNumber, this.state.filteringText)
    }

    handleDelete(carId) {
        axios.delete(`${config.apiGateway.URL}/api/cars/` + carId
        //     , {
        //     headers: {
        //         'Access-Control-Allow-Origin': '*',
        //     }
        // }
        )
            .then(response => {
                // this.props.history.push('/cars')
                this.reloadPage(this.state.pageNumber, this.state.filteringText)
            }).catch(err => this.setState({generalErrorMessage: "Something went wrong while deleting car"}));
    }

    filterCars() {
        const pageNumber = 1
        this.setState({pageNumber: pageNumber})
        const filteringText = this.state.filteringText
        this.reloadPage(pageNumber, filteringText)
    }

    onFilteringChangeChange(event) {
        this.setState({filteringText: event.target.value})
    }

    onInputSearchEnter(event) {
        if (event.keyCode === 13) {
            this.setState({pageNumber: 1})
            this.filterCars()
        }
    }

    handleDetachDriver(carId) {
        axios.delete(`${config.apiGateway.URL}/api/cars/` + carId + "/current-driver")
            .then(response => {
                window.location.reload();
            }).catch(err => this.setState({generalErrorMessage: "Something went wrong while detaching driver from car"}));
    }

    render() {
        const cars = this.state.resultPage.cars
        let pages = []
        for (let i = 1; i <= this.state.resultPage.numberOfPages; i++) {
            pages.push(i)
        }

        const self = this;
        return (
            <div>
                <div className="container search-margin">
                    <div className="row">
                        <div className="col-4">
                            <Link to="cars/new" className="btn btn-link">Add New Car</Link>
                        </div>
                        <div className="col-8">
                            <div className="input-group">
                                <input type="text" onKeyDown={this.onInputSearchEnter} value={this.state.filteringText}
                                       className="form-control" placeholder="Search"
                                       onChange={this.onFilteringChangeChange}/>
                                <div className="input-group-append">
                                    <button className="btn btn-secondary" type="button" onClick={this.filterCars}>
                                        <i className="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container">
                    <div className="row border table-header-bg">
                        <div className="col-3 col-md-2">Make</div>
                        <div className="col-2 col-md-2">Model</div>
                        <div className="col-md-2 d-none d-md-block">Year</div>
                        <div className="col-3 col-md-2">Reg. no.</div>
                        <div className="col-4 col-md-2">Driver</div>
                        <div className="col-12 col-md-2 d-none d-md-block">Options</div>
                    </div>
                    {cars.map(car => {
                        return <div key={car.carId} className="row border-left border-right border-bottom">
                            <div className="col-3 col-md-2">{car.make}</div>
                            <div className="col-2 col-md-2">{car.model}</div>
                            <div className="col-md-2 d-none d-md-block">{car.manufacturedYear}</div>
                            <div className="col-3 col-md-2">{car.numberPlate}</div>
                            <div className="col-4 col-md-2 float-md-right">{car.currentDriver.firstName} {car.currentDriver.lastName}</div>
                            <div className="col-12 col-md-2">
                                <Link to={`cars/${car.carId}`} className="btn btn-link">Edit</Link>
                                <input className="btn btn-link" type="button" value="Delete"
                                       onClick={() => self.handleDelete(car.carId)}/>
                                {!car.driverId ? (
                                    <Link to={`cars/${car.carId}/driver`} className="btn btn-link">Assign</Link>
                                ) : (
                                    < input className="btn btn-link" type="button" value="Detach"
                                            onClick={() => this.handleDetachDriver(car.carId)}/>
                                )}
                                <Link to={`cars/${car.carId}/fuel`} className="btn btn-link">Fuel</Link>
                            </div>
                        </div>
                    })}
                    <div className="row mt-md-1">
                        <div className="col-12 d-flex justify-content-center">
                            <ul className="pagination pagination-sm">
                                {pages.map(page => {
                                    return <li key={page} className="page-item"><Link
                                        onClick={() => this.handlePageChange(page)}
                                        to={`cars?page=${page}&filter=${this.state.filteringText}`}
                                        className="page-link" href="#">{page}</Link></li>
                                })}
                            </ul>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 d-flex justify-content-center">
                            <div className="global-error-margin">
                    <span className="text-danger">
                                   {this.state.generalErrorMessage}
                               </span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }

}


export default Cars;