import React from 'react'
import axios from 'axios'
import config from '../config'

class NewCar extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            make: '',
            model: '',
            manufacturedYear: '',
            numberPlate: '',
            validation: {},
            generalErrorMessage: ''
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleMakeChange = this.handleMakeChange.bind(this);
        this.handleModelChange = this.handleModelChange.bind(this);
        this.handleManufacturedYearChange = this.handleManufacturedYearChange.bind(this);
        this.handleNumberPlateChange = this.handleNumberPlateChange.bind(this);
    }

    handleModelChange(event) {
        this.setState({model: event.target.value})
    }

    handleMakeChange(event) {
        this.setState({make: event.target.value})
    }

    handleManufacturedYearChange(event) {
        this.setState({manufacturedYear: event.target.value})
    }

    handleNumberPlateChange(event) {
        this.setState({numberPlate: event.target.value})
    }

    handleSubmit(event) {
        event.preventDefault();

        const make = this.state.make;
        const model = this.state.model;
        const manufacturedYear = this.state.manufacturedYear;
        const numberPlate = this.state.numberPlate;
        const addNewCarCommand = {
            make: make,
            model: model,
            manufacturedYear: manufacturedYear,
            numberPlate: numberPlate
        }

        axios.post(`${config.apiGateway.URL}/api/cars`,
            addNewCarCommand
        ).then(response => {
            this.props.history.push(`/cars`)
        }).catch(err => {
            if (err.response && err.response.data) {
                const validation = err.response.data
                this.setState({validation: validation})
            } else {
                this.setState({generalErrorMessage: "Something went wrong while executing your request"})
            }
        })
    }

    render() {
        const validation = this.state.validation

        return (
            <div>
                <form onSubmit={this.handleSubmit} className="container">
                    <div className="row">
                        <div className="form-group col-12 col-md-6 text-left">
                                <label htmlFor="make" className="col-12">Make</label>
                                <input type="text" className="form-control col-12" id="make"
                                       value={this.state.make} onChange={this.handleMakeChange}/>
                            <span className="text-danger">
                                   {validation.makeErrors}
                               </span>
                        </div>
                        <div className="form-group col-12 col-md-6 text-left">
                                <label htmlFor="model" className="col-12">Model</label>
                                <input type="text" className="form-control col-12" id="model"
                                       value={this.state.moŁdel} onChange={this.handleModelChange}/>
                            <span className="text-danger">
                                   {validation.modelErrors}
                               </span>

                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-12 col-md-6 text-left">

                            <label htmlFor="manufactured-year" className="col-12 text-left">Manufactured year</label>
                            <input type="text" className="form-control col-12" id="manufactured-year"
                                   value={this.state.manufacturedYear}
                                   onChange={this.handleManufacturedYearChange}/>
                            <span className="text-danger">
                                   {validation.manufacturedYearErrors}
                               </span>
                        </div>
                        <div className="form-group col-12 col-md-6 text-left">
                            <label htmlFor="number-plate" className="col-12 text-left">Number plate</label>
                            <input type="text" className="form-control col-12" id="number-plate"
                                   value={this.state.numberPlate}
                                   onChange={this.handleNumberPlateChange}/>
                            <span className="text-danger">
                                   {validation.numberPlateErrors}
                               </span>
                        </div>
                    </div>
                    <div className="global-error-margin">
                    <span className="text-danger">
                                   {this.state.generalErrorMessage}
                               </span>
                    </div>
                    <div>
                        <button type="submit" value="Save" className="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default NewCar;