import React from 'react'
import DatePicker from 'react-datepicker';
import axios from 'axios'
import "react-datepicker/dist/react-datepicker.css";
import config from '../config'

class NewFuel extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            refuelingDate: new Date(),
            amountInLiters: '',
            fuelType: 'DIESEL',
            priceInPln: '',
            mileageInKm: '',
            generalErrorMessage: '',
            validation: {}
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleLitersChange = this.handleLitersChange.bind(this);
        this.handleFuelTypeChange = this.handleFuelTypeChange.bind(this);
        this.handlePriceChange = this.handlePriceChange.bind(this);
        this.handleMileageChange = this.handleMileageChange.bind(this);
    }

    handleDateChange = date => {
        this.setState({refuelingDate: date});
    };

    handleLitersChange(event) {
        this.setState({amountInLiters: event.target.value})
    }

    handleFuelTypeChange(event) {
        this.setState({fuelType: event.target.value})
    }

    handlePriceChange(event) {
        this.setState({priceInPln: event.target.value})
    }

    handleMileageChange(event) {
        this.setState({mileageInKm: event.target.value})
    }


    handleSubmit(event) {
        event.preventDefault();
        const carId = this.props.match.params.carId;

        const refuelingDate = this.state.refuelingDate;
        const amountInLiters = this.state.amountInLiters;
        const fuelType = this.state.fuelType;
        const priceInPln = this.state.priceInPln;
        const mileageInKm = this.state.mileageInKm;
        const addFuelConsumptionCommand = {
            refuelingDate: refuelingDate,
            amountInLiters: amountInLiters,
            fuelType: fuelType,
            priceInPln: priceInPln,
            mileageInKm: mileageInKm
        }

        axios.post(`${config.apiGateway.URL}/api/cars/` + carId + "/new-refueling",
            addFuelConsumptionCommand
        )
            .then(response => this.props.history.push('/cars/' + carId + "/fuel"))
            .catch(err => {
                if (err.response && err.response.data) {
                    const validation = err.response.data
                    this.setState({validation: validation})
                } else {
                    this.setState({generalErrorMessage: "Something went wrong while executing your request"})
                }
            })
    }


    render() {
        const validation = this.state.validation
        return (
            <div className="container">
                <form onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="form-group col-12 col-md-4">
                            <label>Select date:</label>
                            <div className="customDatePickerWidth">
                                <DatePicker className="form-control" dateFormat="yyyy-MM-dd"
                                            selected={this.state.refuelingDate}
                                            onChange={this.handleDateChange}/>
                                <div>
                                </div>
                                <span className="text-danger">
                                   {validation.refuelingDateErrors}
                               </span>
                            </div>
                        </div>
                        <div className="col-12 col-md-4 form-group">
                            <label htmlFor="amount-in-liters">Amount in liters</label>
                            <input type="text" className="form-control" id="amount-in-liters"
                                   placeholder="Enter amount" value={this.state.amountInLiters}
                                   onChange={this.handleLitersChange}/>
                            <span className="text-danger">
                                   {validation.amountInLitersErrors}
                               </span>
                        </div>
                        <div className="col-12 col-md-4 form-group">
                            <label htmlFor="fuel-type">Fuel Type</label>
                            <select className="form-control" value={this.state.fuelType}
                                    onChange={this.handleFuelTypeChange}>
                                <option value="DIESEL">DIESEL</option>
                                <option value="PETROL">PETROL</option>
                                <option value="LPG">LPG</option>
                            </select>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-12 col-md-6">
                            <label htmlFor="price-in-pln">Price in PLN</label>
                            <input type="text" className="form-control" id="price-in-pln"
                                   placeholder="Enter price" value={this.state.priceInPln}
                                   onChange={this.handlePriceChange}/>
                            <span className="text-danger">
                                   {validation.priceInPlnErrors}
                               </span>
                        </div>
                        <div className="form-group col-12 col-md-6">
                            <label htmlFor="mileage-in-km">Mileage in km</label>
                            <input type="text" className="form-control" id="mileage-in-km"
                                   placeholder="Enter mileage" value={this.state.mileageInKm}
                                   onChange={this.handleMileageChange}/>
                            <span className="text-danger">
                                   {validation.mileageInKmErrors}
                               </span>
                        </div>
                    </div>
                    <div className="global-error-margin">
                    <span className="text-danger">
                                   {this.state.generalErrorMessage}
                               </span>
                    </div>
                    <button type="submit" value="Save" className="btn btn-primary">Save</button>
                </form>
            </div>
        )
    }
}

export default NewFuel;