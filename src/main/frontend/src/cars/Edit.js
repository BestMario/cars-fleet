import React from 'react'
import axios from 'axios'
import config from '../config'

class Edit extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            make: '',
            model: '',
            manufacturedYear: '',
            numberPlate: '',
            validation: {},
            generalErrorMessage: ''
        };
        this.handleMakeChange = this.handleMakeChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleModelChange = this.handleModelChange.bind(this);
        this.handleManufacturedYearChange = this.handleManufacturedYearChange.bind(this);
        this.handleNumberPlateChange = this.handleNumberPlateChange.bind(this);

    }

    componentDidMount() {
        const carId = this.props.match.params.id;
        axios.get(`${config.apiGateway.URL}/api/cars/` + carId)
            .then(response => {
                const car = response.data
                this.setState({
                    make: car.make, model: car.model,
                    manufacturedYear: car.manufacturedYear, numberPlate: car.numberPlate
                })
            })
    }

    handleNumberPlateChange(event) {
        this.setState({numberPlate: event.target.value})
    }

    handleManufacturedYearChange(event) {
        this.setState({manufacturedYear: event.target.value})
    }

    handleModelChange(event) {
        this.setState({model: event.target.value})
    }

    handleMakeChange(event) {
        this.setState({make: event.target.value})
    }

    handleSubmit(event) {
        event.preventDefault();

        const make = this.state.make;
        const model = this.state.model;
        const manufacturedYear = this.state.manufacturedYear;
        const numberPlate = this.state.numberPlate;
        const updateCarCommand = {
            make: make,
            model: model,
            manufacturedYear: manufacturedYear,
            numberPlate: numberPlate
        }

        const carId = this.props.match.params.id;
        axios.patch(`${config.apiGateway.URL}/api/cars/` + carId,
            // {
            // headers: {
            //     'Access-Control-Allow-Origin': '*',
            // }
            // },
            updateCarCommand)
            .then(response => {
                this.props.history.push(`/cars`)
            }).catch(err => {
            if (err.response && err.response.data) {
                const validation = err.response.data
                this.setState({validation: validation})
            } else {
                this.setState({generalErrorMessage: "Something went wrong while executing your request"})
            }
        })
    }


    render() {
        const validation = this.state.validation
        return (
            <div className="container">
                <form onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="form-group col-12 col-md-6">
                            <label htmlFor="make">Make</label>
                            <input type="text" className="form-control" id="make"
                                   placeholder="Enter make" value={this.state.make} onChange={this.handleMakeChange}/>
                            <span className="text-danger">
                                   {validation.makeErrors}
                               </span>
                        </div>
                        <div className="form-group col-12 col-md-6">
                            <label htmlFor="model">Model</label>
                            <input type="text" className="form-control" id="model"
                                   placeholder="Enter model" value={this.state.model}
                                   onChange={this.handleModelChange}/>
                            <span className="text-danger">
                                   {validation.modelErrors}
                               </span>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-12 col-md-6">
                            <label htmlFor="manufactured-year">Manufactured year</label>
                            <input type="text" className="form-control" id="manufactured-year"
                                   placeholder="Enter manufactured year" value={this.state.manufacturedYear}
                                   onChange={this.handleManufacturedYearChange}/>
                            <span className="text-danger">
                                   {validation.manufacturedYearErrors}
                               </span>
                        </div>
                        <div className="form-group col-12 col-md-6">
                            <label htmlFor="number-plate">Number plate</label>
                            <input type="text" className="form-control" id="number-plate"
                                   placeholder="Enter number plate" value={this.state.numberPlate}
                                   onChange={this.handleNumberPlateChange}/>
                            <span className="text-danger">
                                   {validation.numberPlateErrors}
                               </span>
                        </div>
                    </div>
                    <div>
                        <div className="global-error-margin">
                    <span className="text-danger">
                                   {this.state.generalErrorMessage}
                               </span>
                        </div>
                    </div>
                    <button type="submit" value="Save" className="btn btn-primary">Save</button>
                </form>
            </div>
        )
            ;
    }
}

export default Edit;