import React from 'react';
import {
    Link
} from "react-router-dom";
import qs from 'query-string'
import axios from 'axios'
import config from '../config'

class AddDriverToCar extends React.Component {

    constructor(props) {
        super(props)
        let filteringText = qs.parse(this.props.location.search, {ignoreQueryPrefix: true}).filter
        filteringText = filteringText === undefined ? "" : filteringText
        let pageNumber = qs.parse(this.props.location.search, {ingnoreQueryPrefix: true}).page
        pageNumber = pageNumber === undefined ? 1 : pageNumber;

        this.state = {car: {}, driversPage: {drivers: []}, filteringText: filteringText, pageNumber: pageNumber}

        this.assignDriverToCar = this.assignDriverToCar.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
        this.reloadDrivers = this.reloadDrivers.bind(this);
        this.onInputSearchEnter = this.onInputSearchEnter.bind(this);
        this.filterAssign = this.filterAssign.bind(this);
        this.onFilteringChange = this.onFilteringChange.bind(this);
        this.loadCar = this.loadCar.bind(this);
    }

    componentDidMount() {
        const pageNumber = this.state.pageNumber
        let filteringText = this.state.filteringText

        this.loadCar()
        this.reloadDrivers(pageNumber, filteringText)
    }

    loadCar() {
        const carId = this.props.match.params.carId
        axios.get(`${config.apiGateway.URL}/api/cars/` + carId)
            .then(car => {
                this.setState({car: car.data})
            })
    }

    assignDriverToCar(driverId) {
        const carId = this.props.match.params.carId
        axios.patch(`${config.apiGateway.URL}/api/cars/` + carId + "/drivers/" + driverId)
            .then(response => this.props.history.push(`/cars`))
    }

    handlePageChange(pageNumber) {
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        this.reloadDrivers(pageNumber, this.state.filteringText)
    }

    reloadDrivers(pageNumber, filteringText) {
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        filteringText = filteringText === undefined ? "" : filteringText

        axios.get(`${config.apiGateway.URL}/api/cars/drivers?page-number=` + pageNumber + "&filter=" + filteringText)
            .then(response => {
                this.setState({driversPage: response.data})
            })
    }

    onInputSearchEnter(event) {
        if (event.keyCode === 13) {
            this.setState({pageNumber: 1})
            this.filterAssign()
        }
    }

    filterAssign() {
        const pageNumber = 1
        this.setState({pageNumber: pageNumber})
        let filteringText = this.state.filteringText
        this.reloadDrivers(pageNumber, filteringText)
    }

    onFilteringChange(event) {
        this.setState({filteringText: event.target.value})
    }

    render() {
        let pages = []
        for (let i = 1; i <= this.state.driversPage.numberOfPages; i++) {
            pages.push(i)
        }
        const car = this.state.car
        const drivers = this.state.driversPage.drivers;
        return (
            <div>
                <div className="container search-margin">
                    <div className="row font-weight-bold">
                        <div className="col-4">
                            {car.make} {car.model}, {car.manufacturedYear}
                        </div>
                        <div className="col-8">
                            <div className="input-group">
                                <input type="text" onKeyDown={this.onInputSearchEnter} value={this.state.filteringText}
                                       className="form-control" placeholder="Search" onChange={this.onFilteringChange}/>
                                <div className="input-group-append">
                                    <button className="btn btn-secondary" type="button" onClick={this.filterAssign}>
                                        <i className="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row border table-header-bg">
                        <div className="col-4 col-sm-3">First Name</div>
                        <div className="col-4 col-sm-3">Last Name</div>
                        <div className="col-4 col-sm-3">Availability</div>
                        <div className="col-sm-3 d-none d-sm-block">Options</div>
                    </div>
                    {drivers.map(driver =>
                        <div key={driver.driverId} className="row border-left border-right border-bottom">
                            <div className="col-4 col-sm-3">{driver.firstName}</div>
                            <div className="col-4 col-sm-3">{driver.lastName}</div>
                            <div className="col-4 col-sm-3">{driver.currentDriver ? "Assigned" : "Available"}</div>
                            <div className="col-12 col-sm-3">
                                <input type="button" value="Assign"
                                       onClick={() => this.assignDriverToCar(driver.driverId)}
                                       disabled={!driver.assigned}/>
                            </div>
                        </div>
                    )}
                    <div></div>
                    <div className="row mt-md-1">
                        <div className="col-12 d-flex justify-content-center">
                            <ul className="pagination pagination-sm">
                                {pages.map(page => {
                                    return <li key={page} className="page-item">
                                        <Link onClick={() => this.handlePageChange(page)}
                                              to={`driver?page=${page}&filter=${this.state.filteringText}`}
                                              className="page-link" href="#">{page}</Link>
                                    </li>
                                })}

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )

    }
}

export default AddDriverToCar;