import React from 'react'
import {Link} from "react-router-dom";
import qs from "query-string";
import axios from 'axios'
import config from '../config'

class Fuel extends React.Component {

    constructor(props) {
        super(props)
        let pageNumber = qs.parse(this.props.location.search, {ignoreQueryPrefix: true}).page
        pageNumber = pageNumber === undefined ? 1 : pageNumber
        this.state = {
            car: {},
            fuelsPage: {fuelConsumptions: []},
            pageNumber: pageNumber,
            deletingErrorMessage: ''
        }
        this.loadCar = this.loadCar.bind(this);
        this.loadFuels = this.loadFuels.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
    }

    componentDidMount() {
        const pageNumber = this.state.pageNumber

        this.loadCar()
        this.loadFuels(pageNumber)
    }

    handlePageChange(pageNumber) {
        this.setState({pageNumber: pageNumber})
        this.loadCar()
        this.loadFuels(pageNumber)
    }

    loadCar() {
        const carId = this.props.match.params.carId
        axios.get(`${config.apiGateway.URL}/api/cars/` + carId)
            .then(response => {
                const car = response.data
                this.setState({car: car})
            })
    }

    loadFuels(pageNumber) {
        pageNumber = pageNumber === undefined ? 1 : pageNumber;
        const carId = this.props.match.params.carId;
        axios.get(`${config.apiGateway.URL}/api/cars/` + carId + "/fuel-history?page-number=" + pageNumber)
            .then(response => {
                const fuelsPage = response.data
                this.setState({fuelsPage: fuelsPage})
            })
    }

    handleDelete(fuelId) {
        const carId = this.props.match.params.carId;

        axios.delete(`${config.apiGateway.URL}/api/cars/` + carId + "/fuel-history/" + fuelId)
            .then(response => {
                // window.location.reload();
                this.handlePageChange(this.state.pageNumber)
            })
            .catch(err => this.setState({deletingErrorMessage: "Something went wrong while deleting car"}));
    }

    render() {
        const car = this.state.car;
        const fuels = this.state.fuelsPage.fuelConsumptions;
        let pages = []
        for (let i = 1; i <= this.state.fuelsPage.numberOfPages; i++) {
            pages.push(i)
        }
        return (
            <div>
                <div className="container">
                    <div className="row font-weight-bold">
                        <div className="col-2">
                            <Link to="new-refueling" className="btn btn-link">Add Refueling</Link>
                        </div>
                        <div className="col-10 car-margin">
                            {car.make} {car.model}, {car.manufacturedYear}
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row border table-header-bg">
                        <div className="col-3 col-sm-2">Date</div>
                        <div className="col-3 col-sm-2">Amount</div>
                        <div className="col-3 col-sm-2">Fuel type</div>
                        <div className="col-3 col-sm-2">Price</div>
                        <div className="col-sm-2 d-none d-sm-block">Mileage</div>
                        <div className="col-sm-2 d-none d-sm-block">Options</div>
                    </div>
                    {fuels.map(fuel =>
                        <div key={fuel.fuelId} className="row border-left border-right border-bottom">
                            <div className="col-3 col-sm-2">{fuel.refuelingDate}</div>
                            <div className="col-3 col-sm-2">{fuel.amountInLiters}</div>
                            <div className="col-3 col-sm-2">{fuel.fuelType}</div>
                            <div className="col-3 col-sm-2">{fuel.priceInPln}</div>
                            <div className="col-sm-2 d-none d-sm-block">{fuel.mileageInKm}</div>
                            <div className="col-12 col-sm-2">
                                <Link to={`fuel/${fuel.fuelId}`} className="btn btn-link">Edit</Link>
                                <input className="btn btn-link" type="button" value="Delete"
                                       onClick={() => this.handleDelete(fuel.fuelId)}/>
                            </div>
                        </div>
                    )}
                    <div>
                        <div className="global-error-margin">
                    <span className="text-danger">
                                   {this.state.deletingErrorMessage}
                               </span>
                        </div>
                    </div>
                </div>
                <div className="row mt-md-1">
                    <div className="col-12 d-flex justify-content-center">
                        <ul className="pagination pagination-sm">
                            {pages.map(page => {
                                return <li key={page} className="page-item"><Link
                                    onClick={() => this.handlePageChange(page)} to={`fuel?page=${page}`}
                                    className="page-link" href="#">{page}</Link></li>
                            })}

                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}


export default Fuel;