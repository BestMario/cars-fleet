import React, { Component }  from 'react';
import { createRef } from 'react'
import axios from 'axios'

// class AuthenticatedLink({ url, filename, children }) extends React.Component {
function AuthenticatedLink ({ url, filename, children }) {
    const link = createRef()

    const handleAction = async () => {
        if (link.current.href) { return }

        axios.get(url, {
            responseType: 'blob',
            timeout: 3000
        }).then((response) => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');

            // const linkText = document.createTextNode("Generate report");
            // link.appendChild(linkText);

            link.href = url;
            link.setAttribute('download', filename);
            // link.className = 'btn btn-primary'
            // link.setAttribute('class', 'btn btn-primary')
            document.body.appendChild(link);
            link.click();
        });


        // const result = await fetch(url, {
        //     // headers: {...authHeaders}
        //     headers: {}
        // })

        // const blob = result.blob()
        // const href = window.URL.createObjectURL(blob)
        //
        // link.current.download = filename
        // link.current.href = href
        //
        // link.current.click()
    }

    return (
        <>
        <a role='button' ref={link} onClick={handleAction}>{children}</a>
        </>
)
}

export default AuthenticatedLink;