import React from 'react'
import {Map, GoogleApiWrapper, Marker, InfoWindow} from 'google-maps-react';
import axios from "axios";
import config from '../config'

class MapPage extends React.Component {

    constructor(props) {
        super(props);
        this.map = null;
        this.state = {
            cars: [],
            activeMarker: null,
            markerObjects: [],
            infoWindowIsOpen: false,
            selectedPlace: {}
        };

        this.loadPositions = this.loadPositions.bind(this);
        this.displayMarkers = this.displayMarkers.bind(this);
        this.onCarOnListClick = this.onCarOnListClick.bind(this);
        this.onMarkerClick = this.onMarkerClick.bind(this);
        this.handleMapMounted = this.handleMapMounted.bind(this)
        this.handleToggleClose = this.handleToggleClose.bind(this)
        this.onMarkerMounted = this.onMarkerMounted.bind(this)
    }

    componentDidMount() {
        this.loadPositions()
    }

    loadPositions() {
        axios.get(`${config.apiGateway.URL}/api/cars`
        //     , {
        //     headers: {
        //         'Access-Control-Allow-Origin': '*',
        //     }
        // }
        )
            .then(response => {
                const cars = response.data;
                this.setState({cars: cars})
            }).catch(err => {
            console.warn("Problem while loading cars positions", err)
        })
    }

    displayMarkers = () => {
        return this.state.cars.map((car, index) => {
            return <Marker ref={this.onMarkerMounted}
                           name={car.make + " " + car.model + " " + car.numberPlate}
                           title={car.numberPlate}
                           key={car.carId}
                           id={"marker-car-id-" + car.carId}
                           position={{lat: car.position.longitude / 100000, lng: car.position.latitude / 100000}}
                           onClick={this.onMarkerClick}
            />
        })
    }

    onMarkerMounted = element => {
        this.setState(prevState => ({
            markerObjects: [...prevState.markerObjects, element.marker]
        }))
    }

    onMarkerClick = (props, marker, e) => {
        this.setState({
            selectedPlace: props,
            activeMarker: marker,
            infoWindowIsOpen: true
        });
    }

    onCarOnListClick(car) {
        const position = car.position;
        this.moveToLocation(position.longitude / 100000, position.latitude / 100000);

        // const currentMarker = this.state.markerObjects.find(c => c.id === "marker-car-id-" + car.carId)
        // this.setState({infoWindowIsOpen: true, activeMarker: currentMarker})
    }

    moveToLocation(lat, lng) {
        const center = new this.props.google.maps.LatLng(lat, lng);
        this.map.panTo(center);
    }

    handleMapMounted = (mapProps, map) => {
        this.map = map;
    };

    handleToggleClose = () => {
        this.setState({
            infoWindowIsOpen: false
        });
    };

    render() {

        const cars = this.state.cars;

        return (
            <div className="container">
                <div className="row">
                        <div className="col-4">
                            {cars.map(car =>
                                <ul key={car.carId} className="list-group"
                                    onClick={() => this.onCarOnListClick(car, this.props)}>
                                    <li className="list-group-item list-group-item-action text-break car-map-item">{car.make} {car.model} {car.numberPlate}</li>
                                </ul>
                            )}
                        </div>
                        <div className="col-8 map__container">
                            <Map
                                {...this.props}
                                google={this.props.google}
                                zoom={8}
                                initialCenter={{lat: 51.1, lng: 17.0333}}
                                onReady={this.handleMapMounted}
                            >
                                {this.displayMarkers()}

                                <InfoWindow
                                    onClose={this.handleToggleClose}
                                    marker = { this.state.activeMarker }
                                    visible = { this.state.infoWindowIsOpen }
                                >
                                    <div className="font-weight-normal">
                                        {this.state.selectedPlace.name}
                                    </div>
                                </InfoWindow>

                            </Map>
                        </div>
                </div>
            </div>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyDhXXwTD-oLpiUlSCUWKcW9IEwkGN5Lrmo'
})(MapPage);

