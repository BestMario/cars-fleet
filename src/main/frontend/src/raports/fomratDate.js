export function yyyymmdd(date) {
    var yyyy = date.getFullYear()
    var mm = date.getMonth() + 1;
    var dd = date.getDate();

    return [yyyy + '-' +
    (mm>9 ? '' : '0') + mm + '-' +
    (dd>9 ? '' : '0') + dd
    ].join('');
};