import React from 'react'
import DatePicker from "react-datepicker";
import {yyyymmdd} from './fomratDate'
import axios from 'axios'
import Toggle from 'react-bootstrap-toggle';
import AuthenticatedLink from '../link/AuthenticatedLink'
import config from '../config'

class Reports extends React.Component {

    constructor() {
        super()
        const initialDate = new Date()
        this.state = {
            cars: [],
            selectedCar: 'ALL_CARS',
            refuelingDateFrom: initialDate,
            refuelingDateTo: initialDate,
            // datesRangeDisabled: false,
            selectedReportFormat: 'CSV',
            toggleActive: false
        }

        this.onCarOptionChange = this.onCarOptionChange.bind(this)
        this.handleDateChangeFrom = this.handleDateChangeFrom.bind(this)
        this.handleDateChangeTo = this.handleDateChangeTo.bind(this)
        // this.handleSkipDatesRangeChange = this.handleSkipDatesRangeChange.bind(this)
        this.handleChangeReportFormat = this.handleChangeReportFormat.bind(this)
        this.onToggle = this.onToggle.bind(this)
    }

    componentDidMount() {
        axios.get(`${config.apiGateway.URL}/api/cars`)
            .then(response => {
                this.setState({cars: response.data})
            })
    }

    onCarOptionChange(event) {
        this.setState({selectedCar: event.target.value})
    }

    handleDateChangeFrom = dateFrom => {
        this.setState({refuelingDateFrom: dateFrom});
    };

    handleDateChangeTo = dateTo => {
        this.setState({refuelingDateTo: dateTo});
    };

    // handleSkipDatesRangeChange(event) {
    //     this.setState({datesRangeDisabled: event.target.value})
    // }

    handleChangeReportFormat(event) {
        this.setState({selectedReportFormat: event.target.value})
    }

    onToggle() {
        this.setState({
            toggleActive: !this.state.toggleActive,
            // handleSkipDatesRangeChange: !this.state.handleSkipDatesRangeChange,
            // datesRangeDisabled: !this.state.datesRangeDisabled
        })
    }

    render() {

        const reportFormat = this.state.selectedReportFormat
        const cars = this.state.cars
        const datesRangeDisabled = this.state.toggleActive
        const selectedCar = this.state.selectedCar
        const refuelingDateFrom = datesRangeDisabled ? '' : yyyymmdd(this.state.refuelingDateFrom)
        const refuelingDateTo = datesRangeDisabled ? '' : yyyymmdd(this.state.refuelingDateTo)
        const reportUrlAllCars = `${config.apiGateway.URL}/api/cars/fuels/report?refueling-date-from=` + refuelingDateFrom + '&refueling-date-to=' + refuelingDateTo + '&report-format=' + reportFormat
        const reportUrlIdCar = `${config.apiGateway.URL}/api/cars/` + selectedCar + '/fuel/report?refueling-date-from=' + refuelingDateFrom + '&refueling-date-to=' + refuelingDateTo + '&report-format=' + reportFormat
        const reportUrl = selectedCar === 'ALL_CARS' ? reportUrlAllCars : reportUrlIdCar

        const filename = 'report.' + (reportFormat === 'EXCEL' ? 'xlsx' : reportFormat)

        return (
            <div className="container font-weight-bold">
                <div className="row">
                    <div className="col-12 col-md-6">
                        <label>Select format:</label>
                        <select className="form-control" value={this.state.selectedReportFormat}
                                onChange={this.handleChangeReportFormat}>
                            <option value="CSV">csv</option>
                            <option value="EXCEL">excel</option>
                            <option value="PDF">pdf</option>
                        </select>
                    </div>
                    <div className="col-12 col-md-6">
                        <div className="form-group">
                            <label>Select car:</label>
                            <select className="form-control" onChange={this.onCarOptionChange}
                                    value={this.state.selectedCar}>
                                <option value="ALL_CARS">All cars</option>
                                {
                                    cars.map((car) => <option key={car.carId}
                                                              value={car.carId}>{car.make}</option>)
                                }
                            </select>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 col-md-4">
                        <div className="form-group">
                            <label className="col-12">Select date from:</label>
                            <div className="customDatePickerWidth">
                                <DatePicker className="form-control" id="refuelingDateFrom" dateFormat="yyyy-MM-dd"
                                            selected={this.state.refuelingDateFrom}
                                            onChange={this.handleDateChangeFrom} disabled={datesRangeDisabled}/>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-md-4">
                        <div className="form-group">
                            <label className="col-12">Select date to:</label>
                            <div className="customDatePickerWidth">
                                <DatePicker className="form-control" id="refuelingDateTo" dateFormat="yyyy-MM-dd"
                                            selected={this.state.refuelingDateTo}
                                            onChange={this.handleDateChangeTo} disabled={datesRangeDisabled}/>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-md-4 form-group">
                        <label>Select Full Period</label>
                        <div className="col-12 form-group">
                            <Toggle
                                onClick={this.onToggle}
                                on={<h6>Selected</h6>}
                                off={<h6>OFF</h6>}
                                size="xs"
                                offstyle="light"
                                active={this.state.toggleActive}
                            />
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 car-margin">
                        <AuthenticatedLink url={reportUrl} filename={filename}>
                            <input type="button" className="btn btn-primary" value="Generate report" />
                        </AuthenticatedLink>
                    </div>
                </div>
            </div>
        )
    }
}


export default Reports;