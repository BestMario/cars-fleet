import React from 'react'
import AuthenticationService from './AuthenticationService'

class LoginPage extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            username: '',
            password: '',
            hasLoginFailed: false,
            showSuccessMessage: false
        }

        this.handleChange = this.handleChange.bind(this)
        this.loginClicked = this.loginClicked.bind(this)
        this.loginAsDemoUser = this.loginAsDemoUser.bind(this)
    }

    handleChange(event) {
        this.setState(
            {
                [event.target.name]: event.target.value
            }
        )
    }

    loginClicked(event) {
        if (event) {
            event.preventDefault();
        }

        AuthenticationService
            // .executeBasicAuthenticationService(this.state.username, this.state.password)
            .executeJwtAuthenticationService(this.state.username, this.state.password)
            .then((response) => {
                AuthenticationService.registerSuccessfulLoginForJwt(this.state.username, response.data.token)
                // AuthenticationService.registerSuccessfulLogin(this.state.username, this.state.password)
                this.props.history.push(`/map`)
            })
            .catch(() => {
                this.setState({showSuccessMessage: false})
                this.setState({hasLoginFailed: true})
            })
    }

    loginAsDemoUser(ev) {
        ev.preventDefault()
        this.setState({username: 'demo@demo.pl', password: 'demo'}, () => {

            this.loginClicked()
        })
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-3 col-xl-4"></div>
                    <div className="col-12 col-md-6 col-xl-4">
                        <h3>Login</h3>
                        <form id="loginForm" onSubmit={this.loginClicked}>
                            {this.state.hasLoginFailed &&
                            <div className="alert alert-warning">Invalid Credentials</div>}
                            {this.state.showSuccessMessage && <div>Login Sucessful</div>}
                            <div className="form-group">
                                <input type="text" name="username" className="form-control"
                                       placeholder="Enter Your Email *"
                                       value={this.state.username} onChange={this.handleChange}/>
                            </div>
                            <div className="form-group">
                                <input type="password" name="password" className="form-control"
                                       placeholder="Enter Your Password *"
                                       value={this.state.password} onChange={this.handleChange}/>
                            </div>
                            <div className="form-group">
                                <button id="loginButton" className="btn btn-success btn-block" type="submit">Login</button>
                            </div>
                        </form>
                        <div className="form-group">
                            <button className="btn btn-primary btn-block" onClick={this.loginAsDemoUser}>Demo</button>
                        </div>
                    </div>
                    <div className="col-md-3 col-xl-4"></div>
                </div>
            </div>
        )
    }
}

export default LoginPage;
