CREATE TABLE `car_driver` (
    `id` int(11) NOT NULL AUTO_INCREMENT UNIQUE,
    `first_name` varchar(40) NOT NULL,
    `last_name` varchar(40) NOT NULL,
    `age` int(11) NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;