CREATE TABLE `fuel` (
    `id` int(11) NOT NULL AUTO_INCREMENT UNIQUE,
    `refueling_date` DATE NOT NULL,
    `amount_in_liters` DECIMAL(5,2) NOT NULL,
    `fuel_type` varchar(30) NOT NULL,
    `price_in_pln` DECIMAL(7,2) NOT NULL,
    `mileage_in_km` LONG NOT NULL,
    `car_id` int(11) NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (car_id) REFERENCES car(id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;