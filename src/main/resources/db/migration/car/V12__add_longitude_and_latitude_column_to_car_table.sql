ALTER TABLE `car`
ADD COLUMN longitude INT DEFAULT NULL after car_status,
ADD COLUMN latitude INT DEFAULT NULL after longitude;