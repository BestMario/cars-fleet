CREATE TABLE `car` (
    `id` int(11) NOT NULL AUTO_INCREMENT UNIQUE,
    `make` varchar(40) NOT NULL,
    `car_model` varchar(40) NOT NULL,
    `manufactured_year` int(11) NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;