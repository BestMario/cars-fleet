# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Building a RESTful Web Service with Spring Boot Actuator](https://spring.io/guides/gs/actuator-service/)
* [Handling Form Submission](https://spring.io/guides/gs/handling-form-submission/)

mvnw test sonar:sonar -Dsonar.host.url=http://localhost:9000 -Dsonar.login=ab61cc3b6fe701338286fe2f7597bfd525871e3c

Helpful pages
How to create dynamically new pages using Apache PDFBox
https://stackoverflow.com/questions/42767534/pdfbox-issue-while-creating-a-new-page-dynamically

GPS positions formats
http://galka.mountlab.net/www/topografia/formaty_pozycji

https://www.springboottutorial.com/spring-boot-react-full-stack-with-spring-security-basic-and-jwt-authentication

Spring login using mysql and BCryptPasswordEncoder:
http://progressivecoder.com/implementing-spring-boot-security-using-userdetailsservice/

Login page, autorization and authentication
https://www.springboottutorial.com/spring-boot-react-full-stack-with-spring-security-basic-and-jwt-authentication

Uruchamiać sonara conajmniej raz dziennie. Sprawdzać błędy które znalazł sonar, oraz test coverage. tdd,

1. TDD - red/green/refactor
2. Refactoring methods - move to method, move to class, parametrized object
3. Use debug (a lot of issues can be limited or fix quickly)
4. Sonar - for automatization of finding issues and bugs in the code (static analyse of the code)
5. Clean code (especially use often CTRL+ALT + M; code should be readable like a book)
6. Analyze logs of the app

Inspiration:
How to configure view controllers for SPA (Single Page Application) application (e.g. for refreshing page)
https://stackoverflow.com/questions/39331929/spring-catch-all-route-for-index-html/42998817#42998817

Publish on Google Cloud platform:
https://medium.com/@wkrzywiec/how-to-publish-a-spring-boot-app-with-a-database-on-the-google-cloud-platform-614b88613ce3