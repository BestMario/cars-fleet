mvnw package -DskipTests -Pdev

Deploy to Google Cloud Platform:
mvnw clean package appengine:deploy -Pgcp -DskipTests

Managing .env variables for provisional builds with Create React App
https://dev.to/jam3/managing-env-variables-for-provisional-builds-h37